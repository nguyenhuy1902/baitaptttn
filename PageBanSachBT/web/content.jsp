<%-- 
    Document   : content
    Created on : Apr 8, 2018, 8:38:43 PM
    Author     : Administrator
--%>

<%@page import="java.util.TreeMap"%>
<%@page import="model.DbtCart"%>
<%@page import="model.DbtBook"%>
<%@page import="dao.BookDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>content</title>
    </head>
    <body>
        <%
            BookDAO BookDAO = new BookDAO();
            DbtCart dbtCart = (DbtCart) session.getAttribute("dbtCart");
            if (dbtCart == null) {
                dbtCart = new DbtCart();
                session.setAttribute("dbtCart", dbtCart);
            }
//            TreeMap<DbtBook, Integer> list = dbtCart.getCartListBook();
        %>
        <div class="span9">		
            <div class="well well-small">
                <h4>Tác phẩm mới <small class="pull-right"></small></h4>
                <div class="row-fluid">
                    <div id="featured" class="carousel slide">
                        <div class="carousel-inner">
                            <div class="item active">
                                <ul class="thumbnails">
                                    <%
                                        int i = 0;
                                        for (DbtBook b : BookDAO.getDbtBookRandom()) {
                                    %>
                                    <li class="span3">
                                        <div class="thumbnail">
                                            <i class="tag"></i>
                                            <a href="product-details.jsp?bookId=<%=b.getBookId()%>"><img src="<%=b.getBookImage()%>" alt=""></a>
                                            <div class="caption">
                                                <h5 class="name_pro"><%=b.getBookName()%></h5>
                                                <h4><a class="btn" href="product-details.jsp?bookId=<%=b.getBookId()%>">Xem</a> <span class="pull-right"><%=b.getBookPrice()%> VNĐ</span></h4>
                                            </div>
                                        </div>
                                    </li>
                                    <%
                                            i++;
                                            if (i%4 == 0) {
                                                out.print("</ul></div><div class=\"item\"><ul class=\"thummbnails\">");
                                            }
                                        }
                                    %>
                                </ul>
                            </div>

                        </div>
                        <a class="left carousel-control" href="#featured" data-slide="prev">‹</a>
                        <a class="right carousel-control" href="#featured" data-slide="next">›</a>
                    </div>
                </div>
            </div>
            <h4>Tác phẩm đặc trưng </h4>
            <ul class="thumbnails">
                <%
                    for (DbtBook b : BookDAO.getDbtBookRandom1()) {
                %>
                <li class="span3">
                    <div class="thumbnail">
                        <a  href="product-details.jsp?bookId=<%=b.getBookId()%>"><img src="<%=b.getBookImage()%>" alt="" width="150px"/></a>
                        <div class="caption">
                            <h5><%=b.getBookName()%></h5>
                            <p> 
                                Tác giả: <%=b.getBookWriterName()%>
                            </p>

                            <h4 style="text-align:center"><a class="btn" href="product-details.jsp?bookId=<%=b.getBookId()%>"> <i class="icon-zoom-in"></i></a> 
                                <!--<a class="btn" href="<%=request.getContextPath()%>/CartServlet?command=insertItem&bookId=<%=b.getBookId()%>&bookName=<%=b.getBookName()%>&bookWriter=<%=b.getBookWriterName()%>&bookAnh=<%=b.getBookImage()%>&bookGia=<%=b.getBookPrice()%>">Thêm vào <i class="icon-shopping-cart"></i></a>--> 
                                <a class="btn btn-primary" href="#"><%=b.getBookPrice()%> VNĐ</a>
                            </h4>
                        </div>
                    </div>
                </li>
                <%
                    }
                %>
            </ul>	

        </div>

    </body>
</html>
