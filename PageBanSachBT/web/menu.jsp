<%-- 
    Document   : menu
    Created on : Apr 8, 2018, 8:43:45 PM
    Author     : Administrator
--%>

<%@page import="dao.BookDAO"%>
<%@page import="model.DbtBook"%>
<%@page import="model.DbtCategory"%>
<%@page import="dao.CategoryDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
<!--        <link id="callCss" rel="stylesheet" href="themes/bootshop/bootstrap.min.css" media="screen"/>
        <link href="themes/css/base.css" rel="stylesheet" media="screen"/>
         Bootstrap style responsive 	
        <link href="themes/css/bootstrap-responsive.min.css" rel="stylesheet"/>
        <link href="themes/css/font-awesome.css" rel="stylesheet" type="text/css">
         Google-code-prettify 	
        <link href="themes/js/google-code-prettify/prettify.css" rel="stylesheet"/>
        <link href="themes/js/jquery.min.js" rel="stylesheet"/>
        <link href="themes/js/jquery.js" rel="stylesheet"/>
         fav and touch icons -->

        
    </head>
    <body>
        <%
            CategoryDAO categoryDAO = new CategoryDAO();
        %>
        <%
            BookDAO BookDAO = new BookDAO();
            %>

        <div id="sidebar" class="span3">
            <ul id="sideManu" class="nav nav-tabs nav-stacked">
                <%
                    for(DbtCategory c : categoryDAO.getCategoryByParent()){
                    %>
                    <li><a href="product.jsp?DbtCategory=<%=c.getCategoryId()%>"><%=c.getCategoryName()%></a>
                    <%if(categoryDAO.getCategoryByChildrent(c.getCategoryId()).size()>0){%>
                    <ul>
                        <%
                            for(DbtCategory c1 : categoryDAO.getCategoryByChildrent(c.getCategoryId())){
                            %>
                        <li><a class="active" href="product.jsp?DbtCategory=<%=c1.getCategoryId()%>"><i class="icon-chevron-right"></i><%=c1.getCategoryName()%></a></li>
                        <%}%>
                    </ul>
                    <%}%>
                </li>
                <%
                    }
                    %>
            </ul>
            <br/>
            <%
                for(DbtBook b : BookDAO.getDbtBookRandom2()){
                %>
<!--            <div class="thumbnail">
                <a href="product-details.jsp?bookId=<%=b.getBookId()%>"><img src="<%=b.getBookImage()%>" alt=""/></a>
                <div class="caption">
                    <h5><%=b.getBookName()%></h5>
                    <h4 style="text-align:center"><a class="btn" href="product-details.jsp?bookId=<%=b.getBookId()%>"> <i class="icon-zoom-in"></i></a> <a class="btn" href="#">Thêm vào <i class="icon-shopping-cart"></i></a> <a class="btn btn-primary" href="#"><%=b.getBookPrice()%> VNĐ</a></h4>
                </div>
            </div><br/>-->
            <%
                }
                %>
            <div class="thumbnail">
                <img src="themes/images/payment_methods.png" title="Bootshop Payment Methods" alt="Payments Methods">
                <div class="caption">
                    <h5>Phương thức thanh toán</h5>
                </div>
            </div>
        </div>
            
            <script src="themes/js/jquery.js" type="text/javascript"></script>
        <!--<script src="themes/js/jquery.js" type="text/javascript"></script>-->
        <script src="themes/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="themes/js/google-code-prettify/prettify.js"></script>

        <script src="themes/js/bootshop.js"></script>
        <script src="themes/js/jquery.lightbox-0.5.js"></script>
    </body>
</html>
