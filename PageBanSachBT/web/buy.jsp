<%-- 
    Document   : buy
    Created on : Apr 8, 2018, 8:33:08 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>buy</title>

        <!-- Bootstrap style --> 
<!--        <link id="callCss" rel="stylesheet" href="themes/bootshop/bootstrap.min.css" media="screen"/>
        <link href="themes/css/base.css" rel="stylesheet" media="screen"/>
         Bootstrap style responsive 	
        <link href="themes/css/bootstrap-responsive.min.css" rel="stylesheet"/>
        <link href="themes/css/font-awesome.css" rel="stylesheet" type="text/css">
         Google-code-prettify 	
        <link href="themes/js/google-code-prettify/prettify.css" rel="stylesheet"/>
         fav and touch icons 

        <script src="themes/js/jquery.js" type="text/javascript"></script>
        <script src="themes/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="themes/js/google-code-prettify/prettify.js"></script>

        <script src="themes/js/bootshop.js"></script>
        <script src="themes/js/jquery.lightbox-0.5.js"></script>-->

    </head>
    <body>
        <jsp:include page="header.jsp"></jsp:include>

            <div id="mainBody">
                <div class="container">
                    <div class="row">
                    <jsp:include page="menu.jsp"></jsp:include>

                        <div class="span9">
                            <ul class="breadcrumb">
                                <li><a href="index.jsp">Trang chủ</a> <span class="divider">/</span></li>
                                <li class="active">Mua hàng</li>
                            </ul>
                            <h3> Mua hàng</h3>	
                            <div class="well">
                                <!--
                                <div class="alert alert-info fade in">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        <strong>Lorem Ipsum is simply dummy</strong> text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s
                                 </div>
                                <div class="alert fade in">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        <strong>Lorem Ipsum is simply dummy</strong> text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s
                                 </div>
                                 <div class="alert alert-block alert-error fade in">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        <strong>Lorem Ipsum is simply</strong> dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s
                                 </div> -->
                                <form class="form-horizontal" method="post" action="<%=request.getContextPath()%>/OrderServlet?command=checkout">
 
                                    <h4>Địa chỉ của bạn</h4>
                                    <div class="control-group">
                                        <label class="control-label" for="inputFname">Họ tên</label>
                                        <div class="controls">
                                            <input type="text" id="inputFname" name="hoten" placeholder="" required="Vui lòng nhập tên">
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="address">Địa chỉ</label>
                                        <div class="controls">
                                            <input type="text" id="address" name="dchi" placeholder="" required="Vui lòng nhập địa chỉ"/> <span>Địa chỉ đường phố, hộp thư, tên công ty ...</span>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label" for="phone">Điện thoại di động</label>
                                        <div class="controls">
                                            <input type="text" id="phone" name="dthoai" placeholder="" required="Vui lòng nhập số điện thoại"/>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label" for="email">Email</label>
                                        <div class="controls">
                                            <input type="email" id="email" name="email" placeholder="" required="Vui lòng nhập Email"/>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label" for="aditionalInfo">Thêm thông tin</label>
                                        <div class="controls">
                                            <textarea name="aditionalInfo" name="thongtin" id="aditionalInfo" cols="26" rows="3"></textarea>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <div class="controls">
                                            <input type="hidden" name="email_create" value="1">
                                            <input type="hidden" name="is_new_customer" value="1">
                                            <input id="thongbao" class="btn btn-large btn-success" type="submit" value="Mua hàng" onclick="return thongbao();"/>
                                        </div>
                                    </div>		
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        <jsp:include page="footer.jsp"></jsp:include>
        <script>
            $('#thongbao').on('click',function(){
               alert("Bạn đã mua hàng thành công. Sách sẽ được chuyển đến cho bạn sớm nhất"); 
            });
        </script>
    </body>
</html>
