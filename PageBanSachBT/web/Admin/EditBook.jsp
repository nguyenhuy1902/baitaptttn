<%-- 
    Document   : EditBook
    Created on : May 13, 2018, 7:54:59 PM
    Author     : Windows 10 TIMT
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="Admin.DAO.DAO_Catagory"%>
<%@page import="model.DbtCategory"%>
<%@page import="Admin.DAO.DAO_Book"%>
<%@page import="model.DbtBook"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Book Library Admin</title>
        <link href="Images/hogwarts_logo.png" rel="shortcut icon">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/sb-admin.css" rel="stylesheet">
        <link href="css/plugins/morris.css" rel="stylesheet">
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="css/NewStyleAdd.css" rel="stylesheet" type="text/css">
        <script src="js/jquery.min.js"></script>
        <script src="js/jquery.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/plugins/morris/raphael.min.js"></script>
        <script src="js/plugins/morris/morris.min.js"></script>
        <script src="js/plugins/morris/morris-data.js"></script>
    </head>
    <body>
        <%
            DAO_Book bc = new DAO_Book();
            DbtBook book = null;
            DAO_Catagory cd = new DAO_Catagory();
            ArrayList<DbtCategory> listCat = cd.getListCat();

            if (request.getParameter("bookid") == null) {
                response.sendRedirect(request.getContextPath() + "/404Error.jsp");
            } else {
                int id = Integer.parseInt(request.getParameter("bookid"));
                book = bc.findBook(id);
            }
            if (book == null && request.getParameter("bookid") != null) {
                response.sendRedirect(request.getContextPath() + "/ErrorPage.jsp");
            } else {

            }

        %>

        <div id="wrapper">
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <jsp:include page="header.jsp"></jsp:include>
                    <div class="collapse navbar-collapse navbar-ex1-collapse">
                        <ul class="nav navbar-nav side-nav">
                            <li class="active">
                                <a href="index.jsp"><i class="fa fa-fw fa-dashboard"></i>Trang chủ</a>
                            </li>
                            <li>
                                <a href="DataTable.jsp"><i class="fa fa-fw fa-table"></i>Bảng dữ liệu</a>
                            </li>
                            <li>
                                <a href="Account.jsp"><i class="fa fa-fw fa-user"></i>Tài khoản</a>
                            </li>
                            <li>
                                <a href="Book.jsp"><i class="fa fa-fw fa-book"></i>Sách</a>
                            </li>
                            <li>
                                <a href="Buyer.jsp"><i class="fa fa-fw fa-credit-card"></i>Đơn Đặt Hàng</a>
                            </li>
                            <li>
                                <a href="Chart.jsp"><i class="fa fa-fw fa-bar-chart-o"></i>Thống kê</a>
                            </li>
                        </ul>
                    </div>
                </nav>
                <!--trang chu-->

                <div id="page-wrapper">

                    <div class="container-fluid">

                        <!-- Page Heading -->
                        <div class="row">
                            <div class="col-lg-12">
                                <h1 class="page-header">
                                    Sửa Thông Tin Sách
                                </h1>
                                <ol class="breadcrumb">
                                    <li>
                                        <i class="fa fa-dashboard"></i>  <a href="index.jsp">Trang chủ</a>
                                    </li>
                                    <li>
                                        <i class="fa fa-book"></i><a href="Book.jsp">Sách </a>
                                    </li>
                                    <li class="active">
                                        <i class="fa fa-edit"></i> Sửa thông tin sách
                                    </li>
                                </ol>
                            </div>
                        </div>
                        <!-- /.row -->
                        <div class="row">
                            <div class="col-lg-6">
                            </div>
                        </div>
                        <!--NhapThongTin-->
                        <div class="row">
                        <%if (book != null) {%>
                        <form class="row" method="post" action="ManageBookServlet?action=editBook">
                            <div class="col-lg-9">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Mã sách</label>
                                            <input class="form-control" readonly type="text" name="bookid" required value="<%=book.getBookId()%>"/>
                                        </div>
                                        <div class="form-group">
                                            <label>Tên sách</label>
                                            <input class="form-control" type="text" name="bookname" required value="<%=book.getBookName()%>"/>
                                        </div>
                                        <div class="form-group">
                                            <label>Ảnh</label>
                                            <input class="form-control" type="file" name="bookimg" required value="<%=book.getBookImage()%>"/>
                                        </div>
                                        <div class="form-group">
                                            <label>Tên tác giả</label>
                                            <input class="form-control"  type="text" name="bookwriter" required value="<%=book.getBookWriterName()%>"/>
                                        </div>
                                        <div class="form-group">
                                            <label>Năm xuất bản</label>
                                            <input class="form-control" type="text" name="bookYP"pattern="[0-9]{4}" title="Nhập số!!!" required value="<%=book.getBookPublishingYear()%>"/>
                                        </div>                                         
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Giá</label>
                                            <input class="form-control" type="text" name="bookprice" pattern="[0-9]{1,}" title="Nhập số!!!" required value="<%=book.getBookPrice()%>"/>
                                        </div>
                                        <div class="form-group">
                                            <label>Số lượng</label>
                                            <input class="form-control" type="text" name="bookquan" pattern="[0-9]{1,}" title="Nhập số!!!" required value="<%=book.getBookQuantity()%>"/>
                                        </div>
                                        <div class="form-group">
                                            <label>Nhà xuất bản</label>
                                            <input class="form-control" type="text" name="bookP" required value="<%=book.getBookPublishingCompany()%>"/>
                                        </div>   
                                        <div class="form-group">
                                            <label>Mô tả</label>
                                            <textarea class="form-control" rows="8" type="text" name="bookdes" required><%=book.getBookDescription()%></textarea>
                                        </div>                                 
                                    </div>                                 
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label>Danh mục lớn</label>
                                    <div>   
                                        <select  name="catagoryP" style="height: 30px; width: 250px;">
                                            <% for (DbtCategory j : listCat) {
                                                    if (j.getCategoryParent() == 0) {
                                            %>
                                            <option value="<%=j.getCategoryId()%>"><%=j.getCategoryName()%></option>
                                            <%}
                                                }%> 
                                        </select>
                                    </div>
                                    <div>
                                        <br><label>Danh mục nhỏ</label>
                                        <select  name="catagory" style="height: 30px; width: 250px;">
                                            <% for (DbtCategory j : listCat) {
                                                    if (j.getCategoryParent() != 0) {
                                            %>
                                            <option value="<%=j.getCategoryId()%>"><%=j.getCategoryName()%></option>
                                            <%}
                                                }%>
                                        </select>                                  
                                        <div class="form-group">
                                            <br><label>Nhà cung cấp</label>
                                            <input class="form-control" type="text" name="bookC" required value="<%=book.getBookProvider()%>"/>
                                        </div>
                                        <div class="col-lg-12">
                                            <label>Thao Tác</label>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-warning " value="Save"><i class="fa fa-pencil-square"></i> Sửa</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </form>
                        <%}%>   
                    </div>
                    <!-- Bang du lieu-->
                    <!--DeTrong-->
                    <div class="row">

                    </div>
                    <!-- /.row -->
                </div>
            </div>
        </div>
        </div>
    </body>
</html>
