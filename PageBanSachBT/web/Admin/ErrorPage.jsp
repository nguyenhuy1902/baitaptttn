<%-- 
    Document   : ErrorPage
    Created on : Apr 2, 2018, 12:05:36 AM
    Author     : Windows 10 TIMT
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Book Library Admin</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/sb-admin.css" rel="stylesheet">
        <link href="css/plugins/morris.css" rel="stylesheet">
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="css/404.css">
    </head>
    <body>
        <div id="wrapper">
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <jsp:include page="header.jsp"></jsp:include>
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav side-nav">
                        <li>
                            <a href="index.jsp"><i class="fa fa-fw fa-dashboard"></i>Trang chủ</a>
                        </li>
                        <li>
                            <a href="DataTable.jsp"><i class="fa fa-fw fa-table"></i>Bảng dữ liệu</a>
                        </li>
                        <li>
                            <a href="Account.jsp"><i class="fa fa-fw fa-user"></i>Tài khoản</a>
                        </li>
                        <li>
                            <a href="Book.jsp"><i class="fa fa-fw fa-book"></i>Sách</a>
                        </li>
                        <li>
                            <a href="Buyer.jsp"><i class="fa fa-fw fa-edit"></i>Đơn đặt Hàng</a>
                        </li>
                        <li>
                            <a href="Chart.jsp"><i class="fa fa-fw fa-bar-chart-o"></i>Thống kê</a>
                        </li>
                    </ul>
                </div>
            </nav>
            <!-- Trang loi-->
            <div id="page-wrapper">

                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">
                                Blank Page
                                <small>Subheading</small>
                            </h1>
                            <ol class="breadcrumb">
                                <li>
                                    <i class="fa fa-dashboard"></i>  <a href="index.jsp">Dashboard</a>
                                </li>
                                <li class="active">
                                    <i class="fa fa-file"></i> Blank Page
                                </li>
                            </ol>
                        </div>
                    </div>
                    <div class="inner-block">
                        <div class="error-404">     
                            <div class="error-page-left">
                                <img src="Images/404.png">
                            </div>
                            <div class="error-right">
                                <h2>Oops! Page Not Open</h2>
                                <h4>Nothing Is Found Here!!</h4>
                                <a href="index.jsp">Go Home</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <script src="js/jquery.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/plugins/morris/raphael.min.js"></script>
        <script src="js/plugins/morris/morris.min.js"></script>
        <script src="js/plugins/morris/morris-data.js"></script>
    </body>
</html>