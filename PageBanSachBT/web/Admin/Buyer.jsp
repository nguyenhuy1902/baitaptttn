<%-- 
    Document   : buyer
    Created on : Apr 2, 2018, 12:02:07 AM
    Author     : Windows 10 TIMT
--%>

<%@page import="model.DbtOrder"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Admin.DAO.DAO_Order"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Book Library Admin</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/sb-admin.css" rel="stylesheet">
        <link href="css/plugins/morris.css" rel="stylesheet">
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="css/NewStyleAdd.css" rel="stylesheet" type="text/css">

    </head>
    <body>

        <div id="wrapper">
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <jsp:include page="header.jsp"></jsp:include>
                    <div class="collapse navbar-collapse navbar-ex1-collapse">
                        <ul class="nav navbar-nav side-nav">
                            <li>
                                <a href="index.jsp"><i class="fa fa-fw fa-dashboard"></i>Trang chủ</a>
                            </li>
                            <li>
                                <a href="DataTable.jsp"><i class="fa fa-fw fa-table"></i>Bảng dữ liệu</a>
                            </li>
                            <li>
                                <a href="Account.jsp"><i class="fa fa-fw fa-user"></i>Tài khoản</a>
                            </li>
                            <li>
                                <a href="Book.jsp"><i class="fa fa-fw fa-book"></i>Sách</a>
                            </li>
                            <li  class="active">
                                <a href="Buyer.jsp"><i class="fa fa-fw fa-credit-card"></i>Đơn Đặt Hàng</a>
                            </li>
                            <li>
                                <a href="Chart.jsp"><i class="fa fa-fw fa-bar-chart-o"></i>Thống kê</a>
                            </li>
                        </ul>
                    </div>
                </nav>
                <!--Nguoi Mua-->

                <div id="page-wrapper">

                    <div class="container-fluid">

                        <!-- Page Heading -->
                        <div class="row">
                            <div class="col-lg-12">
                                <h1 class="page-header">
                                    Đơn Đặt Hàng
                                </h1>
                                <ol class="breadcrumb">
                                    <li>
                                        <i class="fa fa-dashboard"></i>  <a href="index.jsp">Trang chủ</a>
                                    </li>
                                    <li class="active">
                                        <i class="fa fa-credit-card"></i> Đơn Đặt Hàng
                                    </li>
                                </ol>
                            </div>
                        </div>
                        <!-- /.row -->
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <input class="form-control" placeholder="Tìm Kiếm">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i> Tìm kiếm</button>
                                </div>
                            </div>
                        </div>
                        <!--NhapThongTin-->
                        <!-- Bang Du Liêu-->
                        <div class="row">
                            <div class="col-lg-12">
                                <h2>Thông tin Đơn Đặt Hàng</h2>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover table-striped">
                                        <thead>
                                            <tr>
                                                <th style="text-align: center;">Mã Hóa Đơn</th>
                                                <th style="text-align: center;">Tên Người Dùng</th>
                                                <th style="width: 25%; text-align: center;">Địa Chỉ</th>
                                                <th style="text-align: center;">Số Điện Thoại</th>
                                                <th style="width: 20%; text-align: center;">Email</th>
                                                <th style="text-align: center;">Trạng Thái</th>
                                                <th style="text-align: center;">Giỏ Hàng</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                        <%
                                            DAO_Order ob = new DAO_Order();
                                            ArrayList<DbtOrder> listOrder = ob.getListOrder();
                                        %>
                                        <% for (DbtOrder i : listOrder) {%>
                                        <tr>
                                            <td><%=i.getOrderId()%></td>
                                            <td><%=i.getOrderCustomerName()%></td>
                                            <td><%=i.getOrderCustomerAddress()%></td>
                                            <td><%=i.getOrderCustomerPhone()%></td>
                                            <td><%=i.getOrderCustomerEmail()%></td>
                                            <td><%=i.getOrderZipCodeId()%></td>
                                            <td style="text-align: center">
                                                <a href="OrderDetail.jsp?Orderid=<%=i.getOrderId()%>" class="btn btn-default btn-xs" title="Chi tiết">
                                                    Chi tiết</i>
                                                </a>
                                            </td>

                                        </tr>
                                        <%}%>    
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!--DeTrong-->
                    <div class="row">
                        <div class="col-lg-12">

                        </div>
                    </div>                
                    <!-- /.row -->

                </div>
                <!-- /.container-fluid -->
                <div class="breadcrumb" style="text-align: center;border: 2px solid black; background: #cdcdcd; ">Design by Nguyễn Quang Huy  </div>

            </div>

        </div>
        <script src="js/jquery.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/plugins/morris/raphael.min.js"></script>
        <script src="js/plugins/morris/morris.min.js"></script>
        <script src="js/plugins/morris/morris-data.js"></script>
    </body>
</html>
