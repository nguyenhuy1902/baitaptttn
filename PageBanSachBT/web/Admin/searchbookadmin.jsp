<%-- 
    Document   : book
    Created on : Apr 2, 2018, 12:01:21 AM
    Author     : Windows 10 TIMT
--%>

<%@page import="Admin.DAO.DAO_Search"%>
<%@page import="Admin.DAO.DAO_Catagory"%>
<%@page import="model.DbtCategory"%>
<%@page import="java.util.ArrayList"%>
<%@page import="model.DbtBook"%>
<%@page import="model.DbtBook"%>
<%@page import="Admin.DAO.DAO_Book"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Search - Book Library Admin</title>
        <link href="Images/hogwarts_logo.png" rel="shortcut icon">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/sb-admin.css" rel="stylesheet">
        <link href="css/plugins/morris.css" rel="stylesheet">
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="css/NewStyleAdd.css" rel="stylesheet" type="text/css">
        <script src="js/jquery.min.js"></script>
        <script src="js/jquery.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/plugins/morris/raphael.min.js"></script>
        <script src="js/plugins/morris/morris.min.js"></script>
        <script src="js/plugins/morris/morris-data.js"></script>
    </head>
    <body>
        <!-- -->
        <%
//            DAO_Book hb = new DAO_Book();
//            ArrayList<DbtBook> listBook = hb.getListBook();
//            DAO_Catagory cd = new DAO_Catagory();
//            ArrayList<DbtCategory> listCat = cd.getListCat();
              DAO_Search dAO_Search = new DAO_Search();
        %>
        <!-- -->
        <div id="wrapper">
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <jsp:include page="header.jsp"></jsp:include>
                    <div class="collapse navbar-collapse navbar-ex1-collapse">
                        <ul class="nav navbar-nav side-nav">
                            <li>
                                <a href="index.jsp"><i class="fa fa-fw fa-dashboard"></i>Trang chủ</a>
                            </li>
                            <li>
                                <a href="DataTable.jsp"><i class="fa fa-fw fa-table"></i>Bảng dữ liệu</a>
                            </li>
                            <li>
                                <a href="Account.jsp"><i class="fa fa-fw fa-user"></i>Tài khoản</a>
                            </li>
                            <li  class="active">
                                <a href="Book.jsp"><i class="fa fa-fw fa-book"></i>Sách</a>
                            </li>
                            <li>
                                <a href="Buyer.jsp"><i class="fa fa-fw fa-credit-card"></i>Đơn Đặt Hàng</a>
                            </li>
                            <li>
                                <a href="Chart.jsp"><i class="fa fa-fw fa-bar-chart-o"></i>Thống kê</a>
                            </li>
                        </ul>
                    </div>
                </nav>
                <!-- Sach-->

                <div id="page-wrapper">

                    <div class="container-fluid">

                        <!-- Page Heading -->
                        <div class="row">
                            <div class="col-lg-12">
                                <h1 class="page-header">
                                    Sách
                                </h1>
                                <ol class="breadcrumb">
                                    <li>
                                        <i class="fa fa-dashboard"></i>  <a href="index.jsp">Trang chủ</a>
                                    </li>
                                    <li class="active">
                                        <i class="fa fa-book"></i> Sách
                                    </li>
                                </ol>
                            </div>
                        </div>
                        <!-- /.row -->
                        <div class="row">
                            <form method="get" action="searchbookadmin.jsp">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <input type="text" name="search" id="search" class="form-control"  placeholder="Tìm Kiếm"/>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <button type="submit" name="search" id="submitButton" class="btn btn-default"><i class="fa fa-search"></i> Tìm kiếm</button>
                                </div>
                            </div>
                            </form>
                        </div>

                        <!--NhapThongTin-->
                        
                    <!-- Bang du lieu-->
                    <div class="row">
                        <div class="col-lg-12">
                            <h2>Thông tin Sách</h2>
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped" id="book_table">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;">Mã Sách</th>
                                            <th style="text-align: center;">Tên Sách</th>
                                            <th style="text-align: center;">Ảnh Sách</th>
                                            <th style="text-align: center;">Tên Tác Giả</th>
                                            <th style="text-align: center;">Giá Sách</th>
                                            <th style="text-align: center;">Số Lượng</th>
                                            <th style="text-align: center;">Mô Tả</th>
                                            <th style="text-align: center;">Năm Xuất Bản</th>
                                            <th style="text-align: center;">Nhà Xuất Bản</th>
                                            <th style="text-align: center;">Nhà Cung Cấp</th>
                                            <th style="width: 3%;text-align: center;">Chức năng</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%if(request.getParameter("search") != null) {
                                            for (DbtBook i : dAO_Search.getDbtBookSearch(request.getParameter("search"))) {%>
                                        <tr>
                                            <td style="text-align: center;"><%=i.getBookId()%></td>
                                            <td style="text-align: center;"><%=i.getBookName()%></td>
                                            <td style="text-align: center;"> <img src="<%=i.getBookImage()%>" width="105px" height="150px"></td>
                                            <td style="text-align: center;"><%=i.getBookWriterName()%></td>
                                            <td style="text-align: center;"><%=i.getBookPrice()%></td>
                                            <td style="text-align: center;"><%=i.getBookQuantity()%></td>
                                            <td style="text-align: justify;"><%=i.getBookDescription()%></td>
                                            <td style="text-align: justify;"><%=i.getBookPublishingYear()%></td>
                                            <td style="text-align: center;"><%=i.getBookPublishingCompany()%></td>
                                            <td style="text-align: center;"><%=i.getBookProvider()%></td>
                                            <td style="text-align: center;" class="text-center">
                                                <a href="EditBook.jsp?bookid=<%=i.getBookId()%>" class="btn btn-xs btn-default" role="button" data-toggle="modal" title="Sửa">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                                <p></p> <p></p> <p></p> <p></p>
                                                <a href="ManageBookServlet?bookid=<%=i.getBookId()%>&action=delBook" class="btn btn-xs btn-danger"  title="Xóa" onclick="return confirm('Bạn có chắc chắn muốn xóa?')">
                                                    <i class="fa fa-times"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <%}}%>                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!--DeTrong-->
                    <div class="row">


                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
                <div class="breadcrumb" style="text-align: center;border: 2px solid black; background: #cdcdcd; ">Design by Nguyễn Quang Huy  </div>
            </div>
        </div>
    </body>
</html>