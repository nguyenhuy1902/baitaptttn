<%-- 
    Document   : Account
    Created on : Apr 1, 2018, 11:50:49 PM
    Author     : Windows 10 TIMT
--%>

<%@page import="Admin.DAO.DAO_Account"%>
<%@page import="model.DbtUser"%>
<%@page import="java.util.ArrayList"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Book Library Admin</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/sb-admin.css" rel="stylesheet">
        <link href="css/plugins/morris.css" rel="stylesheet">
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="css/NewStyleAdd.css.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <!-- -->

        <!-- -->
        <div id="wrapper">
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <jsp:include page="header.jsp"></jsp:include>
                    <div class="collapse navbar-collapse navbar-ex1-collapse">
                        <ul class="nav navbar-nav side-nav">
                            <li>
                                <a href="index.jsp"><i class="fa fa-fw fa-dashboard"></i>Trang chủ</a>
                            </li>
                            <li>
                                <a href="DataTable.jsp"><i class="fa fa-fw fa-table"></i>Bảng dữ liệu</a>
                            </li>
                            <li class="active">
                                <a href="Account.jsp"><i class="fa fa-fw fa-user"></i>Tài khoản</a>
                            </li>
                            <li>
                                <a href="Book.jsp"><i class="fa fa-fw fa-book"></i>Sách</a>
                            </li>
                            <li>
                                <a href="Buyer.jsp"><i class="fa fa-fw fa-credit-card"></i>Đơn Đặt Hàng</a>
                            </li>
                            <li>
                                <a href="Chart.jsp"><i class="fa fa-fw fa-bar-chart-o"></i>Thống kê</a>
                            </li>
                        </ul>
                    </div>
                </nav>
                <!-- Tai Khoan -->

                <div id="page-wrapper">

                    <div class="container-fluid">

                        <!-- Page Heading -->
                        <div class="row">
                            <div class="col-lg-12">
                                <h1 class="page-header">
                                    Tài khoản
                                </h1>
                                <ol class="breadcrumb">
                                    <li>
                                        <i class="fa fa-dashboard"></i>  <a href="index.jsp">Trang chủ</a>
                                    </li>
                                    <li class="active">
                                        <i class="fa fa-user"></i> Tài Khoản
                                    </li>
                                </ol>
                            </div>
                        </div>
                        <!-- /.row -->
                        <!-- TimKiem -->
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <input class="form-control" placeholder="Tìm Kiếm">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i> Tìm kiếm</button>
                                </div>
                            </div>
                        </div>
                        <!--NhapThongTin-->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="row">
                                    <form class="row" method="post" action="ManageAccountServlet?action=addUser">
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label>Mã Người dùng</label>
                                                <input class="form-control" placeholder="Mã tài khoản" type="text" name="userId"  readonly/>
                                            </div>
                                            <div class="form-group">
                                                <label>Email</label>
                                                <input class="form-control" placeholder="Email" type="text" name="userRmail" required/>
                                            </div>  

                                        </div>
                                        <div class="col-lg-4">
                                            
                                            <div class="form-group">
                                                <label>Mật khẩu</label>
                                                <input class="form-control" placeholder="Mật khẩu" type="text" name="userPass" required/>
                                            </div>
                                            <div class="form-group">
                                                <label>Số Điện Thoại</label>
                                                <input class="form-control" placeholder="Địa chỉ" type="text" name="userPhone" pattern="[0-9]{1,}" title="Nhập số!!!" required/>
                                            </div>                                                                                
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label>Địa chỉ</label>
                                                <input class="form-control" placeholder="Địa chỉ" type="text" name="userAdd" required/>
                                            </div>    
                                            
                                        </div>
                                        <div class="col-lg-4">
                                            <label>Thao tác </label>
                                        </div>
                                        <div class="col-lg-2">
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-success " value="Save"><i class="fa fa-plus"></i><a onclick="return confirm('Bạn có chắc chắn Thêm?')" style="color: white"> Thêm </a></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- -->
                        <!--Bang tai khoan -->
                        <div class="row">
                            <div class="col-lg-12">
                                <h2>Thông tin tài khoản</h2>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover table-striped">
                                        <thead>
                                            <tr>
                                                <th style="width: 5%; text-align: center;">Mã Người Dùng</th>
                                                <th style="width: 10%; text-align: center;">Tên Người Dùng</th>
                                                <th style="width: 10%; text-align: center;">Email</th>
                                                <th style="width: 10%; text-align: center;">Mật khẩu</th>
                                                <th style="width: 25%; text-align: center;">Địa chỉ</th>
                                                <th style="width: 10%; text-align: center;">Số điện thoại</th>
                                                <th style="width: 10%;text-align: center;">Chức năng</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <!-- -->
                                        <%
                                            DAO_Account ah = new DAO_Account();
                                            ArrayList<DbtUser> listUser = ah.getListUser();
                                        %>  
                                        <!-- -->
                                        <% for (DbtUser i : ah.getListUser()) {%>
                                        <tr>
                                            <td><%=i.getUserId()%></td>
                                            <td><%=i.getUserName()%></td>
                                            <td><%=i.getUserEmail()%></td>
                                            <td><%=i.getUserPassWord()%></td>
                                            <td><%=i.getUserAddress()%></td>
                                            <td><%=i.getUserRole()%></td>
                                                                                     
                                            <td class="text-center">
                                                <a href="EditAccount.jsp?userId=<%=i.getUserId()%>" class="btn btn-xs btn-default" role="button" title="Sửa">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                                <p></p> <p></p> <p></p> <p></p>
                                                <a href="ManageAccountServlet?userId=<%=i.getUserId()%>&action=delUser" class="btn btn-xs btn-danger" title="Xóa" onclick="return confirm('Bạn có chắc chắn muốn xóa?')">
                                                    <i class="fa fa-times"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <%}%>                  
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- -->
                    <!--DeTrong-->
                    <div class="row">
                        <div class="col-lg-12">

                        </div>
                    </div>
                    <!-- -->
                </div>
                <div class="breadcrumb" style="text-align: center;border: 2px solid black; background: #cdcdcd; ">Design by Nguyễn Quang Huy  </div>
            </div>
        </div>
        <script src="js/jquery.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/plugins/morris/raphael.min.js"></script>
        <script src="js/plugins/morris/morris.min.js"></script>
        <script src="js/plugins/morris/morris-data.js"></script>
    </body>
</html>
