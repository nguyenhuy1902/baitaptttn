<%-- 
    Document   : product
    Created on : Apr 8, 2018, 8:48:56 PM
    Author     : Administrator
--%>


<%@page import="java.util.List"%>
<%@page import="model.DbtCart"%>
<%@page import="model.DbtBook"%>
<%@page import="dao.BookDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>product</title>
        <!-- Bootstrap style --> 
<!--        <link id="callCss" rel="stylesheet" href="themes/bootshop/bootstrap.min.css" media="screen"/>
        <link href="themes/css/base.css" rel="stylesheet" media="screen"/>
         Bootstrap style responsive 	
        <link href="themes/css/bootstrap-responsive.min.css" rel="stylesheet"/>
        <link href="themes/css/font-awesome.css" rel="stylesheet" type="text/css">
         Google-code-prettify 	
        <link href="themes/js/google-code-prettify/prettify.css" rel="stylesheet"/>
         fav and touch icons 

        <script src="themes/js/jquery.js" type="text/javascript"></script>
        <script src="themes/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="themes/js/google-code-prettify/prettify.js"></script>

        <script src="themes/js/bootshop.js"></script>
        <script src="themes/js/jquery.lightbox-0.5.js"></script>-->
    </head>
    <body>
        <%
            BookDAO bookDAO = new BookDAO();
            String dbtCategory = "";
            if (request.getParameter("DbtCategory") != null) {
                dbtCategory = request.getParameter("DbtCategory");
            }

//            DbtCart dbtCart = (DbtCart) session.getAttribute("dbtCart");
//            if (dbtCart == null) {
//                dbtCart = new DbtCart();  
//                session.setAttribute("dbtCart", dbtCart);
//            }

//            List<DbtBook> ds = bookDAO.getAllDbtBookByDbtCategory(Long.parseLong(dbtCategory));
//            List<DbtBook> ds1 = bookDAO.getAllDbtBookByDbtCategoryDetails(Long.parseLong(dbtCategory));
            List<DbtBook> ds = bookDAO.getListBook();
            String page1 = "", page2 = "";

            int start = 0;
            int end;
            if (ds.size() >6) {
                end = 6;
            } else {
                end = ds.size();
            }
            if (request.getParameter("start") != null) {
                page1 = request.getParameter("start");
                start = Integer.parseInt(page1);
            }
            if (request.getParameter("end") != null) {
                page2 = request.getParameter("end");
                end = Integer.parseInt(page2);
            }
            List<DbtBook> list = bookDAO.getListBookByPagination(ds, start, end);

        %>
        <jsp:include page="header.jsp"></jsp:include>

            <div id="mainBody">
                <div class="container">
                    <div class="row">
                    <jsp:include page="menu.jsp"></jsp:include>

                        <div class="span9">
                            <ul class="breadcrumb">
                                <li><a href="index.jsp">Trang chủ</a> <span class="divider">/</span></li>
                                <li class="active">Tác phẩm</li>
                            </ul>
                            
                            <h3> Tên tác phẩm</h3>	
                            
                            <hr class="soft"/>

                            <div id="myTab" class="pull-right">
                                <a href="#blockView" data-toggle="tab"><span class="btn btn-large btn-primary"><i class="icon-th-large"></i></span></a>
                            </div>
                            <br class="clr"/>
                            <div class="tab-content">

                                <div class="tab-pane  active" id="blockView">
                                    <ul class="thumbnails">
                                    <%                                        
                                        for (DbtBook b : bookDAO.getAllDbtBookByDbtCategory(Long.parseLong(dbtCategory))) {
                                    %>
                                    <li class="span3">
                                        <div class="thumbnail">
                                            <a href="product-details.jsp?bookId=<%=b.getBookId()%>"><img src="<%=b.getBookImage()%>" alt=""/></a>
                                            <div class="caption">
                                                <h5><%=b.getBookName()%></h5>
                                                <p> 
                                                    Tác giả: <%=b.getBookWriterName()%>
                                                </p>
                                                <h4 style="text-align:center"><a class="btn" href="product-details.jsp?bookId=<%=b.getBookId()%>"> <i class="icon-zoom-in"></i></a> 
                                                    <a class="btn btn-primary" href="#"><%=b.getBookPrice()%> VNĐ</a></h4>
                                            </div>
                                        </div>
                                    </li>
                                    <%
                                        }
                                    %>

                                    <%
                                        for (DbtBook b : bookDAO.getAllDbtBookByDbtCategoryDetails(Long.parseLong(dbtCategory))) {
                                    %>
                                    <li class="span3">
                                        <div class="thumbnail">
                                            <a href="product-details.jsp?bookId=<%=b.getBookId()%>"><img src="<%=b.getBookImage()%>" alt=""/></a>
                                            <div class="caption">
                                                <h5><%=b.getBookName()%></h5>
                                                <p> 
                                                    Tác giả: <%=b.getBookWriterName()%>
                                                </p>    
                                                <h4 style="text-align:center"><a class="btn" href="product-details.jsp?bookId=<%=b.getBookId()%>"> <i class="icon-zoom-in"></i></a> 
                                                    <a class="btn btn-primary" href="#"><%=b.getBookPrice()%> VNĐ</a></h4>
                                            </div>
                                        </div>
                                    </li>
                                    <%
                                        }
                                    %>
                                </ul>
                                <hr class="soft"/>
                            </div>
                        </div>
                        <div class="pagination">
                            <ul>
                                <li><a href="#">&lsaquo;</a></li>
                                    <%
                                        int a, b;
                                        int limit = ds.size() / 6;
                                        if (limit * 6 < ds.size()) {
                                            limit += 1;
                                        }
                                        for (int i = 1; i <= limit; i++) {
                                            a = (i - 1) * 6;
                                            b = i * 6;
                                            if (b > ds.size()) {
                                                b = ds.size();
                                            }
                                    %>
                                <li><a href="product.jsp?start=<%=a%>&end=<%=b%>"><%=i%></a></li>
                                    <%}%>
                                <li><a href="#">&rsaquo;</a></li>
                            </ul>
                        </div>
                        <br class="clr"/>
                    </div>

                </div>
            </div>
        </div>
        <jsp:include page="footer.jsp"></jsp:include>

    </body>
</html>
