<%-- 
    Document   : cart
    Created on : Apr 8, 2018, 8:34:07 PM
    Author     : Administrator
--%>

<%@page import="GioHang.Book"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="dao.BookDAO"%>
<%@page import="java.util.Map"%>
<%@page import="model.DbtBook"%>
<%@page import="java.util.TreeMap"%>
<%@page import="model.DbtCart"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>cart</title>

        <!-- Bootstrap style --> 
        <link id="callCss" rel="stylesheet" href="themes/bootshop/bootstrap.min.css" media="screen"/>
        <link href="themes/css/base.css" rel="stylesheet" media="screen"/>
        <!-- Bootstrap style responsive -->	
        <link href="themes/css/bootstrap-responsive.min.css" rel="stylesheet"/>
        <link href="themes/css/font-awesome.css" rel="stylesheet" type="text/css">
        <!-- Google-code-prettify -->	
        <link href="themes/js/google-code-prettify/prettify.css" rel="stylesheet"/>
        <!-- fav and touch icons -->
        
        <script src="themes/js/jquery.js" type="text/javascript"></script>
        <!--<script src="themes/js/jquery.js" type="text/javascript"></script>-->
        <script src="themes/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="themes/js/google-code-prettify/prettify.js"></script>

        <script src="themes/js/bootshop.js"></script>
        <script src="themes/js/jquery.lightbox-0.5.js"></script>

    </head>
    <body>
        <%
            DbtCart dbtCart = (DbtCart) session.getAttribute("");
            if (dbtCart == null) {
                dbtCart = new DbtCart();
                session.setAttribute("dbtCart", dbtCart);
            }
//            TreeMap<DbtBook, Integer> list = dbtCart.getCartListBook();
        %>

        <jsp:include page="header.jsp"></jsp:include>

            <div id="mainBody">
                <div class="container">
                    <div class="row">
                        <div class="span9">
                            <ul class="breadcrumb">
                                <li><a href="index.jsp">Trang chủ</a> <span class="divider">/</span></li>
                                <li class="active">Giỏ hàng</li>
                            </ul>
                            <h3> Thông tin giỏ hàng</h3>	
                            <div class="well">
                        
                                <form class="form-horizontal" >

                                    <h4>Giỏ hàng</h4>
                                    <table class="table table-bordered table-condensed">
                                        <thead>
                                            <tr>
                                                <th>Sản phẩm</th>
                                                <th>Miêu tả</th>
                                                <th>Có sẵn</th>
                                                <th>Giá</th>
                                                <th>Số lượng</th>
                                                <th>Tổng tiền</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <%
                                            float total=0;
                                            float to=0;
                                            ArrayList<Book> lis = (ArrayList<Book>) session.getAttribute("CartList");
                                            for (Book b : lis) {
//                                         
                                           total= b.getSoluong()*b.getGia();
                                           to+=total;
                                        %>
                                        <tr>
                                            <td><img width="150" height="135" src="<%=b.getAnh()%>" alt=""></td>
                                            <td><%=b.getTen()%><br>Tác giả: <%=b.getTacgia()%></td>
                                            <td><span class="shopBtn"><span class="icon-ok"></span></span> </td>
                                            <td ><span id="price"><%=b.getGia()%></span></td>
                                            <td>
                                                <input class="span1" style="max-width:34px" placeholder="1" id="appendedInputButtons" size="16" type="text" value="<%=b.getSoluong()%>" disabled="true">
<!--                                                <div class="input-append">
                                                    <button class="btn btn-mini plus" type="button">+</button><button  class="minus btn btn-mini" type="button"> - </button>
                                                </div>-->
&nbsp;&nbsp;&nbsp;<a id="s2" class="btn btn-mini btn-danger" href="<%=request.getContextPath()%>/CartServlet?command=removeItem&bookId=<%=b.getIdBook()%>"><span class="icon-remove"></span></a>
                                                <div>
                                                    
                                                </div>
                                            </td>
                                            <td><label id="total"><%=total%></label></td>
                                        </tr>
                        
                                        <%
                                            }
                                        %>
                                        <tr>
                                            <th colspan="5" class="alignR">Thành tiền:	</th>
                                            <td class="label label-primary"><label id="thanhtien"><%=to%></label></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="control-group">
                                    <div class="controls">
                                        <input type="hidden" name="email_create" value="1">
                                        <input type="hidden" name="is_new_customer" value="1">
                                        <a href="buy.jsp" role="submit" style="padding-right:0"><span class="btn btn-large btn-success" style="margin-left: 80px;">Mua hàng</span></a>
                                    </div>
                                </div>		
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <jsp:include page="footer.jsp"></jsp:include>
        <script>
              $('#s2').on('click', function(){
                                alert("Bạn có chắc chắn muốn xóa sách khỏi giỏ hàng");
                        });
        </script>
    </body>
</html>