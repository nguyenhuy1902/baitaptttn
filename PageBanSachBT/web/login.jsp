<%-- 
    Document   : login
    Created on : Apr 8, 2018, 8:43:15 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>login</title>

        <!-- Bootstrap style --> 
<!--        <link id="callCss" rel="stylesheet" href="themes/bootshop/bootstrap.min.css" media="screen"/>
        <link href="themes/css/base.css" rel="stylesheet" media="screen"/>
         Bootstrap style responsive 	
        <link href="themes/css/bootstrap-responsive.min.css" rel="stylesheet"/>
        <link href="themes/css/font-awesome.css" rel="stylesheet" type="text/css">
         Google-code-prettify 	
        <link href="themes/js/google-code-prettify/prettify.css" rel="stylesheet"/>
         fav and touch icons 

        <script src="themes/js/jquery.js" type="text/javascript"></script>
        <script src="themes/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="themes/js/google-code-prettify/prettify.js"></script>

        <script src="themes/js/bootshop.js"></script>
        <script src="themes/js/jquery.lightbox-0.5.js"></script>-->

    </head>
    <body>
        
        <%
            String userEmail_error="", userPassWord_error="", userEmail="", error="";
            if(request.getAttribute("userEmail_error")!=null){
                userEmail_error = (String) request.getAttribute("userEmail_error");
            }
            if(request.getAttribute("userPassWord_error")!=null){
                userPassWord_error = (String) request.getAttribute("userPassWord_error");
            }
            if(request.getAttribute("userEmail")!=null){
                userEmail = (String) request.getAttribute("userEmail");
            }
            if(request.getAttribute("error")!=null){
                error = (String) request.getAttribute("error");
            }
            %>

        <jsp:include page="header.jsp"></jsp:include>

            <div id="mainBody">
                <div class="container">
                    <div class="row">
                    <jsp:include page="menu.jsp"></jsp:include>

                        <div class="span9">
                            <ul class="breadcrumb">
                                <li><a href="index.jsp">Trang chủ</a> <span class="divider">/</span></li>
                                <li class="active">Đăng nhập</li>
                            </ul>
                            <h3> Đăng nhập</h3>	
                            <div class="well">
                            
                                <form class="form-horizontal" action="UserLoginServlet" method="POST">
                                    <h4>Thông tin địa chỉ Email của bạn</h4>

                                    <div class="control-group">
                                        
                                        <label class="control-label" for="input_email">Email <sup>*</sup></label>
                                        <div class="controls">
                                            
                                        <input type="text" name="userEmail" id="email" value="" >
                                            <span id="user-result"></span>
                                        </div>
                                    </div>	  
                                    <div class="control-group">
                                        <label class="control-label" for="inputPassword1">Mật khẩu <sup>*</sup></label>
                                        <div class="controls">
                                            
                                            <input type="password" name="userPassWord" id="Password" placeholder="">
                                            <span></span>
                                        </div>
                                    </div>
                                    <p style="color: red"><%=error%></p>
                                    <div class="control-group">
                                        <label class="control-label" for="inputPassword1"></label>
                                        <div class="controls">
                                            <span style="font-size: 16px;">Bạn chưa có tài khoản? <a href="register.jsp" style="color: blueviolet;">Bấm vào đây</a></span>
                                        </div>
                                    </div>
<!--                                    <div class="alert alert-block alert-error fade in">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        <strong>Vui lòng điền đầy đủ thông tin</strong>
                                    </div>	-->


                                    <div class="control-group">
                                        <div class="controls">
                                            <input type="hidden" name="email_create" value="1">
                                            <input type="hidden" name="is_new_customer" value="1">
                                            <input type="hidden" value="insert" name="command" />
                                            <input class="btn btn-large btn-success" type="submit" value="Đăng nhập" />
                                        </div>
                                    </div>		
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        <jsp:include page="footer.jsp"></jsp:include>

    </body>
</html>
