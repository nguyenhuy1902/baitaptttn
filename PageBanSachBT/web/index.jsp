<%-- 
    Document   : index
    Created on : Apr 8, 2018, 8:42:47 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>index</title>
<!--         Bootstrap style  
        <link id="callCss" rel="stylesheet" href="themes/bootshop/bootstrap.min.css" media="screen"/>
        <link href="themes/css/base.css" rel="stylesheet" media="screen"/>
         Bootstrap style responsive 	
        <link href="themes/css/bootstrap-responsive.min.css" rel="stylesheet"/>
        <link href="themes/css/font-awesome.css" rel="stylesheet" type="text/css">
         Google-code-prettify 	
        <link href="themes/js/google-code-prettify/prettify.css" rel="stylesheet"/>
         fav and touch icons 

        
        <script src="themes/js/jquery.js" type="text/javascript"></script>
        <script src="themes/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="themes/js/google-code-prettify/prettify.js"></script>
        <link href="themes/js/jquery.min.js" rel="stylesheet"/>
        <script src="themes/js/bootshop.js"></script>
        <script src="themes/js/jquery.lightbox-0.5.js"></script>-->

    </head>
    <body>

        <jsp:include page="header.jsp"></jsp:include>

            <div id="mainBody">
                <div class="container">
                    <div class="row">
                    <jsp:include page="menu.jsp"></jsp:include>

                    <jsp:include page="content.jsp"></jsp:include>
                    </div>
                </div>
            </div>
        <jsp:include page="footer.jsp"></jsp:include>

    </body>
</html>
