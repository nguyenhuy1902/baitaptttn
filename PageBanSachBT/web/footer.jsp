<%-- 
    Document   : footer
    Created on : Apr 8, 2018, 8:41:09 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>footer</title>
    </head>
    <body>

        <div  id="footerSection">
            <div class="container">
                <div class="row">
                    <div class="span3">
                        <h4>Tài khoản</h4>
                        <a style="font-size: 13px;" href="login.jsp">Tài khoản của bạn</a>
                        <a style="font-size: 13px;" href="login.jsp">Thông tin cá nhân</a> 
                        <a style="font-size: 13px;" href="login.jsp">Địa chỉ</a> 
                        <a style="font-size: 13px;" href="login.jsp">Giảm giá</a>  
                        <a style="font-size: 13px;" href="login.jsp">Lịch sử giao dịch</a>
                    </div>
                    <div class="span3">
                        <h4>Thông tin</h4>
                        <a style="font-size: 13px;" href="contact.jsp">Liên hệ</a>  
                        <a style="font-size: 13px;" href="register.jsp">Đăng ký</a>    
                        <a style="font-size: 13px;" href="">Điều khoản và điều kiện</a> 
                        <a style="font-size: 13px;" href="">FAQ</a>
                    </div>
                    <div class="span3">
                        <h4>Dịch vụ của chúng tôi</h4>
                        <a style="font-size: 13px;" href="#">Sản phẩm mới</a> 
                        <a style="font-size: 13px;" href="#">Top sản phẩm</a>  
                        <a style="font-size: 13px;" href="#">Dịch vụ đặc biệt</a>  
                        <a style="font-size: 13px;" href="#">Nhà sản xuất</a> 
                        <a style="font-size: 13px;" href="#">Nhà cung cấp</a> 
                    </div>
                    <div id="socialMedia" class="span3 pull-right">
                        <h4>Truyền thông xã hội </h4>
                        <a href="#"><img width="60" height="60" src="themes/images/facebook.png" title="facebook" alt="facebook"/></a>
                        <a href="#"><img width="60" height="60" src="themes/images/twitter.png" title="twitter" alt="twitter"/></a>
                        <a href="#"><img width="60" height="60" src="themes/images/youtube.png" title="youtube" alt="youtube"/></a>
                    </div> 
                </div>
                <p class="pull-right">&copy; BOOK LIBRARY</p>
            </div><!-- Container End -->
        </div>

    </body>
</html>

