<%-- 
    Document   : contact
    Created on : Apr 8, 2018, 8:38:09 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>contact</title>
<!--         Bootstrap style  -->        
        <link id="callCss" rel="stylesheet" href="themes/bootshop/bootstrap.min.css" media="screen"/>
        <link href="themes/css/base.css" rel="stylesheet" media="screen"/>
         <!--Bootstrap style responsive--> 	
        <link href="themes/css/bootstrap-responsive.min.css" rel="stylesheet"/>
        <link href="themes/css/font-awesome.css" rel="stylesheet" type="text/css">
         <!--Google-code-prettify--> 	
        <link href="themes/js/google-code-prettify/prettify.css" rel="stylesheet"/>
         <!--fav and touch icons--> 

         <script src="themes/js/jquery.js" type="text/javascript"></script>
        <!--<script src="themes/js/jquery.js" type="text/javascript"></script>-->
        <script src="themes/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="themes/js/google-code-prettify/prettify.js"></script>

        <script src="themes/js/bootshop.js"></script>
        <script src="themes/js/jquery.lightbox-0.5.js"></script>
    </head>
    <body>
        <jsp:include page="header.jsp"></jsp:include>

            <div id="mainBody">
                <div class="container">
                    <hr class="soften">
                    <h1>Địa chỉ của chúng tôi</h1>
                    <hr class="soften"/>	
                    <div class="row">
                        <div class="span4">
                            <h4>Chi tiết liên hệ</h4>
                            <p>	Đại học Công Nghiệp Hà Nội,<br/> Đường 32, Minh Khai, Hà Nội
                                <br/><br/>
                                nguyenquanghuy.190297@gmail.com<br/>
                                ﻿ĐT 01635224426<br/>
                                web: booklibrary
                            </p>		
                        </div>

                        <div class="span4">
                            <h4>Email cho chúng tôi</h4>
                            <form class="form-horizontal">
                                <fieldset>
                                    <div class="control-group">

                                        <input type="text" placeholder="tên" class="input-xlarge"/>

                                    </div>
                                    <div class="control-group">

                                        <input type="text" placeholder="email" class="input-xlarge"/>

                                    </div>
                                    <div class="control-group">

                                        <input type="text" placeholder="địa chỉ" class="input-xlarge"/>

                                    </div>
                                    <div class="control-group">
                                        <textarea rows="3" id="thêm thông tin" class="input-xlarge"></textarea>

                                    </div>

                                    <button class="btn btn-large" type="submit">Gửi tin nhắn</button>

                                </fieldset>
                            </form>
                        </div>
                    </div>
                    <div class="row">
                        <div class="span12">
                            <iframe style="width:100%; height:300; border: 0px" scrolling="no" src="https://maps.google.co.uk/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=18+California,+Fresno,+CA,+United+States&amp;aq=0&amp;oq=18+California+united+state&amp;sll=39.9589,-120.955336&amp;sspn=0.007114,0.016512&amp;ie=UTF8&amp;hq=&amp;hnear=18,+Fresno,+California+93727,+United+States&amp;t=m&amp;ll=36.732762,-119.695787&amp;spn=0.017197,0.100336&amp;z=14&amp;output=embed"></iframe><br />
                            <small><a href="https://maps.google.co.uk/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=18+California,+Fresno,+CA,+United+States&amp;aq=0&amp;oq=18+California+united+state&amp;sll=39.9589,-120.955336&amp;sspn=0.007114,0.016512&amp;ie=UTF8&amp;hq=&amp;hnear=18,+Fresno,+California+93727,+United+States&amp;t=m&amp;ll=36.732762,-119.695787&amp;spn=0.017197,0.100336&amp;z=14" style="color:#0000FF;text-align:left">View Larger Map</a></small>
                        </div>
                    </div>
                </div>
            </div>
        <jsp:include page="footer.jsp"></jsp:include>
    </body>
</html>
