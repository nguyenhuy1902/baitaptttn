/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.List;
import model.DbtUser;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import conHibernate.HibernateUtil;

/**
 *
 * @author Administrator
 */
public class UserDAO {
    public void insertUser(DbtUser dbtUser){
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = null;
        try{
            transaction = session.beginTransaction();
            session.save(dbtUser);
            System.out.println("Thêm thành công");
            transaction.commit();
        }
        catch(HibernateException e){
            if(transaction != null)
                transaction.rollback();
            e.printStackTrace();
        }
    }
    
    public DbtUser getUser(int userId){
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = null;
        DbtUser dbtUser = null;
        try{
            transaction = session.beginTransaction();
            dbtUser = (DbtUser) session.createQuery("from DbtUser where userId ='"+userId+"'").uniqueResult();
            transaction.commit();
        }
        catch(HibernateException e){
            if(transaction != null)
                transaction.rollback();
            e.printStackTrace();
        }finally{
            session.close();
        }
        return dbtUser;
    }
    
    public boolean checkLogin(String userEmail, String userPassWord){
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = null;
        List<DbtUser> dbtUser = null;
        try{
            transaction = session.beginTransaction();
            dbtUser =  session.createQuery("from DbtUser where userEmail ='"+userEmail+"' and userPassWord='"+userPassWord+"'").list();
            
            transaction.commit();
            
        }
        catch(HibernateException e){
            if(transaction != null)
                transaction.rollback();
            e.printStackTrace();
        }
        if(dbtUser.size()>=1){
            System.out.println("thanh cong");
                return true;
            }
        else{
            System.out.println("that bai");
        return false;}
    }
    
    public boolean checkUserEmail(String userEmail){
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = null;
        List<DbtUser> dbtUser = null;
        try{
            transaction = session.beginTransaction();
            dbtUser =  session.createQuery("from DbtUser where userEmail ='"+userEmail+"'").list();
            
            transaction.commit();
            
        }
        catch(HibernateException e){
            if(transaction != null)
                transaction.rollback();
            e.printStackTrace();
        }
        if(dbtUser.size()>=1){
            System.out.println("thanh cong");
                return true;
            }
        else{
            System.out.println("that bai");
        return false;}
    }
    
    
    public DbtUser findEmail(String userEmail){
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = null;
       DbtUser dbtUser = null;
        try{
            transaction = session.beginTransaction();
            dbtUser =(DbtUser)  session.createQuery("from DbtUser where userEmail ='"+userEmail+"'").uniqueResult();
            
            transaction.commit();
            return dbtUser;
        }
        catch(HibernateException e){
            if(transaction != null)
                transaction.rollback();
            e.printStackTrace();
            return null;
        }
    }
    public static void main(String[] args) {
       UserDAO h= new UserDAO();
        System.out.println(h.getUser(2).getUserId());
    }
}
