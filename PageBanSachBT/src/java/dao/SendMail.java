/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author tadan
 */
public class SendMail {

    public static boolean sendmail(String emailUser, String body) {

        Properties p = new Properties();
        p.put("mail.smtp.auth", "true");
        p.put("mail.smtp.starttls.enable", "true");
        p.put("mail.smtp.host", "smtp.gmail.com");
        p.put("mail.smtp.port", "587");

        Session s = Session.getInstance(p,
                new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("test08011997@gmail.com", "Test12345678");
            }
        });
        try {
            Message msgs = new MimeMessage(s);
            msgs.setFrom(new InternetAddress("test08011997@gmail.com"));
            msgs.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailUser));
            msgs.setSubject("Xac nhan mua hang");
            msgs.setContent(body,"text/html; charset=UTF-8");
            Transport.send(msgs);
        } catch (MessagingException ex) {
            return false;
        }
        return true;
    }

//    public static void main(String args[]) {
//        new SendMail().sendmail();
//    }
}
