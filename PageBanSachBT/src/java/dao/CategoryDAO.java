/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.ArrayList;
import model.DbtCategory;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import conHibernate.HibernateUtil;
import org.hibernate.SessionFactory;

/**
 *
 * @author Administrator
 */
public class CategoryDAO {
    private final SessionFactory sf = HibernateUtil.getSessionFactory();
    private Session session;
    private Transaction transaction;
    
    public ArrayList<DbtCategory> getListDbtCategory(){
        session = HibernateUtil.getSessionFactory().getCurrentSession();
        session = sf.openSession();
        transaction = session.beginTransaction();
        Query query = session.createQuery("from DbtCategory");
        ArrayList<DbtCategory> list = (ArrayList<DbtCategory>) query.list();
        transaction.commit();
        return list;
    }
    
    public ArrayList<DbtCategory> getCategoryByParent(){
        session = HibernateUtil.getSessionFactory().getCurrentSession();
        session = sf.openSession();
        transaction = session.beginTransaction();
        Query query = session.createQuery("from DbtCategory where categoryParent=0");
        ArrayList<DbtCategory> list = (ArrayList<DbtCategory>) query.list();
        transaction.commit();
        return list;
    }
    
    public ArrayList<DbtCategory> getCategoryByChildrent(int categoryParent){
        session = HibernateUtil.getSessionFactory().getCurrentSession();
        session = sf.openSession();
        transaction = session.beginTransaction();
        Query query = session.createQuery("from DbtCategory where categoryParent='"+categoryParent+"'");
        ArrayList<DbtCategory> list = (ArrayList<DbtCategory>) query.list();
        transaction.commit();
        return list;
    }
    
    public static void main(String[] args) {
        System.out.println(new CategoryDAO().getListDbtCategory().size());
//        System.out.println(new CategoryDAO().getCategoryByParent().size());
    }
}
