/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.mysql.jdbc.PreparedStatement;
import java.util.ArrayList;
import java.util.List;
import model.DbtBook;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import conHibernate.HibernateUtil;
import org.hibernate.SessionFactory;

/**
 *
 * @author Administrator
 */
public class BookDAO {
    private final SessionFactory sf = HibernateUtil.getSessionFactory();
    private Session session;
    private Transaction transaction;
    
    public ArrayList<DbtBook> getAllDbtBookByDbtCategory(long dbtCategoryByCategoryId) {
       // session = HibernateUtil.getSessionFactory().getCurrentSession();
        ArrayList<DbtBook> list;
        session = sf.openSession();
            transaction = session.beginTransaction();
        try {
            
            try {
                Query query = session.createQuery("from DbtBook where dbtCategoryByCategoryId = :dbtCategoryByCategoryId");
                query.setLong("dbtCategoryByCategoryId", dbtCategoryByCategoryId);
                list = (ArrayList<DbtBook>) query.list();
                transaction.commit();
            } catch (Exception ex) {
                transaction.rollback();
                throw ex;
            }
        } catch (Exception ex) {
            throw ex;
        }
        return list;
    }
    public ArrayList<DbtBook> getAllbooknear(int dbtCategoryByCategoryId) {
       // session = HibernateUtil.getSessionFactory().getCurrentSession();
        ArrayList<DbtBook> list;
        session = sf.openSession();
            transaction = session.beginTransaction();
        try {
            
            try {
                Query query = session.createQuery("from DbtBook where dbtCategoryByCategoryId = :dbtCategoryByCategoryId");
                query.setLong("dbtCategoryByCategoryId", dbtCategoryByCategoryId);
                list = (ArrayList<DbtBook>) query.list();
                transaction.commit();
            } catch (Exception ex) {
                transaction.rollback();
                throw ex;
            }
        } catch (Exception ex) {
            throw ex;
        }
        return list;
    }
    public ArrayList<DbtBook> getAllDbtBookByDbtCategoryDetails(long dbtCategoryByCategoryParent) {
       // session = HibernateUtil.getSessionFactory().getCurrentSession();
        ArrayList<DbtBook> list;
         session = sf.openSession();
            transaction = session.beginTransaction();
        try {
           
            try {
                Query query = session.createQuery("from DbtBook where dbtCategoryByCategoryParent = :dbtCategoryByCategoryParent");
                query.setLong("dbtCategoryByCategoryParent", dbtCategoryByCategoryParent);
                list = (ArrayList<DbtBook>) query.list();
                transaction.commit();
            } catch (Exception ex) {
                transaction.rollback();
                throw ex;
            }
        } catch (Exception ex) {
            throw ex;
        }
        return list;
    }
    
        public ArrayList<DbtBook> getDbtBookRandom(){
        //session = HibernateUtil.getSessionFactory().getCurrentSession();
        session = sf.openSession();
        transaction = session.beginTransaction();
        try{
            
            Query query = session.createQuery("from DbtBook order by rand()");
            query.setMaxResults(16);
            ArrayList<DbtBook> list = (ArrayList<DbtBook>) query.list();
            transaction.commit();
            return list;
        }
        catch(RuntimeException e){
            session.getTransaction().rollback();
            throw e;
        }
    }

    public ArrayList<DbtBook> getDbtBookRandom1(){
       // session = HibernateUtil.getSessionFactory().getCurrentSession();
        session = sf.openSession();
        transaction = session.beginTransaction();
        try{
            
            Query query = session.createQuery("from DbtBook order by rand()");
            query.setMaxResults(6);
            ArrayList<DbtBook> list = (ArrayList<DbtBook>) query.list();
            transaction.commit();
            return list;
        }
        catch(RuntimeException e){
            session.getTransaction().rollback();
            throw e;
        }
    }
    
    public ArrayList<DbtBook> getDbtBookRandom2(){
      //  session = HibernateUtil.getSessionFactory().getCurrentSession();
        session = sf.openSession();
        transaction = session.beginTransaction();
        try{
            Query query = session.createQuery("from DbtBook order by rand()");
            query.setMaxResults(2);
            ArrayList<DbtBook> list = (ArrayList<DbtBook>) query.list();
            transaction.commit();
            return list;
        }
        catch(RuntimeException e){
            session.getTransaction().rollback();
            throw e;
        }
    }
    
    
    public DbtBook getDbtBook(int bookId){
       // session = HibernateUtil.getSessionFactory().getCurrentSession();
        session = sf.openSession();
        transaction = session.beginTransaction();
        try{
            Query query = session.createQuery("from DbtBook d where d.bookId = :bookId");
            query.setParameter("bookId", bookId);
            DbtBook dbtBook = (DbtBook) query.uniqueResult();
            transaction.commit();
            return dbtBook;
        }
        catch(RuntimeException e){
            session.getTransaction().rollback();
            throw e;
        }
        
    }
    
    public List<DbtBook> getListBookByPagination(List<DbtBook> list, int start, int end){
        List<DbtBook> result = new ArrayList<DbtBook>();
        for(int i = start; i<end; i++){
            result.add(list.get(i));
        }
        return result;
    }
    
    public List<DbtBook> getListBook(){
        session = HibernateUtil.getSessionFactory().getCurrentSession();
        ArrayList<DbtBook> list;
        try {
            session = sf.openSession();
            transaction = session.beginTransaction();
            try {
                Query query = session.createQuery("from DbtBook");
                list = (ArrayList<DbtBook>) query.list();
                transaction.commit();
            } catch (Exception ex) {
                transaction.rollback();
                throw ex;
            }
        } catch (Exception ex) {
            throw ex;
        }
        return list;
    }
    
    public static void main(String[] args) {
        BookDAO bookDAO = new BookDAO();
        DbtBook dbtBook = bookDAO.getDbtBook(3);
        System.out.println(dbtBook.toString());
        System.out.println(dbtBook.getBookName());
    }
}
