/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import model.DbtOrder;
import model.DbtOrderdetail;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import conHibernate.HibernateUtil;

/**
 *
 * @author Administrator
 */
public class Checkout implements OrderInteface{
    @Override
    public boolean insertorder(DbtOrder dbtOrder){
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = null;
        try{
            transaction = session.beginTransaction();
            session.save(dbtOrder);
            System.out.println("Thêm thành công");
            transaction.commit();
            return true;
        }
        catch(HibernateException e){
            if(transaction != null)
                transaction.rollback();
            e.printStackTrace();
            return false;
        }
    }
    @Override
    public boolean insertorderdetail(DbtOrderdetail dbtOrderdetail){
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = null;
        try{
            transaction = session.beginTransaction();
            session.save(dbtOrderdetail);
            System.out.println("Thêm thành công");
            transaction.commit();
            return true;
        }
        catch(HibernateException e){
            if(transaction != null)
                transaction.rollback();
            e.printStackTrace();
            return false;
        }
    }
}
