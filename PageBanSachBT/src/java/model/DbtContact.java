package model;
// Generated May 30, 2018 3:22:45 PM by Hibernate Tools 4.3.1



/**
 * DbtContact generated by hbm2java
 */
public class DbtContact  implements java.io.Serializable {


     private Integer contactId;
     private String contactFullName;
     private String contactEmail;
     private String contacctTitle;
     private String contactContent;
     private String contactCreateAt;
     private String contactUpdateAt;
     private Integer userId;

    public DbtContact() {
    }

	
    public DbtContact(String contactFullName, String contactEmail) {
        this.contactFullName = contactFullName;
        this.contactEmail = contactEmail;
    }
    public DbtContact(String contactFullName, String contactEmail, String contacctTitle, String contactContent, String contactCreateAt, String contactUpdateAt, Integer userId) {
       this.contactFullName = contactFullName;
       this.contactEmail = contactEmail;
       this.contacctTitle = contacctTitle;
       this.contactContent = contactContent;
       this.contactCreateAt = contactCreateAt;
       this.contactUpdateAt = contactUpdateAt;
       this.userId = userId;
    }
   
    public Integer getContactId() {
        return this.contactId;
    }
    
    public void setContactId(Integer contactId) {
        this.contactId = contactId;
    }
    public String getContactFullName() {
        return this.contactFullName;
    }
    
    public void setContactFullName(String contactFullName) {
        this.contactFullName = contactFullName;
    }
    public String getContactEmail() {
        return this.contactEmail;
    }
    
    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }
    public String getContacctTitle() {
        return this.contacctTitle;
    }
    
    public void setContacctTitle(String contacctTitle) {
        this.contacctTitle = contacctTitle;
    }
    public String getContactContent() {
        return this.contactContent;
    }
    
    public void setContactContent(String contactContent) {
        this.contactContent = contactContent;
    }
    public String getContactCreateAt() {
        return this.contactCreateAt;
    }
    
    public void setContactCreateAt(String contactCreateAt) {
        this.contactCreateAt = contactCreateAt;
    }
    public String getContactUpdateAt() {
        return this.contactUpdateAt;
    }
    
    public void setContactUpdateAt(String contactUpdateAt) {
        this.contactUpdateAt = contactUpdateAt;
    }
    public Integer getUserId() {
        return this.userId;
    }
    
    public void setUserId(Integer userId) {
        this.userId = userId;
    }




}


