/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Admin.Controller;

import Admin.DAO.DAO_Book;
import Admin.DAO.DAO_Catagory;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.DbtBook;
import model.DbtCategory;

/**
 *
 * @author Windows 10 TIMT
 */
@WebServlet(name = "ManageBookServlet", urlPatterns = {"/Admin/ManageBookServlet"})
public class ManageBookServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("utf-8");
        try (PrintWriter out = response.getWriter()) {
            String act = request.getParameter("action");
            String page = "";
            DAO_Book bh = new DAO_Book();
            if (act.equals("delBook")) {
                Integer bookId = Integer.valueOf(request.getParameter("bookid"));
                DbtBook db = bh.thongtin(bookId);
                bh.delBook(db);
                page = "Book.jsp";
                //response.sendRedirect(request.getContextPath() + "Admin/Book.jsp");
            } else if (act.equalsIgnoreCase("addBook")) {
                DbtBook db;
                DAO_Catagory ddd = new DAO_Catagory();
                //db = new DbtBook(Integer.parseInt(request.getParameter("bookid")), bh.getCatName(Integer.valueOf(request.getParameter("catagory"))).getCategoryName(), request.getParameter("bookname"), "themes/images/"+request.getParameter("bookimg"), request.getParameter("bookwriter"), Integer.parseInt(request.getParameter("bookprice")), Integer.parseInt(request.getParameter("bookquan")), request.getParameter("bookdes"), request.getParameter("bookYP"), request.getParameter("bookP"), request.getParameter("bookC"), bh.getCatName(Integer.valueOf(request.getParameter("catagory"))).getCategoryId());
                DbtCategory dbc = ddd.findCat(Integer.valueOf(request.getParameter("catagory")));
                DbtCategory dbcp = ddd.findCat(Integer.valueOf(request.getParameter("catagoryP")));
                db = new DbtBook(dbc, dbcp, request.getParameter("bookname"), "themes/images/" + request.getParameter("bookimg"), request.getParameter("bookwriter"), Integer.parseInt(request.getParameter("bookprice")), Integer.parseInt(request.getParameter("bookquan")), request.getParameter("bookdes"), request.getParameter("bookYP"), request.getParameter("bookC"), request.getParameter("bookP"));
                bh.AddOrEditBook(db);
              //  response.sendRedirect(request.getContextPath() + "Book.jsp");
                page = "Book.jsp";
            } else if (act.equalsIgnoreCase("editBook")) {
                DbtBook db;
                DAO_Catagory ddd = new DAO_Catagory();
                DbtCategory dbc = ddd.findCat(Integer.valueOf(request.getParameter("catagory")));
                DbtCategory dbcp = ddd.findCat(Integer.valueOf(request.getParameter("catagoryP")));
                db = new DbtBook(Integer.parseInt(request.getParameter("bookid")), dbc, dbcp, request.getParameter("bookname"), "themes/images/" + request.getParameter("bookimg"), request.getParameter("bookwriter"), Integer.parseInt(request.getParameter("bookprice")), Integer.parseInt(request.getParameter("bookquan")), request.getParameter("bookdes"), request.getParameter("bookYP"), request.getParameter("bookC"), request.getParameter("bookP"));
                bh.EditBook(db);
                //response.sendRedirect(request.getContextPath() + "Admin/Book.jsp");
                page = "Book.jsp";
            }
            request.getRequestDispatcher(page).forward(request, response);
        } catch (Exception ex) {
            System.out.println("Co loi " + ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
