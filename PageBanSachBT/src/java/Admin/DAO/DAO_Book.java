/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Admin.DAO;

import conHibernate.HibernateUtil;
import java.util.ArrayList;
import model.DbtBook;
import model.DbtCategory;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Windows 10 TIMT
 */
public class DAO_Book {

    Session s;
    Transaction t;

    public ArrayList<DbtBook> getListBook() {
        s = HibernateUtil.getSessionFactory().getCurrentSession();
        t = s.beginTransaction();
        String hql = "from DbtBook";
        Query q = s.createQuery(hql);
        ArrayList<DbtBook> listBook = (ArrayList<DbtBook>) q.list();
        t.commit();
        return listBook;
    }

    public DbtBook thongtin(Integer bookid) {
        try {
            s = HibernateUtil.getSessionFactory().getCurrentSession();
            t = s.beginTransaction();
            DbtBook DBU = (DbtBook) s.get(DbtBook.class, bookid);
            t.commit();
            return DBU;
        } catch (Exception e) {
            return null;
        }
    }

    public DbtCategory getCatName(int catid) {
        try {
            s = HibernateUtil.getSessionFactory().getCurrentSession();
            t = s.beginTransaction();
            DbtCategory DBC = (DbtCategory) s.get(DbtCategory.class, catid);
            t.commit();
            return DBC;
        } catch (Exception e) {
            return null;
        }
    }

    public boolean AddOrEditBook(DbtBook tbook) {

        s = HibernateUtil.getSessionFactory().getCurrentSession();
        t = s.beginTransaction();
        try {
            s.saveOrUpdate(tbook);
            t.commit();
            return true;
        } catch (Exception e) {
            t.rollback();
            return false;
        }

    }

    public boolean EditBook(DbtBook tbook) {
        try {
            s = HibernateUtil.getSessionFactory().getCurrentSession();
            t = s.beginTransaction();
            s.update(tbook);
            t.commit();
            return true;
        } catch (Exception e) {
            t.rollback();
            return false;
        }

    }

    public boolean delBook(DbtBook xbook) {
        try {
            s = HibernateUtil.getSessionFactory().getCurrentSession();
            t = s.beginTransaction();
            s.delete(xbook);
            t.commit();
            return true;
        } catch (Exception e) {
            t.rollback();
            return false;
        }
    }

    public DbtBook findBook(int idbook) {
        try {
            s = HibernateUtil.getSessionFactory().getCurrentSession();
            t = s.beginTransaction();
            DbtBook book;
            String hql = "From DbtBook b where b.bookId = :idbook";
            Query q = s.createQuery(hql);
            q.setInteger("idbook", idbook);
            book = (DbtBook) q.uniqueResult();
            t.commit();
            return book;
        } catch (Exception ex) {
            t.rollback();;
            ex.printStackTrace();
            return null;
        }
    }
}
