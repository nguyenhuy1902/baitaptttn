/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Admin.DAO;

import conHibernate.HibernateUtil;
import java.util.ArrayList;
import model.DbtUser;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Windows 10 TIMT
 */
public class DAO_Account {

    Session s;
    Transaction t;

    public ArrayList<DbtUser> getListUser() {
        s = HibernateUtil.getSessionFactory().getCurrentSession();
        t = s.beginTransaction();
        String hql = "from DbtUser";
        Query q = s.createQuery(hql);
        ArrayList<DbtUser> listUser = (ArrayList<DbtUser>) q.list();
        t.commit();
        return listUser;
    }

    public DbtUser thongtin(Integer userid) {
        try {
            s = HibernateUtil.getSessionFactory().getCurrentSession();
            t = s.beginTransaction();
            DbtUser DBU = (DbtUser) s.get(DbtUser.class, userid);
            t.commit();
            return DBU;
        } catch (Exception e) {
            return null;
        }
    }

    public boolean AddOrEditUser(DbtUser tuser) {
        s = HibernateUtil.getSessionFactory().getCurrentSession();
        t = s.beginTransaction();
        try {
            s.saveOrUpdate(tuser);
            t.commit();
            return true;
        } catch (Exception e) {
            t.rollback();
            return false;
        }
    }

    public boolean EditUser(DbtUser tuser) {
        try {
            s = HibernateUtil.getSessionFactory().getCurrentSession();
            t = s.beginTransaction();
            s.update(tuser);
            t.commit();
            return true;
        } catch (Exception e) {
            t.rollback();
            return false;
        }
    }

    public boolean delUser(DbtUser xuser) {
        s = HibernateUtil.getSessionFactory().getCurrentSession();
        t = s.beginTransaction();
        try {
            s.delete(xuser);
            t.commit();
            return true;
        } catch (Exception e) {
            t.rollback();
            return false;
        }
    }

    public DbtUser findUser(int idUser) {
        try {
            s = HibernateUtil.getSessionFactory().getCurrentSession();
            t = s.beginTransaction();
            DbtUser user;
            String hql = "From DbtUser u where u.userId = :idUser";
            Query q = s.createQuery(hql);
            q.setInteger("idUser", idUser);
            user = (DbtUser) q.uniqueResult();
            t.commit();
            return user;
        } catch (Exception ex) {
            t.rollback();;
            ex.printStackTrace();
            return null;
        }
    }
    public static void main(String[] args) {
        System.out.println(new DAO_Account().getListUser().size());
    }
}
