/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Admin.DAO;

import conHibernate.HibernateUtil;
import dao.SearchDAO;
import java.util.ArrayList;
import model.DbtBook;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Administrator
 */
public class DAO_Search {
    public ArrayList<DbtBook> getDbtBookSearch(String bookName) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        ArrayList<DbtBook> list;
        try {
            Transaction transaction = session.beginTransaction();
            try {
                Query query = session.createQuery("from DbtBook where bookName like '%"+bookName+"%'");
//                query.setString("bookName",bookName+"%");
                list = (ArrayList<DbtBook>) query.list();
                transaction.commit();
            } catch (Exception ex) {
                transaction.rollback();
                throw ex;
            }
        } catch (Exception ex) {
            throw ex;
        }
        return list;
    }
    
    public ArrayList<DbtBook> getDbtBookSearchByWriteName(String bookWriterName) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        ArrayList<DbtBook> list;
        try {
            Transaction transaction = session.beginTransaction();
            try {
                Query query = session.createQuery("from DbtBook where bookWriterName like '%"+bookWriterName+"%'");
//                query.setString("bookName",bookName+"%");
                list = (ArrayList<DbtBook>) query.list();
                transaction.commit();
            } catch (Exception ex) {
                transaction.rollback();
                throw ex;
            }
        } catch (Exception ex) {
            throw ex;
        }
        return list;
    }
    
    
    public static void main(String[] args) {
        System.out.println(new SearchDAO().getDbtBookSearch("đ"));
    }
}
