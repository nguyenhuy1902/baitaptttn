/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Admin.DAO;

import conHibernate.HibernateUtil;
import java.util.ArrayList;
import model.DbtCategory;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Windows 10 TIMT
 */
public class DAO_Catagory {

    Session s;
    Transaction t;

    public ArrayList<DbtCategory> getListCat() {
        s = HibernateUtil.getSessionFactory().getCurrentSession();
        t = s.beginTransaction();
        String hql = "from DbtCategory";
        Query q = s.createQuery(hql);
        ArrayList<DbtCategory> listCat = (ArrayList<DbtCategory>) q.list();
        t.commit();
        return listCat;
    }

    public DbtCategory findCat(int idcat) {
        try {
            s = HibernateUtil.getSessionFactory().getCurrentSession();
            t = s.beginTransaction();
            DbtCategory book;
            String hql = "From DbtCategory b where b.categoryId = :categoryId";
            Query q = s.createQuery(hql);
            q.setInteger("categoryId", idcat);
            book = (DbtCategory) q.uniqueResult();
            t.commit();
            return book;
        } catch (Exception ex) {
            t.rollback();;
            ex.printStackTrace();
            return null;
        }

    }

}
