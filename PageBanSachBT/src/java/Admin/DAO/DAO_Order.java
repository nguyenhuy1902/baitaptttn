/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Admin.DAO;

import conHibernate.HibernateUtil;
import java.util.ArrayList;
import model.DbtOrder;
import model.DbtOrderdetail;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Windows 10 TIMT
 */
public class DAO_Order {

    Session s;
    Transaction t;

    public ArrayList<DbtOrder> getListOrder() {
        s = HibernateUtil.getSessionFactory().getCurrentSession();
        t = s.beginTransaction();
        String hql = "from DbtOrder";
        Query q = s.createQuery(hql);
        ArrayList<DbtOrder> listOrder = (ArrayList<DbtOrder>) q.list();
        t.commit();
        return listOrder;
    }

    public DbtOrder thongtin(Integer orderid) {
        try {
            s = HibernateUtil.getSessionFactory().getCurrentSession();
            t = s.beginTransaction();
            DbtOrder DBU = (DbtOrder) s.get(DbtOrder.class, orderid);
            t.commit();
            return DBU;
        } catch (Exception e) {
            return null;
        }
    }

    public boolean AddOrEditBook(DbtOrder tOrder) {
        try {
            s = HibernateUtil.getSessionFactory().getCurrentSession();
            t = s.beginTransaction();
            s.saveOrUpdate(tOrder);
            t.commit();
            return true;
        } catch (Exception e) {
            t.rollback();
            return false;
        }
    }

    public boolean delBook(DbtOrder xOrder) {
        try {
            s = HibernateUtil.getSessionFactory().getCurrentSession();
            t = s.beginTransaction();
            s.delete(xOrder);
            t.commit();
            return true;
        } catch (Exception e) {
            t.rollback();
            return false;
        }
    }
	
    public boolean EditUser(DbtOrder tOrder) {
        try {
            s = HibernateUtil.getSessionFactory().getCurrentSession();
            t = s.beginTransaction();
            s.update(tOrder);
            t.commit();
            return true;
        } catch (Exception e) {
            t.rollback();
            return false;
        }
    }
    public DbtOrder findOrder(int idbook) {
        try {
            s = HibernateUtil.getSessionFactory().getCurrentSession();
            t = s.beginTransaction();
            DbtOrder book;
            String hql = "From DbtOrder b where b.orderId = :orderId";
            Query q = s.createQuery(hql);
            q.setInteger("orderId", idbook);
            book = (DbtOrder) q.uniqueResult();
            t.commit();
            return book;
        } catch (Exception ex) {
            t.rollback();;
            ex.printStackTrace();
            return null;
        }
    }

  

    public ArrayList<DbtOrderdetail> getListOrderD(int idOrder) {
        s = HibernateUtil.getSessionFactory().getCurrentSession();
        t = s.beginTransaction();
        String hql = "from DbtOrderdetail  where orderId = :orderId";
        Query q = s.createQuery(hql); 
        q.setInteger("orderId", idOrder);
        ArrayList<DbtOrderdetail> listOrderD = (ArrayList<DbtOrderdetail>) q.list();
        t.commit();
        return listOrderD;
    }

    public DbtOrderdetail thongtinOD(Integer orderDid) {
        try {
            s = HibernateUtil.getSessionFactory().getCurrentSession();
            t = s.beginTransaction();
            DbtOrderdetail DBU = (DbtOrderdetail) s.get(DbtOrderdetail.class, orderDid);
            t.commit();
            return DBU;
        } catch (Exception e) {
            return null;
        }
    }
    public static void main(String[] args) {
        System.out.println(new DAO_Order().getListOrder().size());
    }
}
