/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GioHang;

/**
 *
 * @author Administrator
 */
public class Book {
    private int idBook;
    private String ten;
    private String anh;
    private String tacgia;
    private float gia;
    private int soluong;

    public Book() {
    }

    public String getAnh() {
        return anh;
    }

    public void setAnh(String anh) {
        this.anh = anh;
    }

    public String getTacgia() {
        return tacgia;
    }

    public void setTacgia(String tacgia) {
        this.tacgia = tacgia;
    }

    public Book(int idBook, String ten, String anh, String tacgia, float gia, int soluong) {
        this.idBook = idBook;
        this.ten = ten;
        this.anh = anh;
        this.tacgia = tacgia;
        this.gia = gia;
        this.soluong = soluong;
    }    

    public int getSoluong() {
        return soluong;
    }

    public void setSoluong(int soluong) {
        this.soluong = soluong;
    }


    
    public int getIdBook() {
        return idBook;
    }

    public void setIdBook(int idBook) {
        this.idBook = idBook;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public float getGia() {
        return gia;
    }

    public void setGia(float gia) {
        this.gia = gia;
    }
    
}
