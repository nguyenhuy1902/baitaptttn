/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import GioHang.Book;
import dao.BookDAO;
import dao.Checkout;
import dao.SendMail;
import dao.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.DbtOrder;
import model.DbtOrderdetail;
import model.DbtUser;

/**
 *
 * @author Administrator
 */
public class OrderServlet extends HttpServlet {
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        HttpSession session = request.getSession();
        UserDAO dAO = new UserDAO();
        BookDAO bdao = new BookDAO();
        SendMail mail = new SendMail();
        Checkout f = new Checkout();
        String command = request.getParameter("command");
        
        if (command.equals("checkout")) {
            
            String hoten = request.getParameter("hoten");
            String diachi = request.getParameter("dchi");
            String dienthoai = request.getParameter("dthoai");
            String email = request.getParameter("email");
            String thongtin = request.getParameter("thongtin");
            
            DbtUser b = dAO.findEmail((String) session.getAttribute("uEmail"));
            DbtOrder db = new DbtOrder(hoten, diachi, Integer.parseInt(dienthoai), email, thongtin);
            f.insertorder(db);
            
            String h="<div style='float:left;'>Chào bạn, "+b.getUserName()+"<br>Sách đã đặt: </div>";
            ArrayList<Book> lic = (ArrayList<Book>) session.getAttribute("CartList");
            for (Book book : lic) {
                DbtOrderdetail orderdetail=new DbtOrderdetail(book.getIdBook(), db.getOrderId(),String.valueOf(book.getGia()), String.valueOf(book.getSoluong()), String.valueOf(book.getSoluong()*book.getGia()));
                f.insertorderdetail(orderdetail);
                h=h+"<div style='width:50%;float:left;'> <br>"
                        + "<p>Sách: "+bdao.getDbtBook(orderdetail.getBookId()).getBookName()+"</p>"
                        + "<p>Số lượng: "+orderdetail.getOrderdetailQuantity()+"</p>"
                        + "<p>Giá: "+orderdetail.getOrderdetailPrice()+" VNĐ</p>"
                        + "<p>Tổng tiền: "+orderdetail.getOrderdetailTotalMoney()+" VNĐ</p>"
                        + "</div>";
            }
            mail.sendmail(email, h);
        }
        
        String url = "/index.jsp";
        response.sendRedirect(request.getContextPath() + url);
        
    }
    
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
