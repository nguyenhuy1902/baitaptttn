/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.DbtUser;
import org.hibernate.Session;
import org.hibernate.Transaction;
import tools.MD5;

/**
 *
 * @author Administrator
 */
public class UserRegisterServlet extends HttpServlet {

    private UserDAO userDAO = new UserDAO();
//    Session s = HibernateUtil.getSessionFactory().getCurrentSession();
//        Transaction transaction = s.beginTransaction();
   
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        String userName = request.getParameter("userName");
        String userEmail = request.getParameter("userEmail");
        String userPassWord = request.getParameter("userPassWord");
        
        String url = "/register.jsp";
        
        String userEmail_error = "", userPassWord_error="";
        if(userEmail.equals("")){
            userEmail_error += "Vui lòng nhập địa chỉ Email";
        }else{
            if(userDAO.checkUserEmail(userEmail)==true){
                userEmail_error += "Email đã tồn tại";
            }
        }
        if(userPassWord.equals("")){
            userPassWord_error += "Vui lòng nhập mật khẩu";
        }
        
        if(userEmail_error.length()>0){
            request.setAttribute("userEmail_error", userEmail_error);
            request.setAttribute("userEmail", userEmail);
        }
        
        if(userPassWord_error.length()>0){
            request.setAttribute("userPassWord_error", userPassWord_error);
        }
        
        try{
            if(userEmail_error.length()==0 && userPassWord_error.length()==0){
                 HttpSession session = request.getSession();
//                userDAO.insertUser(new DbtUser(userName, userEmail, MD5.encryption(userPassWord)));
                userDAO.insertUser(new DbtUser(userName, userEmail, userPassWord));
                session.setAttribute("uEmail",userEmail);
//                s.save(userEmail);
                url = "/index.jsp";
            }else{
                url = "/register.jsp";
            }
            RequestDispatcher rd = getServletContext().getRequestDispatcher(url);
            rd.forward(request, response);
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
