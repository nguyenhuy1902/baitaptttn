/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import tools.MD5;

/**
 *
 * @author Administrator
 */
public class UserLoginServlet extends HttpServlet {

    private UserDAO userDAO = new UserDAO();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        String command = request.getParameter("command");
        String url = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath();
        if(command.equals("Logout")){
            HttpSession session = request.getSession();
            session.invalidate();
            response.sendRedirect(url+"/index.jsp");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        String userEmail = request.getParameter("userEmail");
        String userPassWord = request.getParameter("userPassWord");

        String error = "";
        if (userEmail.equals("") || userPassWord.equals("")) {
            error += "Vui lòng nhập đầy đủ thông tin";
        } else {
            if (userDAO.checkLogin(userEmail, userPassWord) == false) {
                error += "Tài khoản hoặc mật khẩu không đúng";
            }
        }

        if (error.length() > 0) {
            request.setAttribute("userEmail", userEmail);
            request.setAttribute("error", error);
        }

        String url = "/header.jsp";
        try {
            if (error.length() == 0) {
                HttpSession session = request.getSession();
                session.setAttribute("uEmail", userEmail);
                url = "/index.jsp";
            } else {
                url = "/login.jsp";
            }
            RequestDispatcher rd = getServletContext().getRequestDispatcher(url);
            rd.forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
