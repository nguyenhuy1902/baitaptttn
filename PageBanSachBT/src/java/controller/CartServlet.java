/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import GioHang.Book;
import static com.sun.org.apache.xalan.internal.xsltc.compiler.util.Type.Int;
import dao.BookDAO;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.JOptionPane;
import model.DbtBook;
import model.DbtCart;

/**
 *
 * @author Administrator
 */
public class CartServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    @SuppressWarnings("unchecked")
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");

        HttpSession session = request.getSession();
        ArrayList<Book> li = new ArrayList<>();
        Book b = new Book();
        String command = request.getParameter("command");
        if (command.equals("insertItem")) {
            if (session.getAttribute("uEmail") == null) {   //Chưa đăng nhập thì chuyển sang trang login
                String url = "/login.jsp";
                response.sendRedirect(request.getContextPath() + url);
            } else {
                int bookId = Integer.parseInt(request.getParameter("bookId"));
                String bookName = request.getParameter("bookName");
                String bookWriter = request.getParameter("bookWriter");
                String bookAnh = request.getParameter("bookAnh");
                String bookQuantity = request.getParameter("sluo");
                Float bookPrice = Float.parseFloat(request.getParameter("bookGia"));
                if (session.getAttribute("CartList") == null) {
                    b.setIdBook(bookId);
                    b.setTen(bookName);
                    b.setTacgia(bookWriter);
                    b.setAnh(bookAnh);
                    b.setGia(bookPrice);
                    b.setSoluong(Integer.parseInt(bookQuantity));
                    li.add(b);
                    session.setAttribute("CartList", li);
                } else {
                    li = (ArrayList<Book>) session.getAttribute("CartList");
                    b.setIdBook(bookId);
                    b.setTen(bookName);
                    b.setTacgia(bookWriter);
                    b.setAnh(bookAnh);
                    b.setGia(bookPrice);
                    b.setSoluong(Integer.parseInt(bookQuantity));
                    li.add(b);
                    session.setAttribute("CartList", li);
                }String url = "/cart.jsp";
                response.sendRedirect(request.getContextPath() + url);
            }
        } else if (command.equals("removeItem")) {
            int bookId = Integer.parseInt(request.getParameter("bookId"));
            li = (ArrayList<Book>) session.getAttribute("CartList");
            for (Book book : li) {
                if (book.getIdBook() == bookId) {
                    li.remove(book);
                    break;
                }
            }
            session.setAttribute("CartList", li);
            String url = "/cart.jsp";
            response.sendRedirect(request.getContextPath() + url);
        }
        
        

    }

}
