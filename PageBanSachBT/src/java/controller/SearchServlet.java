/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.mysql.jdbc.Statement;
import dao.SearchDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Administrator
 */
public class SearchServlet extends HttpServlet {

    private final SearchDAO searchDAO = new SearchDAO();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String bookName = request.getParameter("bookName");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");

        String bookName = request.getParameter("bookName");
        String buffer = "";
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:330/pagebansach?zeroDateTimeBehavior=convertToNull", "root", "123456");
            Statement st = (Statement) con.createStatement();
            ResultSet rs = st.executeQuery("from DbtBook where bookName like '%"+bookName+"%'");
            while (rs.next()) {
                buffer = buffer + "'" + rs.getString("bookName") + "',";
            }
            response.getWriter().println(buffer);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

//    @Override
//    public String getServletInfo() {
//        return "Short description";
//    }// </editor-fold>
}
