<%-- 
    Document   : buyer
    Created on : Apr 2, 2018, 12:02:07 AM
    Author     : Windows 10 TIMT
--%>


<%@page import="model.DbtBook"%>
<%@page import="model.DbtOrderdetail"%>
<%@page import="java.util.ArrayList"%>
<%@page import="model.DbtOrder"%>
<%@page import="Admin.DAO.DAO_Book"%>
<%@page import="Admin.DAO.DAO_Order"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>TH ADMIN</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/sb-admin.css" rel="stylesheet">
        <link href="css/plugins/morris.css" rel="stylesheet">
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="css/NewStyleAdd.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <%
            DAO_Order bc = new DAO_Order();
            DAO_Book sad = new DAO_Book();
            DbtOrder order = null;

            if (request.getParameter("Orderid") == null) {
                response.sendRedirect(request.getContextPath() + "/ErrorPage.jsp");
            } else {
                int id = Integer.parseInt(request.getParameter("Orderid"));
                order = bc.findOrder(id);

            }
            if (order == null && request.getParameter("Orderid") != null) {
                response.sendRedirect(request.getContextPath() + "/ErrorPage.jsp");
            } else {
            }
        %>
        <div id="wrapper">
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <jsp:include page="header.jsp"></jsp:include>
                    <div class="collapse navbar-collapse navbar-ex1-collapse">
                        <ul class="nav navbar-nav side-nav">
                            <li>
                                <a href="index.jsp"><i class="fa fa-fw fa-dashboard"></i>Trang chủ</a>
                            </li>
                            <li>
                                <a href="DataTable.jsp"><i class="fa fa-fw fa-table"></i>Bảng dữ liệu</a>
                            </li>
                            <li>
                                <a href="Account.jsp"><i class="fa fa-fw fa-user"></i>Tài khoản</a>
                            </li>
                            <li>
                                <a href="Book.jsp"><i class="fa fa-fw fa-book"></i>Sách</a>
                            </li>
                            <li  class="active">
                                <a href="Buyer.jsp"><i class="fa fa-fw fa-credit-card"></i>Đơn Đặt Hàng</a>
                            </li>
                            <li>
                                <a href="Chart.jsp"><i class="fa fa-fw fa-bar-chart-o"></i>Thống kê</a>
                            </li>
                        </ul>
                    </div>
                </nav>
                <!--Nguoi Mua-->

                <div id="page-wrapper">

                    <div class="container-fluid">

                        <!-- Page Heading -->
                        <div class="row">
                            <div class="col-lg-12">
                                <h1 class="page-header">
                                    Chi Tiết Đơn Đặt Hàng
                                </h1>
                                <ol class="breadcrumb">
                                    <li>
                                        <i class="fa fa-dashboard"></i>  <a href="index.jsp">Trang chủ</a>
                                    </li>
                                    <li>
                                        <i class="fa fa-book"></i><a href="Buyer.jsp">Đơn Đặt Hàng </a>
                                    </li>
                                    <li class="active">
                                        <i class="fa fa-edit"></i>  Chi Tiết Đơn Đặt Hàng
                                    </li>
                                </ol>
                            </div>
                        </div>
                        <!-- /.row -->
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <input class="form-control" placeholder="Tìm Kiếm">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i> Tìm kiếm</button>
                                </div>
                            </div>
                        </div>
                        <!--NhapThongTin-->
                        <div class="row">
                        <%if (order != null) {%>

                        <form class="row" method="post" action="ManageOrderServlet?action=editOrder">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Mã Khách Mua</label>
                                            <input class="form-control" placeholder="Mã khách hàng" type="text" name="orderId" readonly value="<%=order.getOrderId()%>"/>
                                        </div>
                                        <div class="form-group">
                                            <label>Tên Khách Mua</label>
                                            <input class="form-control" placeholder="Tên khách hàng" readonly value="<%=order.getOrderCustomerName()%>"/>
                                        </div>
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input class="form-control" placeholder="Email" readonly value="<%=order.getOrderCustomerEmail()%>"/>
                                        </div>  
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Địa chỉ</label>
                                            <input class="form-control" placeholder="Địa chỉ"readonly value="<%=order.getOrderCustomerAddress()%>"/>
                                        </div>
                                        <div class="form-group">
                                            <label>Số điện thoại</label>
                                            <input class="form-control" placeholder="Số địện thoại"readonly value="<%=order.getOrderCustomerPhone()%>"/>
                                        </div>
                                        <div class="col-lg-12">
                                            <label>Trạng Thái</label>
                                        </div>
                                        <div class="col-lg-4">
                                            <input type="radio" name="trangthai" value="Đã Thanh Toán"> Đã Thanh Toán                                            
                                        </div>
                                        <div class="col-lg-4">
                                            <input type="radio" name="trangthai" value="Chưa Thanh Toán"> Chưa Thanh Toán                                            
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-warning " value="Save"><i class="fa fa-pencil-square"></i> Thanh Toán</button>
                                            </div>                                            
                                        </div>                            
                                    </div>
                                </div>
                            </div>
                        </form>

                        <%}%>   
                    </div>
                    <!-- Bang Du Liêu-->
                    <div class="row">
                        <div class="col-lg-12">
                            <h2>Thông tin Đơn Đặt Hàng</h2>
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;">Mã Đơn Hàng</th>
                                            <th style="text-align: center;">Tên Sách</th>
                                            <th style="text-align: center;">Số Lượng Quyển</th>
                                            <th style="text-align: center;">Giá Sách</th>
                                            <th style="text-align: center;">Tổng Tiền</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%
                                            ArrayList<DbtOrderdetail> listOrder = bc.getListOrderD(Integer.parseInt(request.getParameter("Orderid")));

                                        %>
                                        <%  float total = 0;
                                            float to = 0;
                                            for (DbtOrderdetail i : listOrder) {
                                                DbtBook bo = sad.findBook(i.getBookId());

                                        %>

                                        <tr>
                                            <td><%=i.getOrderdetailId()%></td>
                                            <td><%=bo.getBookName()%></td>
                                            <td><%=i.getOrderdetailQuantity()%></td>
                                            <td><%=i.getOrderdetailPrice()%></td>
                                            <td><%=i.getOrderdetailTotalMoney()%></td>
                                        </tr>  
                                        <%
                                                total = Float.parseFloat(i.getOrderdetailTotalMoney());
                                                to += total;
                                            }
                                        %>
                                        <tr>
                                            <th colspan="4" style="text-align: center;"> Thành tiền</th>
                                            <th><%=to%> VNĐ</th>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!--DeTrong-->
                    <div class="row">
                        <div class="col-lg-12">

                        </div>
                    </div>                
                    <!-- /.row -->

                </div>
                <!-- /.container-fluid -->
                <div class="breadcrumb" style="text-align: center;border: 2px solid black; background: #cdcdcd; ">Design by Nguyễn Quang Huy  </div>

            </div>

        </div>
        <script src="js/jquery.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/plugins/morris/raphael.min.js"></script>
        <script src="js/plugins/morris/morris.min.js"></script>
        <script src="js/plugins/morris/morris-data.js"></script>
    </body>
</html>
