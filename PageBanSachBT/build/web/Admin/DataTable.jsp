<%-- 
    Document   : DataTable
    Created on : Apr 2, 2018, 12:05:04 AM
    Author     : Windows 10 TIMT
--%>

<%@page import="model.DbtUser"%>
<%@page import="DAO.Account_classHiber"%>
<%@page import="java.util.ArrayList"%>
<%@page import="model.DbtBook"%>
<%@page import="DAO.Book_classHiber"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Book Library Admin</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/sb-admin.css" rel="stylesheet">
        <link href="css/plugins/morris.css" rel="stylesheet">
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <%
            Book_classHiber hb = new Book_classHiber();
            ArrayList<DbtBook> listBook = hb.getListBook();
        %>
        <div id="wrapper">
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <jsp:include page="header.jsp"></jsp:include>
                    <div class="collapse navbar-collapse navbar-ex1-collapse">
                        <ul class="nav navbar-nav side-nav">
                            <li>
                                <a href="index.jsp"><i class="fa fa-fw fa-dashboard"></i>Trang chủ</a>
                            </li>
                            <li class="active">
                                <a href="DataTable.jsp"><i class="fa fa-fw fa-table"></i>Bảng dữ liệu</a>
                            </li>
                            <li>
                                <a href="Account.jsp"><i class="fa fa-fw fa-user"></i>Tài khoản</a>
                            </li>
                            <li>
                                <a href="Book.jsp"><i class="fa fa-fw fa-book"></i>Sách</a>
                            </li>
                            <li>
                                <a href="Buyer.jsp"><i class="fa fa-fw fa-credit-card"></i>Đơn Đặt Hàng</a>
                            </li>
                            <li>
                                <a href="Chart.jsp"><i class="fa fa-fw fa-bar-chart-o"></i>Thống kê</a>
                            </li>
                        </ul>
                    </div>
                </nav>
                <!--Bang du Lieu-->

                <div id="page-wrapper">

                    <div class="container-fluid">

                        <!-- Page Heading -->
                        <div class="row">
                            <div class="col-lg-12">
                                <h1 class="page-header">
                                    Bảng dữ liệu
                                </h1>
                                <ol class="breadcrumb">
                                    <li>
                                        <i class="fa fa-dashboard"></i>  <a href="index.jsp">Trang chủ</a>
                                    </li>
                                    <li class="active">
                                        <i class="fa fa-table"></i> Bảng dữ liệu
                                    </li>
                                </ol>
                            </div>
                        </div>
                        <!-- /.row -->

                        <div class="row">
                            <div class="col-lg-12">
                                <h2 style="text-align: center">Thông tin tài khoản</h2>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover table-striped">
                                        <thead>
                                            <tr>
                                                <th style="width: 5%; text-align: center;">Mã Người Dùng</th>
                                                <th style="width: 10%; text-align: center;">Tên Người Dùng</th>
                                                <th style="width: 10%; text-align: center;">Ảnh</th>
                                                <th style="width: 10%; text-align: center;">Tên Tài Khoản</th>
                                                <th style="width: 10%; text-align: center;">Mật khẩu</th>
                                                <th style="width: 25%; text-align: center;">Địa chỉ</th>
                                                <th style="width: 10%; text-align: center;">Số điện thoại</th>
                                                <th style="width: 10%; text-align: center;">Email</th>
                                                <th style="width: 10%;text-align: center;">Chức năng</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <!-- -->
                                        <%
                                            Account_classHiber ah = new Account_classHiber();
                                            //ArrayList<DbtUser> listUser = ah.getListUser();
                                        %>  
                                        <!-- -->
                                        <% for (DbtUser i : ah.getListUser()) {%>
                                        <tr>
                                            <td><%=i.getUserId()%></td>
                                            <td><%=i.getUserName()%></td>
                                            <td><%=i.getUserAvatar()%></td>
                                            <td><%=i.getUserAccount()%></td>
                                            <td><%=i.getUserPassWord()%></td>
                                            <td><%=i.getUserAddress()%></td>
                                            <td><%=i.getUserPhone()%></td>
                                            <td><%=i.getUserEmail()%></td>                                         
                                            <td class="text-center">
                                                <a href="ManageAccountServlet?userId=<%=i.getUserId()%>&action=delUser" class="btn btn-xs btn-danger" title="Xóa" onclick="return confirm('Bạn có chắc chắn muốn xóa?')">
                                                    <i class="fa fa-times"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <%}%>                  
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <h2 style="text-align: center">Thông tin Sách</h2>
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped">
                                    <thead>
                                        <tr>
                                            <th style="">Mã Sách</th>
                                            <th style="">Tên Sách</th>
                                            <th style="">Ảnh Sách</th>
                                            <th style="">Tên Tác Giả</th>
                                            <th style="">Thể loại</th>
                                            <th style="">Giá Sách</th>
                                            <th style="">Số Lượng</th>
                                            <th style="">Mô Tả</th>
                                            <th style="">Trạng thái</th>
                                            <th style="">Năm Xuất Bản</th>
                                            <th style="">Nhà Xuất Bản</th>
                                            <th style="">Nhà Cung Cấp</th>
                                            <th style="width: 3%;text-align: center;">Chức năng</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <% for (DbtBook i : listBook) {%>
                                        <tr>
                                            <td style="text-align: center;"><%=i.getBookId()%></td>
                                            <td style="text-align: center;"><%=i.getBookName()%></td>
                                            <td style="text-align: center;"><%=i.getBookImage()%></td>
                                            <td style="text-align: center;"><%=i.getBookWriterName()%></td>
                                            <td style="text-align: center;"><%=i.getBookCategory()%></td>
                                            <td style="text-align: center;"><%=i.getBookPrice()%></td>
                                            <td style="text-align: center;"><%=i.getBookQuantity()%></td>
                                            <td style="text-align: justify;"><%=i.getBookDescription()%></td>
                                            <td style="text-align: center;"><%=i.getBookStatus()%></td>
                                            <td style="text-align: justify;"><%=i.getBookPublishingYear()%></td>
                                            <td style="text-align: center;"><%=i.getBookPublishingCompany()%></td>
                                            <td style="text-align: center;"><%=i.getBookProvider()%></td>
                                            <td style="text-align: center;" class="text-center">
                                                <!--                                                <a href="" class="btn btn-default btn-xs" title="Sửa">
                                                                                                    <i class="fa fa-pencil"></i>
                                                                                                </a>-->
                                                <a href="#" class="btn btn-xs btn-danger" title="Xóa" onclick="return confirm('Bạn có chắc chắn muốn xóa?')">
                                                    <i class="fa fa-times"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <%}%>                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <h2 style="text-align: center">Thông tin khách mua</h2>
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;">Mã khách</th>
                                            <th style="text-align: center;">Tên Người Dùng</th>
                                            <th style="width: 25%; text-align: center;">Địa Chỉ</th>
                                            <th style="text-align: center;">Số Điện Thoại</th>
                                            <th style="width: 20%; text-align: center;">Email</th>
                                            <th style="text-align: center;">Trạng Thái</th>
                                            <th style="text-align: center;">Chức năng</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>/index.html</td>
                                            <td>1265</td>
                                            <td>32.3%</td>
                                            <td>$321.33</td>
                                            <td>32.3%</td>
                                            <td style="text-align: center">
                                                <input type="checkbox" title="Trạn thái mua">
                                            </td>
                                            <td class="text-center">
                                                <a href="#" class="btn btn-default btn-xs" title="Sửa">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                                <a href="#" class="btn btn-xs btn-danger" title="Xóa" onclick="return confirm('Bạn có chắc chắn muốn xóa?')">
                                                    <i class="fa fa-times"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>/index.html</td>
                                            <td>1265</td>
                                            <td>32.3%</td>
                                            <td>$321.33</td>
                                            <td>32.3%</td>
                                            <td style="text-align: center">
                                                <input type="checkbox" title="Trạn thái mua">
                                            </td>
                                            <td class="text-center">
                                                <a href="#" class="btn btn-default btn-xs" title="Sửa">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                                <a href="#" class="btn btn-xs btn-danger" title="Xóa" onclick="return confirm('Bạn có chắc chắn muốn xóa?')">
                                                    <i class="fa fa-times"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>/index.html</td>
                                            <td>1265</td>
                                            <td>32.3%</td>
                                            <td>$321.33</td>
                                            <td>32.3%</td>
                                            <td style="text-align: center">
                                                <input type="checkbox" title="Trạn thái mua">
                                            </td>
                                            <td class="text-center">
                                                <a href="#" class="btn btn-default btn-xs" title="Sửa">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                                <a href="#" class="btn btn-xs btn-danger" title="Xóa" onclick="return confirm('Bạn có chắc chắn muốn xóa?')">
                                                    <i class="fa fa-times"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>/index.html</td>
                                            <td>1265</td>
                                            <td>32.3%</td>
                                            <td>$321.33</td>
                                            <td>32.3%</td>
                                            <td style="text-align: center">
                                                <input type="checkbox" title="Trạn thái mua">
                                            </td>
                                            <td class="text-center">
                                                <a href="#" class="btn btn-default btn-xs" title="Sửa">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                                <a href="#" class="btn btn-xs btn-danger" title="Xóa" onclick="return confirm('Bạn có chắc chắn muốn xóa?')">
                                                    <i class="fa fa-times"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>/index.html</td>
                                            <td>1265</td>
                                            <td>32.3%</td>
                                            <td>$321.33</td>
                                            <td>32.3%</td>
                                            <td style="text-align: center">
                                                <input type="checkbox" title="Trạn thái mua">
                                            </td>
                                            <td class="text-center">
                                                <a href="#" class="btn btn-default btn-xs" title="Sửa">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                                <a href="#" class="btn btn-xs btn-danger" title="Xóa" onclick="return confirm('Bạn có chắc chắn muốn xóa?')">
                                                    <i class="fa fa-times"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>/index.html</td>
                                            <td>1265</td>
                                            <td>32.3%</td>
                                            <td>$321.33</td>
                                            <td>32.3%</td>
                                            <td style="text-align: center">
                                                <input type="checkbox" title="Trạn thái mua">
                                            </td>
                                            <td class="text-center">
                                                <a href="#" class="btn btn-default btn-xs" title="Sửa">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                                <a href="#" class="btn btn-xs btn-danger" title="Xóa" onclick="return confirm('Bạn có chắc chắn muốn xóa?')">
                                                    <i class="fa fa-times"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>/index.html</td>
                                            <td>1265</td>
                                            <td>32.3%</td>
                                            <td>$321.33</td>
                                            <td>32.3%</td>
                                            <td style="text-align: center">
                                                <input type="checkbox" title="Trạn thái mua">
                                            </td>
                                            <td class="text-center">
                                                <a href="#" class="btn btn-default btn-xs" title="Sửa">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                                <a href="#" class="btn btn-xs btn-danger" title="Xóa" onclick="return confirm('Bạn có chắc chắn muốn xóa?')">
                                                    <i class="fa fa-times"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>/index.html</td>
                                            <td>1265</td>
                                            <td>32.3%</td>
                                            <td>$321.33</td>
                                            <td>32.3%</td>
                                            <td style="text-align: center">
                                                <input type="checkbox" title="Trạn thái mua">
                                            </td>
                                            <td class="text-center">
                                                <a href="#" class="btn btn-default btn-xs" title="Sửa">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                                <a href="#" class="btn btn-xs btn-danger" title="Xóa" onclick="return confirm('Bạn có chắc chắn muốn xóa?')">
                                                    <i class="fa fa-times"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>/index.html</td>
                                            <td>1265</td>
                                            <td>32.3%</td>
                                            <td>$321.33</td>
                                            <td>32.3%</td>
                                            <td style="text-align: center">
                                                <input type="checkbox" title="Trạn thái mua">
                                            </td>
                                            <td class="text-center">
                                                <a href="#" class="btn btn-default btn-xs" title="Sửa">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                                <a href="#" class="btn btn-xs btn-danger" title="Xóa" onclick="return confirm('Bạn có chắc chắn muốn xóa?')">
                                                    <i class="fa fa-times"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>/index.html</td>
                                            <td>1265</td>
                                            <td>32.3%</td>
                                            <td>$321.33</td>
                                            <td>32.3%</td>
                                            <td style="text-align: center">
                                                <input type="checkbox" title="Trạn thái mua">
                                            </td>
                                            <td class="text-center">
                                                <a href="#" class="btn btn-default btn-xs" title="Sửa">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                                <a href="#" class="btn btn-xs btn-danger" title="Xóa" onclick="return confirm('Bạn có chắc chắn muốn xóa?')">
                                                    <i class="fa fa-times"></i>
                                                </a>
                                            </td>
                                        </tr>                                    
                                    </tbody>
                                </table>
                            </div>
                        </div>                        
                    </div>
                    <!-- /.row -->

                    <div class="breadcrumb" style="text-align: center;border: 2px solid black; background: #cdcdcd; ">Design by Đỗ Ngọc Thành &amp; Nguyễn Quang Huy  </div>

                    <!-- /.row -->

                </div>
                <!-- /.container-fluid -->

            </div>

        </div>
        <script src="js/jquery.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/plugins/morris/raphael.min.js"></script>
        <script src="js/plugins/morris/morris.min.js"></script>
        <script src="js/plugins/morris/morris-data.js"></script>
    </body>
</html>