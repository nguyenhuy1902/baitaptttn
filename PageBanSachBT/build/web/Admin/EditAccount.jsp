<%-- 
    Document   : EditAccount
    Created on : May 14, 2018, 10:40:43 AM
    Author     : Windows 10 TIMT
--%>

<%@page import="Admin.DAO.DAO_Account"%>
<%@page import="model.DbtUser"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Book Library Admin</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/sb-admin.css" rel="stylesheet">
        <link href="css/plugins/morris.css" rel="stylesheet">
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="css/NewStyleAdd.css.css" rel="stylesheet" type="text/css">
        <script src="js/jquery.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/plugins/morris/raphael.min.js"></script>
        <script src="js/plugins/morris/morris.min.js"></script>
        <script src="js/plugins/morris/morris-data.js"></script>
    </head>
    <body>
        <%
            DAO_Account ac = new DAO_Account();
            DbtUser user = null;

            if (request.getParameter("userId") == null) {
                response.sendRedirect(request.getContextPath() + "/ErrorPage.jsp");
            } else {
                int id = Integer.parseInt(request.getParameter("userId"));
                user = ac.findUser(id);
            }
            if (user == null && request.getParameter("userId") != null) {
                response.sendRedirect(request.getContextPath() + "/ErrorPage.jsp");
            } else {

            }

        %>
        <div id="wrapper">
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <jsp:include page="header.jsp"></jsp:include>
                    <div class="collapse navbar-collapse navbar-ex1-collapse">
                        <ul class="nav navbar-nav side-nav">
                            <li>
                                <a href="index.jsp"><i class="fa fa-fw fa-dashboard"></i>Trang chủ</a>
                            </li>
                            <li>
                                <a href="DataTable.jsp"><i class="fa fa-fw fa-table"></i>Bảng dữ liệu</a>
                            </li>
                            <li class="active">
                                <a href="Account.jsp"><i class="fa fa-fw fa-user"></i>Tài khoản</a>
                            </li>
                            <li>
                                <a href="Book.jsp"><i class="fa fa-fw fa-book"></i>Sách</a>
                            </li>
                            <li>
                                <a href="Buyer.jsp"><i class="fa fa-fw fa-credit-card"></i>Đơn Đặt Hàng</a>
                            </li>
                            <li>
                                <a href="Chart.jsp"><i class="fa fa-fw fa-bar-chart-o"></i>Thống kê</a>
                            </li>
                        </ul>
                    </div>
                </nav>
                <!-- Tai Khoan -->

                <div id="page-wrapper">

                    <div class="container-fluid">

                        <!-- Page Heading -->
                        <div class="row">
                            <div class="col-lg-12">
                                <h1 class="page-header">
                                    Sửa Thông Tin Tài khoản
                                </h1>
                                <ol class="breadcrumb">
                                    <li>
                                        <i class="fa fa-dashboard"></i>  <a href="index.jsp">Trang chủ</a>
                                    </li>
                                    <li>
                                        <i class="fa fa-user"></i> <a href="Account.jsp">Tài Khoản</a>
                                    </li>
                                    <li class="active">
                                        <i class="fa fa-edit"></i> Sửa Thông Tin Tài Khoản
                                    </li>
                                </ol>
                            </div>
                        </div>
                        <!-- /.row -->
                        <!-- TimKiem -->
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <input class="form-control" placeholder="Tìm Kiếm">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i> Tìm kiếm</button>
                                </div>
                            </div>
                        </div>
                        <!--NhapThongTin-->
                        <div class="row">
                        <%if (user != null) {%>
                        <div class="col-lg-12">
                            <div class="row">
                                <form class="row" method="post" action="ManageAccountServlet?action=editUser">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Mã Người dùng</label>
                                            <input class="form-control" placeholder="Mã tài khoản" type="text" name="userId" value="<%=user.getUserId()%>" readonly/>
                                        </div>
                                        <div class="form-group">
                                            <label>Tên Người Dùng</label>
                                            <input class="form-control" placeholder="Tên tài khoản" type="text" name="userName" required value="<%=user.getUserName()%>"/>
                                        </div>
                                        <div class="form-group">
                                            <label>Ảnh Người Dùng</label>
                                            <input class="form-control" placeholder="Tên tài khoản" type="text" name="userAvatar"  value="<%=user.getUserAvatar()%>"/>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input class="form-control" placeholder="Email" type="text" name="userRmail" required value="<%=user.getUserEmail()%>"/>
                                        </div>  
                                        <div class="form-group">
                                            <label>Mật khẩu</label>
                                            <input class="form-control" placeholder="Mật khẩu" type="text" name="userPass" required value="<%=user.getUserPassWord()%>"/>
                                        </div>
                                        <div class="form-group">
                                            <label>Số Điện Thoại</label>
                                            <input class="form-control" placeholder="Số điện thoại" type="text" name="userPhone" pattern="[0-9]{1,}" title="Nhập số!!!" required value="<%=user.getUserRole()%>"/>
                                        </div>                                                                                
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Địa chỉ</label>
                                            <input class="form-control" placeholder="Địa chỉ" type="text" name="userAdd" required value="<%=user.getUserAddress()%>"/>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Thao tác </label><br>
                                            <button type="submit" class="btn btn-warning " value="Save"><i class="fa fa-pencil-square"></i> Sửa</button>
                                        </div>
                                    </div>
                                </form>
                                <%}%>  
                            </div>
                        </div>
                    </div>
                    <!-- -->
                    <!--Bang tai khoan -->
                    <!-- -->
                    <!--DeTrong-->
                    <div class="row">
                        <div class="col-lg-12">

                        </div>
                    </div>
                    <!-- -->
                </div>
            </div>
        </div>
    </body>
</html>
