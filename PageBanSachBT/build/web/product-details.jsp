<%-- 
    Document   : product-details
    Created on : Apr 8, 2018, 8:45:42 PM
    Author     : Administrator
--%>

<%@page import="model.DbtBook"%>
<%@page import="dao.BookDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>product-details</title>
        <!-- Bootstrap style --> 
        <!--        <link id="callCss" rel="stylesheet" href="themes/bootshop/bootstrap.min.css" media="screen"/>
                <link href="themes/css/base.css" rel="stylesheet" media="screen"/>
                 Bootstrap style responsive 	
                <link href="themes/css/bootstrap-responsive.min.css" rel="stylesheet"/>
                <link href="themes/css/font-awesome.css" rel="stylesheet" type="text/css">
                 Google-code-prettify 	
                <link href="themes/js/google-code-prettify/prettify.css" rel="stylesheet"/>
                 fav and touch icons 
        
                <script src="themes/js/jquery.js" type="text/javascript"></script>
                <script src="themes/js/bootstrap.min.js" type="text/javascript"></script>
                <script src="themes/js/google-code-prettify/prettify.js"></script>
        
                <script src="themes/js/bootshop.js"></script>
                <script src="themes/js/jquery.lightbox-0.5.js"></script>-->
    </head>
    <body>
        <%
            BookDAO bookDAO = new BookDAO();
            DbtBook b = new DbtBook();
            String bookId = "";
            if (request.getParameter("bookId") != null) {
                bookId = request.getParameter("bookId");
                b = bookDAO.getDbtBook(Integer.parseInt(bookId));
            }
            String dbtCategory = "";
            if (request.getParameter("DbtCategory") != null) {
                dbtCategory = request.getParameter("DbtCategory");
            }
        %>
        <jsp:include page="header.jsp"></jsp:include>
            <div id="mainBody">
                <div class="container">
                    <div class="row">
                    <jsp:include page="menu.jsp"></jsp:include>
                        <div class="span9">
                            <ul class="breadcrumb">
                                <li><a href="index.jsp">Trang chủ</a> <span class="divider">/</span></li>
                                <li><a href="product-details.jsp?bookId=<%=b.getBookId()%>">Tác phẩm</a> <span class="divider">/</span></li>
                            <li class="active">Thông tin tác phẩm</li>
                        </ul>	
                        <div class="row">	  
                            <div id="gallery" class="span3">
                                <a href="#" title="<%=b.getBookName()%>">
                                    <img src="<%=b.getBookImage()%>" style="width:100%; height: auto;" alt="<%=b.getBookName()%>"/>
                                </a>

                                <div class="btn-toolbar">
                                    <div class="btn-group">
                                        <span class="btn"><i class="icon-envelope"></i></span>
                                        <span class="btn" ><i class="icon-print"></i></span>
                                        <span class="btn" ><i class="icon-zoom-in"></i></span>
                                        <span class="btn" ><i class="icon-star"></i></span>
                                        <span class="btn" ><i class=" icon-thumbs-up"></i></span>
                                        <span class="btn" ><i class="icon-thumbs-down"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="span6">
                                <h3><%=b.getBookName()%>  </h3>
                                <small><h4>- Tác giả: <%=b.getBookWriterName()%></h4></small>
                                <hr class="soft"/>
                                <form class="form-horizontal qtyFrm" action="<%=request.getContextPath()%>/CartServlet?command=insertItem&bookId=<%=b.getBookId()%>&bookName=<%=b.getBookName()%>&bookWriter=<%=b.getBookWriterName()%>&bookAnh=<%=b.getBookImage()%>&bookGia=<%=b.getBookPrice()%>" method="POST">
                                    <div class="control-group">
                                        <label class="control-label"><span><%=b.getBookPrice()%> VNĐ</span></label>
                                        <div class="controls">
                                            <input type="number" id="sl" name="sluo" class="span1" placeholder="Số lượng" value="1"/>
                                            <input class="btn" type="submit" value="Thêm vào giỏ">
                                        </div>
                                    </div>
                                </form>

                                <hr class="soft clr"/>
                                <p style="font-size: 14px;">
                                    <%=b.getBookDescription()%><br>
                                    
                                    - Trích <%=b.getBookName()%>
                                </p>
                                <a class="btn btn-small pull-right" href="#detail">Chi tiết</a>
                                <br class="clr"/>
                                <a href="#" name="detail"></a>
                                <hr class="soft"/>
                            </div>

                            <div class="span9">
                                <ul id="productDetail" class="nav nav-tabs">
                                    <li class="active"><a href="#home" data-toggle="tab">Thông tin tác phẩm</a></li>
                                    <li><a href="#profile" data-toggle="tab">Tác phẩm liên quan</a></li>
                                </ul>
                                <div id="myTabContent" class="tab-content">
                                    <div class="tab-pane fade active in" id="home">
                                        <h4>Thông tin chi tiết</h4>
                                        <table class="table table-bordered">
                                            <tbody>
                                                <tr class="techSpecRow"><th colspan="2">Thông tin chi tiết tác phẩm</th></tr>
                                                <tr class="techSpecRow"><td class="techSpecTD1">Tên sách: </td><td class="techSpecTD2"><%=b.getBookName()%></td></tr>
                                                <tr class="techSpecRow"><td class="techSpecTD1">Tác giả:</td><td class="techSpecTD2"><%=b.getBookWriterName()%></td></tr>
                                                <tr class="techSpecRow"><td class="techSpecTD1">Năm xuất bản:</td><td class="techSpecTD2"> <%=b.getBookPublishingYear()%></td></tr>
                                                <tr class="techSpecRow"><td class="techSpecTD1">Nhà xuất bản:</td><td class="techSpecTD2"><%=b.getBookPublishingCompany()%></td></tr>
                                                <tr class="techSpecRow"><td class="techSpecTD1">Nhà cung cấp:</td><td class="techSpecTD2"><%=b.getBookProvider()%></td></tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="tab-pane fade" id="profile">
                                        <div id="myTab" class="pull-right">
                                            <a href="#blockView" data-toggle="tab"><span class="btn btn-large btn-primary"><i class="icon-th-large"></i></span></a>
                                        </div>
                                        <br class="clr"/>
                                        <hr class="soft"/>
                                        <div class="tab-content">

                                            <div class="tab-pane active" id="blockView">
                                                <ul class="thumbnails">
                                                    <% int idcate =b.getDbtCategoryByCategoryId().getCategoryId();
                                                         for(DbtBook booknear : bookDAO.getAllbooknear(idcate)){
                                                    %>
                                                    <li class="span3">
                                                        <div class="thumbnail">
                                                            <a href="product-details.jsp?bookId=<%=booknear.getBookId()%>"><img src="<%=booknear.getBookImage()%>" alt=""/></a>
                                                            <div class="caption">
                                                                <h5><%= booknear.getBookName() %></h5>
                                                                <p> 
                                                                    Tác giả: <%=booknear.getBookWriterName()%>
                                                                </p>
                                                                <h4 style="text-align:center"><a class="btn" href="product-details.jsp?bookId=<%=booknear.getBookId()%>"> <i class="icon-zoom-in"></i></a> <a class="btn btn-primary" href="#"><%=booknear.getBookPrice()%></a></h4>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <%}%>
                                                </ul>
                                                <hr class="soft"/>
                                                
                                            </div>
                                        </div>
                                        <br class="clr">
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <jsp:include page="footer.jsp"></jsp:include>
        <script>
            $('#sl').on('change', function () {
                var f = $('#sl').val();
                if (f <= 0) {
                    alert("Số lượng sách chọn phải lớn hơn 0");
                    $('#sl').val(1);
                }
            });

        </script>
    </body>
</html>
