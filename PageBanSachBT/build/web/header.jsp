<%-- 
    Document   : header
    Created on : Apr 8, 2018, 8:41:48 PM
    Author     : Administrator
--%>

<%@page import="java.text.NumberFormat"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="dao.BookDAO"%>
<%@page import="model.DbtBook"%>
<%@page import="dao.SearchDAO"%>
<%@page import="model.DbtUser"%>
<%@page import="dao.UserDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>header</title>
        <link id="callCss" rel="stylesheet" href="themes/bootshop/bootstrap.min.css" media="screen"/>
        <link href="themes/css/base.css" rel="stylesheet" media="screen"/>
         <!--Bootstrap style responsive--> 	
        <link href="themes/css/bootstrap-responsive.min.css" rel="stylesheet"/>
        <link href="themes/css/font-awesome.css" rel="stylesheet" type="text/css">
         <!--Google-code-prettify--> 	
        <link href="themes/js/google-code-prettify/prettify.css" rel="stylesheet"/>
         <!--fav and touch icons--> 
         
         <script src="themes/js/jquery.js" type="text/javascript"></script>
        <!--<script src="themes/js/jquery.js" type="text/javascript"></script>-->
        <script src="themes/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="themes/js/google-code-prettify/prettify.js"></script>
        <!--<link href="themes/js/jquery.min.js" rel="stylesheet"/>-->
        <script src="themes/js/bootshop.js"></script>
        <script src="themes/js/jquery.lightbox-0.5.js"></script>
               <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js">
 </script>
<script type="text/javascript">
function showData(value){ 
$.ajax({
    url : "SearchServlet?bookName="+value,
    type : "POST",
    async : false,
    success : function(data) {
//Do something with the data here
    }
});
}
</script>
    </head>
    <body>


        <%
            String userEmail_error = "", userPassWord_error = "", userEmail = "", error = "";
            if (request.getAttribute("userEmail_error") != null) {
                userEmail_error = (String) request.getAttribute("userEmail_error");
            }
            if (request.getAttribute("userPassWord_error") != null) {
                userPassWord_error = (String) request.getAttribute("userPassWord_error");
            }
            if (request.getAttribute("userEmail") != null) {
                userEmail = (String) request.getAttribute("userEmail");
            }
            if (request.getAttribute("error") != null) {
                error = (String) request.getAttribute("error");
            }

            BookDAO bookDAO = new BookDAO();
            SearchDAO searchDAO = new SearchDAO();
            List<DbtBook> list = new ArrayList<DbtBook>();
            list = bookDAO.getListBook();
            String bookName = "";
            String categoryName = "";
            if (request.getParameter("DbtBook") != null) {
                bookName = request.getParameter("DbtBook");
            }
            if (request.getParameter("DbtBook") != null) {
                categoryName = request.getParameter("DbtBook");
            }
            NumberFormat nf = NumberFormat.getInstance();
            nf.setMinimumFractionDigits(0);

//            if(request.getParameter("error")!=null){
//                error = (String) request.getAttribute("error");

        %>
        <%            String err = "";
            if (request.getAttribute("err") != null) {
                err = (String) request.getAttribute("err");
        %>
        <h3><%=err%></h3>
        <%
            }
        %>
        <%
//            if (bookDAO.searchList(bookName, categoryName).size() == 0 & err == "") {%>
        <%
//            }
        %>
        <div id="header">
            <div class="container">
                <div id="welcomeLine" class="row">
                    <div class="span6">

                    </div>
                    <div class="span6">

                    </div>
                </div>
                <!-- Navbar ================================================== -->
                <div id="logoArea" class="navbar">

                    <a id="smallScreen" data-target="#topMenu" data-toggle="collapse" class="btn btn-navbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                    <div class="navbar-inner">

                        <a class="brand" href="index.jsp"><h2>Book Library</h2></a>
                        <form class="form-inline navbar-search" method="get" action="searchbook.jsp">
                            <input name="srchFld" id="srchFld" placeholder="Tìm kiếm" type="text" onkeyup="showData(this.value)" />
                            <div id="showBookArea" style="position: absolute;width: 220px;background-color: white;height: 50px; display: none" >
                              
                            </div>

                            <button type="submit" name="srchFld" id="submitButton" class="btn btn-primary">Go</button>
                        </form>

                        <ul id="topMenu" class="nav pull-right">
                            <li class="">
                                <a href="contact.jsp" role="button" style="padding-right:0"><span class="btn btn-large btn-success">Liên hệ</span></a>
                            <li class="">
                                <a href="cart.jsp" role="button" style="padding-right:0"><span class="btn btn-large btn-success">Giỏ hàng</span></a>
                                <%  if (session.getAttribute("uEmail") != null) {
                                %>
                            <li class="">
                                <a role="button" style="padding-right:0"><span class="btn btn-large btn-success">Xin chào, <%=session.getAttribute("uEmail")%></span></a>
                            </li>
                            <li><a href="UserLoginServlet?command=Logout">Đăng xuất</a></li>
                                <%} else {%>
                            <li class="">
                                <a href="login.jsp" role="button" style="padding-right:0"><span class="btn btn-large btn-success">Đăng nhập</span></a>
                            </li>
                            <%}%>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
                            
    </body>
</html>
