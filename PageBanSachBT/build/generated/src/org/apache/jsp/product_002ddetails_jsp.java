package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import model.DbtBook;
import dao.BookDAO;

public final class product_002ddetails_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>product-details</title>\n");
      out.write("        <!-- Bootstrap style --> \n");
      out.write("<!--        <link id=\"callCss\" rel=\"stylesheet\" href=\"themes/bootshop/bootstrap.min.css\" media=\"screen\"/>\n");
      out.write("        <link href=\"themes/css/base.css\" rel=\"stylesheet\" media=\"screen\"/>\n");
      out.write("         Bootstrap style responsive \t\n");
      out.write("        <link href=\"themes/css/bootstrap-responsive.min.css\" rel=\"stylesheet\"/>\n");
      out.write("        <link href=\"themes/css/font-awesome.css\" rel=\"stylesheet\" type=\"text/css\">\n");
      out.write("         Google-code-prettify \t\n");
      out.write("        <link href=\"themes/js/google-code-prettify/prettify.css\" rel=\"stylesheet\"/>\n");
      out.write("         fav and touch icons \n");
      out.write("\n");
      out.write("        <script src=\"themes/js/jquery.js\" type=\"text/javascript\"></script>\n");
      out.write("        <script src=\"themes/js/bootstrap.min.js\" type=\"text/javascript\"></script>\n");
      out.write("        <script src=\"themes/js/google-code-prettify/prettify.js\"></script>\n");
      out.write("\n");
      out.write("        <script src=\"themes/js/bootshop.js\"></script>\n");
      out.write("        <script src=\"themes/js/jquery.lightbox-0.5.js\"></script>-->\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        ");

            BookDAO bookDAO = new BookDAO();
            DbtBook b = new DbtBook();
            String bookId = "";
            if(request.getParameter("bookId")!=null){
                bookId = request.getParameter("bookId");
                b = bookDAO.getDbtBook(Integer.parseInt(bookId));
            }
            String dbtCategory = "";
            if (request.getParameter("DbtCategory") != null) {
                dbtCategory = request.getParameter("DbtCategory");
            }
            
      out.write("\n");
      out.write("        ");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "header.jsp", out, false);
      out.write("\n");
      out.write("            <div id=\"mainBody\">\n");
      out.write("                <div class=\"container\">\n");
      out.write("                    <div class=\"row\">\n");
      out.write("                    ");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "menu.jsp", out, false);
      out.write("\n");
      out.write("                    <div class=\"span9\">\n");
      out.write("                        <ul class=\"breadcrumb\">\n");
      out.write("                            <li><a href=\"index.jsp\">Trang chủ</a> <span class=\"divider\">/</span></li>\n");
      out.write("                            <li><a href=\"product-details.jsp?bookId=");
      out.print(b.getBookId());
      out.write("\">Tác phẩm</a> <span class=\"divider\">/</span></li>\n");
      out.write("                            <li class=\"active\">Thông tin tác phẩm</li>\n");
      out.write("                        </ul>\t\n");
      out.write("                        <div class=\"row\">\t  \n");
      out.write("                            <div id=\"gallery\" class=\"span3\">\n");
      out.write("                                <a href=\"#\" title=\"");
      out.print(b.getBookName());
      out.write("\">\n");
      out.write("                                    <img src=\"");
      out.print(b.getBookImage());
      out.write("\" style=\"width:100%; height: auto;\" alt=\"");
      out.print(b.getBookName());
      out.write("\"/>\n");
      out.write("                                </a>\n");
      out.write("\n");
      out.write("                                <div class=\"btn-toolbar\">\n");
      out.write("                                    <div class=\"btn-group\">\n");
      out.write("                                        <span class=\"btn\"><i class=\"icon-envelope\"></i></span>\n");
      out.write("                                        <span class=\"btn\" ><i class=\"icon-print\"></i></span>\n");
      out.write("                                        <span class=\"btn\" ><i class=\"icon-zoom-in\"></i></span>\n");
      out.write("                                        <span class=\"btn\" ><i class=\"icon-star\"></i></span>\n");
      out.write("                                        <span class=\"btn\" ><i class=\" icon-thumbs-up\"></i></span>\n");
      out.write("                                        <span class=\"btn\" ><i class=\"icon-thumbs-down\"></i></span>\n");
      out.write("                                    </div>\n");
      out.write("                                </div>\n");
      out.write("                            </div>\n");
      out.write("                            <div class=\"span6\">\n");
      out.write("                                <h3>");
      out.print(b.getBookName());
      out.write("  </h3>\n");
      out.write("                                <small><h4>- Tác giả: ");
      out.print(b.getBookWriterName());
      out.write("</h4></small>\n");
      out.write("                                <hr class=\"soft\"/>\n");
      out.write("                                <form class=\"form-horizontal qtyFrm\" action=\"");
      out.print(request.getContextPath());
      out.write("/CartServlet?command=insertItem&bookId=");
      out.print(b.getBookId());
      out.write("&bookName=");
      out.print(b.getBookName());
      out.write("&bookWriter=");
      out.print(b.getBookWriterName());
      out.write("&bookAnh=");
      out.print(b.getBookImage());
      out.write("&bookGia=");
      out.print(b.getBookPrice());
      out.write("\" method=\"POST\">\n");
      out.write("                                    <div class=\"control-group\">\n");
      out.write("                                        <label class=\"control-label\"><span>");
      out.print(b.getBookPrice());
      out.write(" VNĐ</span></label>\n");
      out.write("                                        <div class=\"controls\">\n");
      out.write("                                            <input type=\"number\" id=\"sl\" name=\"sluo\" class=\"span1\" placeholder=\"Số lượng\" value=\"1\"/>\n");
      out.write("                                            <!--<button type=\"submit\" class=\"btn btn-large btn-primary pull-right\"><a href=\"CartServlet?command=insertItem&bookId=");
      out.print(b.getBookId());
      out.write("&cartId=");
      out.print(System.currentTimeMillis());
      out.write("\">Thêm</a>  <i class=\" icon-shopping-cart\"></i></button>-->\n");
      out.write("                                            <!--<a href=\"buy.jsp\" role=\"submit\" style=\"padding-right:0\"><span class=\"btn btn-large btn-success\" style=\"margin-left: 80px;\">Mua ngay</span></a>-->\n");
      out.write("                                            <input class=\"btn\" type=\"submit\" value=\"Thêm vào giỏ\">\n");
      out.write("                                            <!--<i class=\"icon-shopping-cart\"></i>-->\n");
      out.write("                                        </div>\n");
      out.write("                                    </div>\n");
      out.write("                                </form>\n");
      out.write("\n");
      out.write("                                <hr class=\"soft clr\"/>\n");
      out.write("                                <p style=\"font-size: 14px;\">\n");
      out.write("                                    ");
      out.print(b.getBookDescription());
      out.write("\n");
      out.write("                                    - Trích ");
      out.print(b.getBookName());
      out.write("\n");
      out.write("                                </p>\n");
      out.write("                                <a class=\"btn btn-small pull-right\" href=\"#detail\">Chi tiết</a>\n");
      out.write("                                <br class=\"clr\"/>\n");
      out.write("                                <a href=\"#\" name=\"detail\"></a>\n");
      out.write("                                <hr class=\"soft\"/>\n");
      out.write("                            </div>\n");
      out.write("\n");
      out.write("                            <div class=\"span9\">\n");
      out.write("                                <ul id=\"productDetail\" class=\"nav nav-tabs\">\n");
      out.write("                                    <li class=\"active\"><a href=\"#home\" data-toggle=\"tab\">Thông tin tác phẩm</a></li>\n");
      out.write("                                    <li><a href=\"#profile\" data-toggle=\"tab\">Tác phẩm liên quan</a></li>\n");
      out.write("                                </ul>\n");
      out.write("                                <div id=\"myTabContent\" class=\"tab-content\">\n");
      out.write("                                    <div class=\"tab-pane fade active in\" id=\"home\">\n");
      out.write("                                        <h4>Thông tin chi tiết</h4>\n");
      out.write("                                        <table class=\"table table-bordered\">\n");
      out.write("                                            <tbody>\n");
      out.write("                                                <tr class=\"techSpecRow\"><th colspan=\"2\">Thông tin chi tiết tác phẩm</th></tr>\n");
      out.write("                                                <tr class=\"techSpecRow\"><td class=\"techSpecTD1\">Tên sách: </td><td class=\"techSpecTD2\">");
      out.print(b.getBookName());
      out.write("</td></tr>\n");
      out.write("                                                <tr class=\"techSpecRow\"><td class=\"techSpecTD1\">Tác giả:</td><td class=\"techSpecTD2\">");
      out.print(b.getBookWriterName());
      out.write("</td></tr>\n");
      out.write("                                                \n");
      out.write("                                                <tr class=\"techSpecRow\"><td class=\"techSpecTD1\">Năm xuất bản:</td><td class=\"techSpecTD2\"> ");
      out.print(b.getBookPublishingYear());
      out.write("</td></tr>\n");
      out.write("                                                <tr class=\"techSpecRow\"><td class=\"techSpecTD1\">Nhà xuất bản:</td><td class=\"techSpecTD2\">");
      out.print(b.getBookPublishingCompany());
      out.write("</td></tr>\n");
      out.write("                                                <tr class=\"techSpecRow\"><td class=\"techSpecTD1\">Nhà cung cấp:</td><td class=\"techSpecTD2\">");
      out.print(b.getBookProvider());
      out.write("</td></tr>\n");
      out.write("                                            </tbody>\n");
      out.write("                                        </table>\n");
      out.write("\n");
      out.write("                                    </div>\n");
      out.write("                                    <div class=\"tab-pane fade\" id=\"profile\">\n");
      out.write("                                        <div id=\"myTab\" class=\"pull-right\">\n");
      out.write("                                            <a href=\"#blockView\" data-toggle=\"tab\"><span class=\"btn btn-large btn-primary\"><i class=\"icon-th-large\"></i></span></a>\n");
      out.write("                                        </div>\n");
      out.write("                                        <br class=\"clr\"/>\n");
      out.write("                                        <hr class=\"soft\"/>\n");
      out.write("                                        <div class=\"tab-content\">\n");
      out.write("\n");
      out.write("                                            <div class=\"tab-pane active\" id=\"blockView\">\n");
      out.write("                                                <ul class=\"thumbnails\">\n");
      out.write("                                                    ");
                                        
                                                        for (DbtBook db : bookDAO.getAllDbtBookByDbtCategory(Long.parseLong(dbtCategory))) {
//                                        
                                                    
      out.write("\n");
      out.write("                                    <li class=\"span3\">\n");
      out.write("                                        <div class=\"thumbnail\">\n");
      out.write("                                            <a href=\"product-details.jsp?bookId=");
      out.print(db.getBookId());
      out.write("\"><img src=\"");
      out.print(db.getBookImage());
      out.write("\" alt=\"\"/></a>\n");
      out.write("                                            <div class=\"caption\">\n");
      out.write("                                                <h5>");
      out.print(db.getBookName());
      out.write("</h5>\n");
      out.write("                                                <p> \n");
      out.write("                                                    Tác giả: ");
      out.print(db.getBookWriterName());
      out.write("\n");
      out.write("                                                </p>\n");
      out.write("                                                <h4 style=\"text-align:center\"><a class=\"btn\" href=\"product-details.jsp?bookId=");
      out.print(db.getBookId());
      out.write("\"> <i class=\"icon-zoom-in\"></i></a> \n");
      out.write("                                                    <a class=\"btn btn-primary\" href=\"#\">");
      out.print(db.getBookPrice());
      out.write(" VNĐ</a></h4>\n");
      out.write("                                            </div>\n");
      out.write("                                        </div>\n");
      out.write("                                    </li>\n");
      out.write("                                    ");

                                        }
                                    
      out.write("\n");
      out.write("<!--                                                    <li class=\"span3\">\n");
      out.write("                                                        <div class=\"thumbnail\">\n");
      out.write("                                                            <a href=\"product_details.html\"><img src=\"themes/images/luocsuvanvat.jpg\" alt=\"\"/></a>\n");
      out.write("                                                            <div class=\"caption\">\n");
      out.write("                                                                <h5>Lược sử vạn vật</h5>\n");
      out.write("                                                                <p> \n");
      out.write("                                                                    Lược sử vạn vật là cuốn sách phổ biến khoa học trình bày một cách ngắn gọn lịch sử nghiên cứu khoa học tự nhiên, những thành tựu khoa học trong các lĩnh vực khoa học tự nhiên chính: vật lý, hóa học, sinh học, địa chất, thiên văn… với nhiều tên tuổi, giai thoại và sự thật. \n");
      out.write("                                                                </p>\n");
      out.write("                                                                <h4 style=\"text-align:center\"><a class=\"btn\" href=\"product_details.html\"> <i class=\"icon-zoom-in\"></i></a> <a class=\"btn\" href=\"#\">Thêm vào <i class=\"icon-shopping-cart\"></i></a> <a class=\"btn btn-primary\" href=\"#\">20.000 VNĐ</a></h4>\n");
      out.write("                                                            </div>\n");
      out.write("                                                        </div>\n");
      out.write("                                                    </li>-->\n");
      out.write("                                                    \n");
      out.write("                                                </ul>\n");
      out.write("                                                <hr class=\"soft\"/>\n");
      out.write("                                            </div>\n");
      out.write("                                        </div>\n");
      out.write("                                        <br class=\"clr\">\n");
      out.write("                                    </div>\n");
      out.write("                                </div>\n");
      out.write("                            </div>\n");
      out.write("\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("                    ");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "footer.jsp", out, false);
      out.write("\n");
      out.write("                    <script>\n");
      out.write("                        $('#sl').on('change', function(){\n");
      out.write("                            var f=$('#sl').val();\n");
      out.write("                            if(f<0){\n");
      out.write("                                alert(\"Số lượng sách chọn phải lớn hơn 0\");\n");
      out.write("                                $('#sl').val(1);\n");
      out.write("                            }\n");
      out.write("                        });\n");
      out.write("                        \n");
      out.write("                        \n");
      out.write("                    </script>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
