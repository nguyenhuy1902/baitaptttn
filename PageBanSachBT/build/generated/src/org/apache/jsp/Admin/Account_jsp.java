package org.apache.jsp.Admin;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import Admin.DAO.DAO_Account;
import model.DbtUser;
import java.util.ArrayList;

public final class Account_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n");
      out.write("        <meta name=\"description\" content=\"\">\n");
      out.write("        <meta name=\"author\" content=\"\">\n");
      out.write("        <title>TH ADMIN</title>\n");
      out.write("        <link href=\"css/bootstrap.min.css\" rel=\"stylesheet\">\n");
      out.write("        <link href=\"css/sb-admin.css\" rel=\"stylesheet\">\n");
      out.write("        <link href=\"css/plugins/morris.css\" rel=\"stylesheet\">\n");
      out.write("        <link href=\"font-awesome/css/font-awesome.min.css\" rel=\"stylesheet\" type=\"text/css\">\n");
      out.write("        <link href=\"css/NewStyleAdd.css.css\" rel=\"stylesheet\" type=\"text/css\">\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <!-- -->\n");
      out.write("\n");
      out.write("        <!-- -->\n");
      out.write("        <div id=\"wrapper\">\n");
      out.write("            <nav class=\"navbar navbar-inverse navbar-fixed-top\" role=\"navigation\">\n");
      out.write("                ");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "header.jsp", out, false);
      out.write("\n");
      out.write("                    <div class=\"collapse navbar-collapse navbar-ex1-collapse\">\n");
      out.write("                        <ul class=\"nav navbar-nav side-nav\">\n");
      out.write("                            <li>\n");
      out.write("                                <a href=\"index.jsp\"><i class=\"fa fa-fw fa-dashboard\"></i>Trang chủ</a>\n");
      out.write("                            </li>\n");
      out.write("                            <li>\n");
      out.write("                                <a href=\"DataTable.jsp\"><i class=\"fa fa-fw fa-table\"></i>Bảng dữ liệu</a>\n");
      out.write("                            </li>\n");
      out.write("                            <li class=\"active\">\n");
      out.write("                                <a href=\"Account.jsp\"><i class=\"fa fa-fw fa-user\"></i>Tài khoản</a>\n");
      out.write("                            </li>\n");
      out.write("                            <li>\n");
      out.write("                                <a href=\"Book.jsp\"><i class=\"fa fa-fw fa-book\"></i>Sách</a>\n");
      out.write("                            </li>\n");
      out.write("                            <li>\n");
      out.write("                                <a href=\"Buyer.jsp\"><i class=\"fa fa-fw fa-credit-card\"></i>Đơn Đặt Hàng</a>\n");
      out.write("                            </li>\n");
      out.write("                            <li>\n");
      out.write("                                <a href=\"Chart.jsp\"><i class=\"fa fa-fw fa-bar-chart-o\"></i>Thống kê</a>\n");
      out.write("                            </li>\n");
      out.write("                        </ul>\n");
      out.write("                    </div>\n");
      out.write("                </nav>\n");
      out.write("                <!-- Tai Khoan -->\n");
      out.write("\n");
      out.write("                <div id=\"page-wrapper\">\n");
      out.write("\n");
      out.write("                    <div class=\"container-fluid\">\n");
      out.write("\n");
      out.write("                        <!-- Page Heading -->\n");
      out.write("                        <div class=\"row\">\n");
      out.write("                            <div class=\"col-lg-12\">\n");
      out.write("                                <h1 class=\"page-header\">\n");
      out.write("                                    Tài khoản\n");
      out.write("                                </h1>\n");
      out.write("                                <ol class=\"breadcrumb\">\n");
      out.write("                                    <li>\n");
      out.write("                                        <i class=\"fa fa-dashboard\"></i>  <a href=\"index.jsp\">Trang chủ</a>\n");
      out.write("                                    </li>\n");
      out.write("                                    <li class=\"active\">\n");
      out.write("                                        <i class=\"fa fa-user\"></i> Tài Khoản\n");
      out.write("                                    </li>\n");
      out.write("                                </ol>\n");
      out.write("                            </div>\n");
      out.write("                        </div>\n");
      out.write("                        <!-- /.row -->\n");
      out.write("                        <!-- TimKiem -->\n");
      out.write("                        <div class=\"row\">\n");
      out.write("                            <div class=\"col-lg-6\">\n");
      out.write("                                <div class=\"form-group\">\n");
      out.write("                                    <input class=\"form-control\" placeholder=\"Tìm Kiếm\">\n");
      out.write("                                </div>\n");
      out.write("                            </div>\n");
      out.write("                            <div class=\"col-lg-6\">\n");
      out.write("                                <div class=\"form-group\">\n");
      out.write("                                    <button type=\"submit\" class=\"btn btn-default\"><i class=\"fa fa-search\"></i> Tìm kiếm</button>\n");
      out.write("                                </div>\n");
      out.write("                            </div>\n");
      out.write("                        </div>\n");
      out.write("                        <!--NhapThongTin-->\n");
      out.write("                        <div class=\"row\">\n");
      out.write("                            <div class=\"col-lg-12\">\n");
      out.write("                                <div class=\"row\">\n");
      out.write("                                    <form class=\"row\" method=\"post\" action=\"ManageAccountServlet?action=addUser\">\n");
      out.write("                                        <div class=\"col-lg-4\">\n");
      out.write("                                            <div class=\"form-group\">\n");
      out.write("                                                <label>Mã Người dùng</label>\n");
      out.write("                                                <input class=\"form-control\" placeholder=\"Mã tài khoản\" type=\"text\" name=\"userId\"  readonly/>\n");
      out.write("                                            </div>\n");
      out.write("                                            <div class=\"form-group\">\n");
      out.write("                                                <label>Email</label>\n");
      out.write("                                                <input class=\"form-control\" placeholder=\"Email\" type=\"text\" name=\"userRmail\" required/>\n");
      out.write("                                            </div>  \n");
      out.write("\n");
      out.write("                                        </div>\n");
      out.write("                                        <div class=\"col-lg-4\">\n");
      out.write("                                            \n");
      out.write("                                            <div class=\"form-group\">\n");
      out.write("                                                <label>Mật khẩu</label>\n");
      out.write("                                                <input class=\"form-control\" placeholder=\"Mật khẩu\" type=\"text\" name=\"userPass\" required/>\n");
      out.write("                                            </div>\n");
      out.write("                                            <div class=\"form-group\">\n");
      out.write("                                                <label>Số Điện Thoại</label>\n");
      out.write("                                                <input class=\"form-control\" placeholder=\"Địa chỉ\" type=\"text\" name=\"userPhone\" pattern=\"[0-9]{1,}\" title=\"Nhập số!!!\" required/>\n");
      out.write("                                            </div>                                                                                \n");
      out.write("                                        </div>\n");
      out.write("                                        <div class=\"col-lg-4\">\n");
      out.write("                                            <div class=\"form-group\">\n");
      out.write("                                                <label>Địa chỉ</label>\n");
      out.write("                                                <input class=\"form-control\" placeholder=\"Địa chỉ\" type=\"text\" name=\"userAdd\" required/>\n");
      out.write("                                            </div>    \n");
      out.write("                                            \n");
      out.write("                                        </div>\n");
      out.write("                                        <div class=\"col-lg-4\">\n");
      out.write("                                            <caption>Thao tác </caption>\n");
      out.write("                                        </div>\n");
      out.write("                                        <div class=\"col-lg-2\">\n");
      out.write("                                            <div class=\"form-group\">\n");
      out.write("                                                <button type=\"submit\" class=\"btn btn-success \" value=\"Save\"><i class=\"fa fa-plus\"></i><a onclick=\"return confirm('Bạn có chắc chắn Thêm?')\" style=\"color: white\"> Thêm </a></button>\n");
      out.write("                                            </div>\n");
      out.write("                                        </div>\n");
      out.write("                                    </form>\n");
      out.write("                                </div>\n");
      out.write("                            </div>\n");
      out.write("                        </div>\n");
      out.write("                        <!-- -->\n");
      out.write("                        <!--Bang tai khoan -->\n");
      out.write("                        <div class=\"row\">\n");
      out.write("                            <div class=\"col-lg-12\">\n");
      out.write("                                <h2>Thông tin tài khoản</h2>\n");
      out.write("                                <div class=\"table-responsive\">\n");
      out.write("                                    <table class=\"table table-bordered table-hover table-striped\">\n");
      out.write("                                        <thead>\n");
      out.write("                                            <tr>\n");
      out.write("                                                <th style=\"width: 5%; text-align: center;\">Mã Người Dùng</th>\n");
      out.write("                                                <th style=\"width: 10%; text-align: center;\">Tên Người Dùng</th>\n");
      out.write("                                                <th style=\"width: 10%; text-align: center;\">Email</th>\n");
      out.write("                                                <th style=\"width: 10%; text-align: center;\">Mật khẩu</th>\n");
      out.write("                                                <th style=\"width: 25%; text-align: center;\">Địa chỉ</th>\n");
      out.write("                                                <th style=\"width: 10%; text-align: center;\">Số điện thoại</th>\n");
      out.write("                                                <th style=\"width: 10%;text-align: center;\">Chức năng</th>\n");
      out.write("                                            </tr>\n");
      out.write("                                        </thead>\n");
      out.write("                                        <tbody>\n");
      out.write("                                            <!-- -->\n");
      out.write("                                        ");

                                            DAO_Account ah = new DAO_Account();
                                            ArrayList<DbtUser> listUser = ah.getListUser();
                                        
      out.write("  \n");
      out.write("                                        <!-- -->\n");
      out.write("                                        ");
 for (DbtUser i : ah.getListUser()) {
      out.write("\n");
      out.write("                                        <tr>\n");
      out.write("                                            <td>");
      out.print(i.getUserId());
      out.write("</td>\n");
      out.write("                                            <td>");
      out.print(i.getUserName());
      out.write("</td>\n");
      out.write("                                            <td>");
      out.print(i.getUserEmail());
      out.write("</td>\n");
      out.write("                                            <td>");
      out.print(i.getUserPassWord());
      out.write("</td>\n");
      out.write("                                            <td>");
      out.print(i.getUserAddress());
      out.write("</td>\n");
      out.write("                                            <td>");
      out.print(i.getUserRole());
      out.write("</td>\n");
      out.write("                                                                                     \n");
      out.write("                                            <td class=\"text-center\">\n");
      out.write("                                                <a href=\"EditAccount.jsp?userId=");
      out.print(i.getUserId());
      out.write("\" class=\"btn btn-xs btn-default\" role=\"button\" title=\"Sửa\">\n");
      out.write("                                                    <i class=\"fa fa-pencil\"></i>\n");
      out.write("                                                </a>\n");
      out.write("                                                <p></p> <p></p> <p></p> <p></p>\n");
      out.write("                                                <a href=\"ManageAccountServlet?userId=");
      out.print(i.getUserId());
      out.write("&action=delUser\" class=\"btn btn-xs btn-danger\" title=\"Xóa\" onclick=\"return confirm('Bạn có chắc chắn muốn xóa?')\">\n");
      out.write("                                                    <i class=\"fa fa-times\"></i>\n");
      out.write("                                                </a>\n");
      out.write("                                            </td>\n");
      out.write("                                        </tr>\n");
      out.write("                                        ");
}
      out.write("                  \n");
      out.write("                                    </tbody>\n");
      out.write("                                </table>\n");
      out.write("                            </div>\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                    <!-- -->\n");
      out.write("                    <!--DeTrong-->\n");
      out.write("                    <div class=\"row\">\n");
      out.write("                        <div class=\"col-lg-12\">\n");
      out.write("\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                    <!-- -->\n");
      out.write("                </div>\n");
      out.write("                <div class=\"breadcrumb\" style=\"text-align: center;border: 2px solid black; background: #cdcdcd; \">Design by Đỗ Ngọc Thành &amp; Nguyễn Quang Huy  </div>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("        <script src=\"js/jquery.js\"></script>\n");
      out.write("        <script src=\"js/bootstrap.min.js\"></script>\n");
      out.write("        <script src=\"js/plugins/morris/raphael.min.js\"></script>\n");
      out.write("        <script src=\"js/plugins/morris/morris.min.js\"></script>\n");
      out.write("        <script src=\"js/plugins/morris/morris-data.js\"></script>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
