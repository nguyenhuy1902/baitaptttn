package org.apache.jsp.Admin;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import model.DbtOrder;
import java.util.ArrayList;
import Admin.DAO.DAO_Order;

public final class Buyer_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n");
      out.write("        <meta name=\"description\" content=\"\">\n");
      out.write("        <meta name=\"author\" content=\"\">\n");
      out.write("        <title>TH ADMIN</title>\n");
      out.write("        <link href=\"css/bootstrap.min.css\" rel=\"stylesheet\">\n");
      out.write("        <link href=\"css/sb-admin.css\" rel=\"stylesheet\">\n");
      out.write("        <link href=\"css/plugins/morris.css\" rel=\"stylesheet\">\n");
      out.write("        <link href=\"font-awesome/css/font-awesome.min.css\" rel=\"stylesheet\" type=\"text/css\">\n");
      out.write("        <link href=\"css/NewStyleAdd.css\" rel=\"stylesheet\" type=\"text/css\">\n");
      out.write("\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("\n");
      out.write("        <div id=\"wrapper\">\n");
      out.write("            <nav class=\"navbar navbar-inverse navbar-fixed-top\" role=\"navigation\">\n");
      out.write("                ");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "header.jsp", out, false);
      out.write("\n");
      out.write("                    <div class=\"collapse navbar-collapse navbar-ex1-collapse\">\n");
      out.write("                        <ul class=\"nav navbar-nav side-nav\">\n");
      out.write("                            <li>\n");
      out.write("                                <a href=\"index.jsp\"><i class=\"fa fa-fw fa-dashboard\"></i>Trang chủ</a>\n");
      out.write("                            </li>\n");
      out.write("                            <li>\n");
      out.write("                                <a href=\"DataTable.jsp\"><i class=\"fa fa-fw fa-table\"></i>Bảng dữ liệu</a>\n");
      out.write("                            </li>\n");
      out.write("                            <li>\n");
      out.write("                                <a href=\"Account.jsp\"><i class=\"fa fa-fw fa-user\"></i>Tài khoản</a>\n");
      out.write("                            </li>\n");
      out.write("                            <li>\n");
      out.write("                                <a href=\"Book.jsp\"><i class=\"fa fa-fw fa-book\"></i>Sách</a>\n");
      out.write("                            </li>\n");
      out.write("                            <li  class=\"active\">\n");
      out.write("                                <a href=\"Buyer.jsp\"><i class=\"fa fa-fw fa-credit-card\"></i>Đơn Đặt Hàng</a>\n");
      out.write("                            </li>\n");
      out.write("                            <li>\n");
      out.write("                                <a href=\"Chart.jsp\"><i class=\"fa fa-fw fa-bar-chart-o\"></i>Thống kê</a>\n");
      out.write("                            </li>\n");
      out.write("                        </ul>\n");
      out.write("                    </div>\n");
      out.write("                </nav>\n");
      out.write("                <!--Nguoi Mua-->\n");
      out.write("\n");
      out.write("                <div id=\"page-wrapper\">\n");
      out.write("\n");
      out.write("                    <div class=\"container-fluid\">\n");
      out.write("\n");
      out.write("                        <!-- Page Heading -->\n");
      out.write("                        <div class=\"row\">\n");
      out.write("                            <div class=\"col-lg-12\">\n");
      out.write("                                <h1 class=\"page-header\">\n");
      out.write("                                    Đơn Đặt Hàng\n");
      out.write("                                </h1>\n");
      out.write("                                <ol class=\"breadcrumb\">\n");
      out.write("                                    <li>\n");
      out.write("                                        <i class=\"fa fa-dashboard\"></i>  <a href=\"index.jsp\">Trang chủ</a>\n");
      out.write("                                    </li>\n");
      out.write("                                    <li class=\"active\">\n");
      out.write("                                        <i class=\"fa fa-credit-card\"></i> Đơn Đặt Hàng\n");
      out.write("                                    </li>\n");
      out.write("                                </ol>\n");
      out.write("                            </div>\n");
      out.write("                        </div>\n");
      out.write("                        <!-- /.row -->\n");
      out.write("                        <div class=\"row\">\n");
      out.write("                            <div class=\"col-lg-6\">\n");
      out.write("                                <div class=\"form-group\">\n");
      out.write("                                    <input class=\"form-control\" placeholder=\"Tìm Kiếm\">\n");
      out.write("                                </div>\n");
      out.write("                            </div>\n");
      out.write("                            <div class=\"col-lg-6\">\n");
      out.write("                                <div class=\"form-group\">\n");
      out.write("                                    <button type=\"submit\" class=\"btn btn-default\"><i class=\"fa fa-search\"></i> Tìm kiếm</button>\n");
      out.write("                                </div>\n");
      out.write("                            </div>\n");
      out.write("                        </div>\n");
      out.write("                        <!--NhapThongTin-->\n");
      out.write("                        <!-- Bang Du Liêu-->\n");
      out.write("                        <div class=\"row\">\n");
      out.write("                            <div class=\"col-lg-12\">\n");
      out.write("                                <h2>Thông tin Đơn Đặt Hàng</h2>\n");
      out.write("                                <div class=\"table-responsive\">\n");
      out.write("                                    <table class=\"table table-bordered table-hover table-striped\">\n");
      out.write("                                        <thead>\n");
      out.write("                                            <tr>\n");
      out.write("                                                <th style=\"text-align: center;\">Mã Hóa Đơn</th>\n");
      out.write("                                                <th style=\"text-align: center;\">Tên Người Dùng</th>\n");
      out.write("                                                <th style=\"width: 25%; text-align: center;\">Địa Chỉ</th>\n");
      out.write("                                                <th style=\"text-align: center;\">Số Điện Thoại</th>\n");
      out.write("                                                <th style=\"width: 20%; text-align: center;\">Email</th>\n");
      out.write("                                                <th style=\"text-align: center;\">Trạng Thái</th>\n");
      out.write("                                                <th style=\"text-align: center;\">Giỏ Hàng</th>\n");
      out.write("\n");
      out.write("                                            </tr>\n");
      out.write("                                        </thead>\n");
      out.write("                                        <tbody>\n");
      out.write("                                        ");

                                            DAO_Order ob = new DAO_Order();
                                            ArrayList<DbtOrder> listOrder = ob.getListOrder();
                                        
      out.write("\n");
      out.write("                                        ");
 for (DbtOrder i : listOrder) {
      out.write("\n");
      out.write("                                        <tr>\n");
      out.write("                                            <td>");
      out.print(i.getOrderId());
      out.write("</td>\n");
      out.write("                                            <td>");
      out.print(i.getOrderCustomerName());
      out.write("</td>\n");
      out.write("                                            <td>");
      out.print(i.getOrderCustomerAddress());
      out.write("</td>\n");
      out.write("                                            <td>");
      out.print(i.getOrderCustomerPhone());
      out.write("</td>\n");
      out.write("                                            <td>");
      out.print(i.getOrderCustomerEmail());
      out.write("</td>\n");
      out.write("                                            <td>");
      out.print(i.getOrderZipCodeId());
      out.write("</td>\n");
      out.write("                                            <td style=\"text-align: center\">\n");
      out.write("                                                <a href=\"OrderDetail.jsp?Orderid=");
      out.print(i.getOrderId());
      out.write("\" class=\"btn btn-default btn-xs\" title=\"Chi tiết\">\n");
      out.write("                                                    Chi tiết</i>\n");
      out.write("                                                </a>\n");
      out.write("                                            </td>\n");
      out.write("\n");
      out.write("                                        </tr>\n");
      out.write("                                        ");
}
      out.write("    \n");
      out.write("                                    </tbody>\n");
      out.write("                                </table>\n");
      out.write("                            </div>\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                    <!--DeTrong-->\n");
      out.write("                    <div class=\"row\">\n");
      out.write("                        <div class=\"col-lg-12\">\n");
      out.write("\n");
      out.write("                        </div>\n");
      out.write("                    </div>                \n");
      out.write("                    <!-- /.row -->\n");
      out.write("\n");
      out.write("                </div>\n");
      out.write("                <!-- /.container-fluid -->\n");
      out.write("                <div class=\"breadcrumb\" style=\"text-align: center;border: 2px solid black; background: #cdcdcd; \">Design by Đỗ Ngọc Thành &amp; Nguyễn Quang Huy  </div>\n");
      out.write("\n");
      out.write("            </div>\n");
      out.write("\n");
      out.write("        </div>\n");
      out.write("        <script src=\"js/jquery.js\"></script>\n");
      out.write("        <script src=\"js/bootstrap.min.js\"></script>\n");
      out.write("        <script src=\"js/plugins/morris/raphael.min.js\"></script>\n");
      out.write("        <script src=\"js/plugins/morris/morris.min.js\"></script>\n");
      out.write("        <script src=\"js/plugins/morris/morris-data.js\"></script>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
