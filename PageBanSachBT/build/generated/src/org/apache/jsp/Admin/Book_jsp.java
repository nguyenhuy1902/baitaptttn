package org.apache.jsp.Admin;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import Admin.DAO.DAO_Catagory;
import model.DbtCategory;
import java.util.ArrayList;
import model.DbtBook;
import model.DbtBook;
import Admin.DAO.DAO_Book;

public final class Book_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n");
      out.write("        <meta name=\"description\" content=\"\">\n");
      out.write("        <meta name=\"author\" content=\"\">\n");
      out.write("        <title>Book Library Admin</title>\n");
      out.write("        <link href=\"Images/hogwarts_logo.png\" rel=\"shortcut icon\">\n");
      out.write("        <link href=\"css/bootstrap.min.css\" rel=\"stylesheet\">\n");
      out.write("        <link href=\"css/sb-admin.css\" rel=\"stylesheet\">\n");
      out.write("        <link href=\"css/plugins/morris.css\" rel=\"stylesheet\">\n");
      out.write("        <link href=\"font-awesome/css/font-awesome.min.css\" rel=\"stylesheet\" type=\"text/css\">\n");
      out.write("        <link href=\"css/NewStyleAdd.css\" rel=\"stylesheet\" type=\"text/css\">\n");
      out.write("        <script src=\"js/jquery.min.js\"></script>\n");
      out.write("        <script src=\"js/jquery.js\"></script>\n");
      out.write("        <script src=\"js/bootstrap.min.js\"></script>\n");
      out.write("        <script src=\"js/plugins/morris/raphael.min.js\"></script>\n");
      out.write("        <script src=\"js/plugins/morris/morris.min.js\"></script>\n");
      out.write("        <script src=\"js/plugins/morris/morris-data.js\"></script>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <!-- -->\n");
      out.write("        ");

            DAO_Book hb = new DAO_Book();
            ArrayList<DbtBook> listBook = hb.getListBook();
            DAO_Catagory cd = new DAO_Catagory();
            ArrayList<DbtCategory> listCat = cd.getListCat();
        
      out.write("\n");
      out.write("        <!-- -->\n");
      out.write("        <div id=\"wrapper\">\n");
      out.write("            <nav class=\"navbar navbar-inverse navbar-fixed-top\" role=\"navigation\">\n");
      out.write("                ");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "header.jsp", out, false);
      out.write("\n");
      out.write("                    <div class=\"collapse navbar-collapse navbar-ex1-collapse\">\n");
      out.write("                        <ul class=\"nav navbar-nav side-nav\">\n");
      out.write("                            <li>\n");
      out.write("                                <a href=\"index.jsp\"><i class=\"fa fa-fw fa-dashboard\"></i>Trang chủ</a>\n");
      out.write("                            </li>\n");
      out.write("                            <li>\n");
      out.write("                                <a href=\"DataTable.jsp\"><i class=\"fa fa-fw fa-table\"></i>Bảng dữ liệu</a>\n");
      out.write("                            </li>\n");
      out.write("                            <li>\n");
      out.write("                                <a href=\"Account.jsp\"><i class=\"fa fa-fw fa-user\"></i>Tài khoản</a>\n");
      out.write("                            </li>\n");
      out.write("                            <li  class=\"active\">\n");
      out.write("                                <a href=\"Book.jsp\"><i class=\"fa fa-fw fa-book\"></i>Sách</a>\n");
      out.write("                            </li>\n");
      out.write("                            <li>\n");
      out.write("                                <a href=\"Buyer.jsp\"><i class=\"fa fa-fw fa-credit-card\"></i>Đơn Đặt Hàng</a>\n");
      out.write("                            </li>\n");
      out.write("                            <li>\n");
      out.write("                                <a href=\"Chart.jsp\"><i class=\"fa fa-fw fa-bar-chart-o\"></i>Thống kê</a>\n");
      out.write("                            </li>\n");
      out.write("                        </ul>\n");
      out.write("                    </div>\n");
      out.write("                </nav>\n");
      out.write("                <!-- Sach-->\n");
      out.write("\n");
      out.write("                <div id=\"page-wrapper\">\n");
      out.write("\n");
      out.write("                    <div class=\"container-fluid\">\n");
      out.write("\n");
      out.write("                        <!-- Page Heading -->\n");
      out.write("                        <div class=\"row\">\n");
      out.write("                            <div class=\"col-lg-12\">\n");
      out.write("                                <h1 class=\"page-header\">\n");
      out.write("                                    Sách\n");
      out.write("                                </h1>\n");
      out.write("                                <ol class=\"breadcrumb\">\n");
      out.write("                                    <li>\n");
      out.write("                                        <i class=\"fa fa-dashboard\"></i>  <a href=\"index.jsp\">Trang chủ</a>\n");
      out.write("                                    </li>\n");
      out.write("                                    <li class=\"active\">\n");
      out.write("                                        <i class=\"fa fa-book\"></i> Sách\n");
      out.write("                                    </li>\n");
      out.write("                                </ol>\n");
      out.write("                            </div>\n");
      out.write("                        </div>\n");
      out.write("                        <!-- /.row -->\n");
      out.write("                        <div class=\"row\">\n");
      out.write("                            <div class=\"col-lg-6\">\n");
      out.write("                                <div class=\"form-group\">\n");
      out.write("                                    <input type=\"text\" name=\"search\" id=\"search\" class=\"form-control\"  placeholder=\"Tìm Kiếm\"/>\n");
      out.write("                                </div>\n");
      out.write("                            </div>\n");
      out.write("                            <div class=\"col-lg-6\">\n");
      out.write("                            </div>\n");
      out.write("                        </div>\n");
      out.write("\n");
      out.write("                        <!--NhapThongTin-->\n");
      out.write("                        <div class=\"row\">\n");
      out.write("                            <form class=\"row\" method=\"post\" action=\"ManageBookServlet?action=addBook\">\n");
      out.write("                                <div class=\"col-lg-9\">\n");
      out.write("                                    <div class=\"row\">\n");
      out.write("                                        <div class=\"col-lg-6\">\n");
      out.write("<!--                                            <div class=\"form-group\">\n");
      out.write("                                                <label>Mã sách</label>\n");
      out.write("                                                <input class=\"form-control\" placeholder=\"Mã sách\" type=\"text\" name=\"bookid\" pattern=\"[0-9]{1,}\" readonly/>\n");
      out.write("                                            </div>-->\n");
      out.write("                                            <div class=\"form-group\">\n");
      out.write("                                                <label>Tên sách</label>\n");
      out.write("                                                <input class=\"form-control\" placeholder=\"Tên sách\" type=\"text\" name=\"bookname\" required/>\n");
      out.write("                                            </div>\n");
      out.write("                                            <div class=\"form-group\">\n");
      out.write("                                                <label>Ảnh</label>\n");
      out.write("                                                <input class=\"form-control\" placeholder=\"Ảnh\" type=\"file\" name=\"bookimg\"/>\n");
      out.write("                                            </div>\n");
      out.write("                                            <div class=\"form-group\">\n");
      out.write("                                                <label>Tên tác giả</label>\n");
      out.write("                                                <input class=\"form-control\" placeholder=\"Tên tác giả\" type=\"text\" name=\"bookwriter\" required/>\n");
      out.write("                                            </div>\n");
      out.write("                                            <div class=\"form-group\">\n");
      out.write("                                                <label>Năm xuất bản</label>\n");
      out.write("                                                <input class=\"form-control\" placeholder=\"Năm sản xuất\" type=\"text\" name=\"bookYP\" pattern=\"[0-9]{4}\" title=\"Nhập số\" required/>\n");
      out.write("                                            </div>                                         \n");
      out.write("                                        </div>\n");
      out.write("                                        <div class=\"col-lg-6\">\n");
      out.write("                                            <div class=\"form-group\">\n");
      out.write("                                                <label>Giá</label>\n");
      out.write("                                                <input class=\"form-control\" placeholder=\"Giá\" type=\"text\" name=\"bookprice\" pattern=\"[0-9]{1,}\" title=\"Nhập số!!!\" required>\n");
      out.write("                                            </div>\n");
      out.write("                                            <div class=\"form-group\">\n");
      out.write("                                                <label>Số lượng</label>\n");
      out.write("                                                <input class=\"form-control\" placeholder=\"Số lượng\" type=\"text\" name=\"bookquan\" pattern=\"[0-9]{1,}\" title=\"Nhập số!!!\" required/>\n");
      out.write("                                            </div>\n");
      out.write("                                            <div class=\"form-group\">\n");
      out.write("                                                <label>Nhà xuất bản</label>\n");
      out.write("                                                <input class=\"form-control\" placeholder=\"Nhà xuất bản\" type=\"text\" name=\"bookP\" required/>\n");
      out.write("                                            </div>   \n");
      out.write("                                            <div class=\"form-group\">\n");
      out.write("                                                <label>Mô tả</label>\n");
      out.write("                                                <textarea class=\"form-control\" rows=\"4\" placeholder=\"Mô tả\" type=\"text\" name=\"bookdes\" required></textarea>\n");
      out.write("                                            </div>                                 \n");
      out.write("                                        </div>                                 \n");
      out.write("                                    </div>\n");
      out.write("                                </div>\n");
      out.write("                                <div class=\"col-lg-3\">\n");
      out.write("                                    <div class=\"form-group\">\n");
      out.write("                                        <!--                                        class=\"station_box box_addcus\"-->\n");
      out.write("                                        <label>Danh mục lớn</label>\n");
      out.write("                                        <div>   \n");
      out.write("                                            <select  name=\"catagoryP\" style=\"height: 30px; width: 250px;\">\n");
      out.write("                                            ");
 for (DbtCategory j : listCat) {
                                                    if (j.getCategoryParent() == 0) {
                                            
      out.write("\n");
      out.write("                                            <option value=\"");
      out.print(j.getCategoryId());
      out.write('"');
      out.write('>');
      out.print(j.getCategoryName());
      out.write("</option>\n");
      out.write("                                            ");
}
                                                }
      out.write(" \n");
      out.write("                                            </select>\n");
      out.write("                                    </div>\n");
      out.write("                                    <div>\n");
      out.write("                                        <br><label>Danh mục nhỏ</label>\n");
      out.write("                                        <select  name=\"catagory\" style=\"height: 30px; width: 250px;\">\n");
      out.write("                                            ");
 for (DbtCategory j : listCat) {
                                                    if (j.getCategoryParent() != 0) {
                                            
      out.write("\n");
      out.write("                                            <option value=\"");
      out.print(j.getCategoryId());
      out.write('"');
      out.write('>');
      out.print(j.getCategoryName());
      out.write("</option>\n");
      out.write("                                            ");
}
                                                }
      out.write("\n");
      out.write("                                        </select>\n");
      out.write("                                    </div>                                  \n");
      out.write("                                    <div class=\"form-group\">\n");
      out.write("                                        <br><label>Nhà cung cấp</label>\n");
      out.write("                                        <input class=\"form-control\" placeholder=\"Nhà cung cấp\" type=\"text\" name=\"bookC\" required/>\n");
      out.write("                                    </div>\n");
      out.write("                                    <div class=\"col-lg-12\">\n");
      out.write("                                        <label>Thao Tác</label>\n");
      out.write("                                    </div>\n");
      out.write("                                    <div class=\"col-lg-6\">\n");
      out.write("                                        <div class=\"form-group\">\n");
      out.write("                                            <button type=\"submit\" class=\"btn btn-success \" value=\"Save\"><i class=\"fa fa-plus\"></i> Thêm </button>\n");
      out.write("                                        </div>\n");
      out.write("                                    </div>\n");
      out.write("                                </div>\n");
      out.write("                            </div>\n");
      out.write("                        </form>\n");
      out.write("                    </div>\n");
      out.write("                    <!-- Bang du lieu-->\n");
      out.write("                    <div class=\"row\">\n");
      out.write("                        <div class=\"col-lg-12\">\n");
      out.write("                            <h2>Thông tin Sách</h2>\n");
      out.write("                            <div class=\"table-responsive\">\n");
      out.write("                                <table class=\"table table-bordered table-hover table-striped\" id=\"book_table\">\n");
      out.write("                                    <thead>\n");
      out.write("                                        <tr>\n");
      out.write("                                            <th style=\"text-align: center;\">Mã Sách</th>\n");
      out.write("                                            <th style=\"text-align: center;\">Tên Sách</th>\n");
      out.write("                                            <th style=\"text-align: center;\">Ảnh Sách</th>\n");
      out.write("                                            <th style=\"text-align: center;\">Tên Tác Giả</th>\n");
      out.write("                                            <th style=\"text-align: center;\">Giá Sách</th>\n");
      out.write("                                            <th style=\"text-align: center;\">Số Lượng</th>\n");
      out.write("                                            <th style=\"text-align: center;\">Mô Tả</th>\n");
      out.write("                                            <th style=\"text-align: center;\">Năm Xuất Bản</th>\n");
      out.write("                                            <th style=\"text-align: center;\">Nhà Xuất Bản</th>\n");
      out.write("                                            <th style=\"text-align: center;\">Nhà Cung Cấp</th>\n");
      out.write("                                            <th style=\"width: 3%;text-align: center;\">Chức năng</th>\n");
      out.write("                                        </tr>\n");
      out.write("                                    </thead>\n");
      out.write("                                    <tbody>\n");
      out.write("                                        ");
 for (DbtBook i : listBook) {
      out.write("\n");
      out.write("                                        <tr>\n");
      out.write("                                            <td style=\"text-align: center;\">");
      out.print(i.getBookId());
      out.write("</td>\n");
      out.write("                                            <td style=\"text-align: center;\">");
      out.print(i.getBookName());
      out.write("</td>\n");
      out.write("                                            <td style=\"text-align: center;\"> <img src=\"");
      out.print(i.getBookImage());
      out.write("\" width=\"105px\" height=\"150px\"></td>\n");
      out.write("                                            <td style=\"text-align: center;\">");
      out.print(i.getBookWriterName());
      out.write("</td>\n");
      out.write("                                            <td style=\"text-align: center;\">");
      out.print(i.getBookPrice());
      out.write("</td>\n");
      out.write("                                            <td style=\"text-align: center;\">");
      out.print(i.getBookQuantity());
      out.write("</td>\n");
      out.write("                                            <td style=\"text-align: justify;\">");
      out.print(i.getBookDescription());
      out.write("</td>\n");
      out.write("                                            <td style=\"text-align: justify;\">");
      out.print(i.getBookPublishingYear());
      out.write("</td>\n");
      out.write("                                            <td style=\"text-align: center;\">");
      out.print(i.getBookPublishingCompany());
      out.write("</td>\n");
      out.write("                                            <td style=\"text-align: center;\">");
      out.print(i.getBookProvider());
      out.write("</td>\n");
      out.write("                                            <td style=\"text-align: center;\" class=\"text-center\">\n");
      out.write("                                                <a href=\"EditBook.jsp?bookid=");
      out.print(i.getBookId());
      out.write("\" class=\"btn btn-xs btn-default\" role=\"button\" data-toggle=\"modal\" title=\"Sửa\">\n");
      out.write("                                                    <i class=\"fa fa-pencil\"></i>\n");
      out.write("                                                </a>\n");
      out.write("                                                <p></p> <p></p> <p></p> <p></p>\n");
      out.write("                                                <a href=\"ManageBookServlet?bookid=");
      out.print(i.getBookId());
      out.write("&action=delBook\" class=\"btn btn-xs btn-danger\"  title=\"Xóa\" onclick=\"return confirm('Bạn có chắc chắn muốn xóa?')\">\n");
      out.write("                                                    <i class=\"fa fa-times\"></i>\n");
      out.write("                                                </a>\n");
      out.write("                                            </td>\n");
      out.write("                                        </tr>\n");
      out.write("                                        ");
}
      out.write("                                        \n");
      out.write("                                    </tbody>\n");
      out.write("                                </table>\n");
      out.write("                            </div>\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                    <!--DeTrong-->\n");
      out.write("                    <div class=\"row\">\n");
      out.write("\n");
      out.write("\n");
      out.write("                    </div>\n");
      out.write("                    <!-- /.row -->\n");
      out.write("                </div>\n");
      out.write("                <!-- /.container-fluid -->\n");
      out.write("                <div class=\"breadcrumb\" style=\"text-align: center;border: 2px solid black; background: #cdcdcd; \">Design by Nguyễn Quang Huy  </div>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("    </body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
