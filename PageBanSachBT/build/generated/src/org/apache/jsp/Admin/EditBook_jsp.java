package org.apache.jsp.Admin;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.ArrayList;
import Admin.DAO.DAO_Catagory;
import model.DbtCategory;
import Admin.DAO.DAO_Book;
import model.DbtBook;

public final class EditBook_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n");
      out.write("        <meta name=\"description\" content=\"\">\n");
      out.write("        <meta name=\"author\" content=\"\">\n");
      out.write("        <title>TH ADMIN</title>\n");
      out.write("        <link href=\"Images/hogwarts_logo.png\" rel=\"shortcut icon\">\n");
      out.write("        <link href=\"css/bootstrap.min.css\" rel=\"stylesheet\">\n");
      out.write("        <link href=\"css/sb-admin.css\" rel=\"stylesheet\">\n");
      out.write("        <link href=\"css/plugins/morris.css\" rel=\"stylesheet\">\n");
      out.write("        <link href=\"font-awesome/css/font-awesome.min.css\" rel=\"stylesheet\" type=\"text/css\">\n");
      out.write("        <link href=\"css/NewStyleAdd.css\" rel=\"stylesheet\" type=\"text/css\">\n");
      out.write("        <script src=\"js/jquery.min.js\"></script>\n");
      out.write("        <script src=\"js/jquery.js\"></script>\n");
      out.write("        <script src=\"js/bootstrap.min.js\"></script>\n");
      out.write("        <script src=\"js/plugins/morris/raphael.min.js\"></script>\n");
      out.write("        <script src=\"js/plugins/morris/morris.min.js\"></script>\n");
      out.write("        <script src=\"js/plugins/morris/morris-data.js\"></script>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        ");

            DAO_Book bc = new DAO_Book();
            DbtBook book = null;
            DAO_Catagory cd = new DAO_Catagory();
            ArrayList<DbtCategory> listCat = cd.getListCat();

            if (request.getParameter("bookid") == null) {
                response.sendRedirect(request.getContextPath() + "/404Error.jsp");
            } else {
                int id = Integer.parseInt(request.getParameter("bookid"));
                book = bc.findBook(id);
            }
            if (book == null && request.getParameter("bookid") != null) {
                response.sendRedirect(request.getContextPath() + "/ErrorPage.jsp");
            } else {

            }

        
      out.write("\n");
      out.write("\n");
      out.write("        <div id=\"wrapper\">\n");
      out.write("            <nav class=\"navbar navbar-inverse navbar-fixed-top\" role=\"navigation\">\n");
      out.write("                ");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "header.jsp", out, false);
      out.write("\n");
      out.write("                    <div class=\"collapse navbar-collapse navbar-ex1-collapse\">\n");
      out.write("                        <ul class=\"nav navbar-nav side-nav\">\n");
      out.write("                            <li class=\"active\">\n");
      out.write("                                <a href=\"index.jsp\"><i class=\"fa fa-fw fa-dashboard\"></i>Trang chủ</a>\n");
      out.write("                            </li>\n");
      out.write("                            <li>\n");
      out.write("                                <a href=\"DataTable.jsp\"><i class=\"fa fa-fw fa-table\"></i>Bảng dữ liệu</a>\n");
      out.write("                            </li>\n");
      out.write("                            <li>\n");
      out.write("                                <a href=\"Account.jsp\"><i class=\"fa fa-fw fa-user\"></i>Tài khoản</a>\n");
      out.write("                            </li>\n");
      out.write("                            <li>\n");
      out.write("                                <a href=\"Book.jsp\"><i class=\"fa fa-fw fa-book\"></i>Sách</a>\n");
      out.write("                            </li>\n");
      out.write("                            <li>\n");
      out.write("                                <a href=\"Buyer.jsp\"><i class=\"fa fa-fw fa-credit-card\"></i>Đơn Đặt Hàng</a>\n");
      out.write("                            </li>\n");
      out.write("                            <li>\n");
      out.write("                                <a href=\"Chart.jsp\"><i class=\"fa fa-fw fa-bar-chart-o\"></i>Thống kê</a>\n");
      out.write("                            </li>\n");
      out.write("                        </ul>\n");
      out.write("                    </div>\n");
      out.write("                </nav>\n");
      out.write("                <!--trang chu-->\n");
      out.write("\n");
      out.write("                <div id=\"page-wrapper\">\n");
      out.write("\n");
      out.write("                    <div class=\"container-fluid\">\n");
      out.write("\n");
      out.write("                        <!-- Page Heading -->\n");
      out.write("                        <div class=\"row\">\n");
      out.write("                            <div class=\"col-lg-12\">\n");
      out.write("                                <h1 class=\"page-header\">\n");
      out.write("                                    Sửa Thông Tin Sách\n");
      out.write("                                </h1>\n");
      out.write("                                <ol class=\"breadcrumb\">\n");
      out.write("                                    <li>\n");
      out.write("                                        <i class=\"fa fa-dashboard\"></i>  <a href=\"index.jsp\">Trang chủ</a>\n");
      out.write("                                    </li>\n");
      out.write("                                    <li>\n");
      out.write("                                        <i class=\"fa fa-book\"></i><a href=\"Book.jsp\">Sách </a>\n");
      out.write("                                    </li>\n");
      out.write("                                    <li class=\"active\">\n");
      out.write("                                        <i class=\"fa fa-edit\"></i> Sửa thông tin sách\n");
      out.write("                                    </li>\n");
      out.write("                                </ol>\n");
      out.write("                            </div>\n");
      out.write("                        </div>\n");
      out.write("                        <!-- /.row -->\n");
      out.write("                        <div class=\"row\">\n");
      out.write("                            <div class=\"col-lg-6\">\n");
      out.write("                            </div>\n");
      out.write("                        </div>\n");
      out.write("                        <!--NhapThongTin-->\n");
      out.write("                        <div class=\"row\">\n");
      out.write("                        ");
if (book != null) {
      out.write("\n");
      out.write("                        <form class=\"row\" method=\"post\" action=\"ManageBookServlet?action=editBook\">\n");
      out.write("                            <div class=\"col-lg-9\">\n");
      out.write("                                <div class=\"row\">\n");
      out.write("                                    <div class=\"col-lg-6\">\n");
      out.write("                                        <div class=\"form-group\">\n");
      out.write("                                            <label>Mã sách</label>\n");
      out.write("                                            <input class=\"form-control\" readonly type=\"text\" name=\"bookid\" required value=\"");
      out.print(book.getBookId());
      out.write("\"/>\n");
      out.write("                                        </div>\n");
      out.write("                                        <div class=\"form-group\">\n");
      out.write("                                            <label>Tên sách</label>\n");
      out.write("                                            <input class=\"form-control\" type=\"text\" name=\"bookname\" required value=\"");
      out.print(book.getBookName());
      out.write("\"/>\n");
      out.write("                                        </div>\n");
      out.write("                                        <div class=\"form-group\">\n");
      out.write("                                            <label>Ảnh</label>\n");
      out.write("                                            <input class=\"form-control\" type=\"file\" name=\"bookimg\" required value=\"");
      out.print(book.getBookImage());
      out.write("\"/>\n");
      out.write("                                        </div>\n");
      out.write("                                        <div class=\"form-group\">\n");
      out.write("                                            <label>Tên tác giả</label>\n");
      out.write("                                            <input class=\"form-control\"  type=\"text\" name=\"bookwriter\" required value=\"");
      out.print(book.getBookWriterName());
      out.write("\"/>\n");
      out.write("                                        </div>\n");
      out.write("                                        <div class=\"form-group\">\n");
      out.write("                                            <label>Năm xuất bản</label>\n");
      out.write("                                            <input class=\"form-control\" type=\"text\" name=\"bookYP\"pattern=\"[0-9]{4}\" title=\"Nhập số!!!\" required value=\"");
      out.print(book.getBookPublishingYear());
      out.write("\"/>\n");
      out.write("                                        </div>                                         \n");
      out.write("                                    </div>\n");
      out.write("                                    <div class=\"col-lg-6\">\n");
      out.write("                                        <div class=\"form-group\">\n");
      out.write("                                            <label>Giá</label>\n");
      out.write("                                            <input class=\"form-control\" type=\"text\" name=\"bookprice\" pattern=\"[0-9]{1,}\" title=\"Nhập số!!!\" required value=\"");
      out.print(book.getBookPrice());
      out.write("\"/>\n");
      out.write("                                        </div>\n");
      out.write("                                        <div class=\"form-group\">\n");
      out.write("                                            <label>Số lượng</label>\n");
      out.write("                                            <input class=\"form-control\" type=\"text\" name=\"bookquan\" pattern=\"[0-9]{1,}\" title=\"Nhập số!!!\" required value=\"");
      out.print(book.getBookQuantity());
      out.write("\"/>\n");
      out.write("                                        </div>\n");
      out.write("                                        <div class=\"form-group\">\n");
      out.write("                                            <label>Nhà xuất bản</label>\n");
      out.write("                                            <input class=\"form-control\" type=\"text\" name=\"bookP\" required value=\"");
      out.print(book.getBookPublishingCompany());
      out.write("\"/>\n");
      out.write("                                        </div>   \n");
      out.write("                                        <div class=\"form-group\">\n");
      out.write("                                            <label>Mô tả</label>\n");
      out.write("                                            <textarea class=\"form-control\" rows=\"4\" type=\"text\" name=\"bookdes\" required>");
      out.print(book.getBookDescription());
      out.write("</textarea>\n");
      out.write("                                        </div>                                 \n");
      out.write("                                    </div>                                 \n");
      out.write("                                </div>\n");
      out.write("                            </div>\n");
      out.write("                            <div class=\"col-lg-3\">\n");
      out.write("                                <div class=\"form-group\">\n");
      out.write("                                    <label>Danh Mục Lớn</label>\n");
      out.write("                                    <div>   \n");
      out.write("                                        <select  name=\"catagoryP\" >\n");
      out.write("                                            ");
 for (DbtCategory j : listCat) {
                                                    if (j.getCategoryParent() == 0) {
                                            
      out.write("\n");
      out.write("                                            <option value=\"");
      out.print(j.getCategoryId());
      out.write('"');
      out.write('>');
      out.print(j.getCategoryName());
      out.write("</option>\n");
      out.write("                                            ");
}
                                                }
      out.write(" \n");
      out.write("                                        </select>\n");
      out.write("                                    </div>\n");
      out.write("                                    <div>\n");
      out.write("                                        <label>Danh Mục Nhỏ</label>\n");
      out.write("                                        <select  name=\"catagory\" >\n");
      out.write("                                            ");
 for (DbtCategory j : listCat) {
                                                    if (j.getCategoryParent() != 0) {
                                            
      out.write("\n");
      out.write("                                            <option value=\"");
      out.print(j.getCategoryId());
      out.write('"');
      out.write('>');
      out.print(j.getCategoryName());
      out.write("</option>\n");
      out.write("                                            ");
}
                                                }
      out.write("\n");
      out.write("                                        </select>                                  \n");
      out.write("                                        <div class=\"form-group\">\n");
      out.write("                                            <label>Nhà cung cấp</label>\n");
      out.write("                                            <input class=\"form-control\" type=\"text\" name=\"bookC\" required value=\"");
      out.print(book.getBookProvider());
      out.write("\"/>\n");
      out.write("                                        </div>\n");
      out.write("                                        <div class=\"col-lg-12\">\n");
      out.write("                                            <label>Thao Tác</label>\n");
      out.write("                                        </div>\n");
      out.write("                                        <div class=\"col-lg-6\">\n");
      out.write("                                            <div class=\"form-group\">\n");
      out.write("                                                <button type=\"submit\" class=\"btn btn-warning \" value=\"Save\"><i class=\"fa fa-pencil-square\"></i> Sửa</button>\n");
      out.write("                                            </div>\n");
      out.write("                                        </div>\n");
      out.write("                                    </div>\n");
      out.write("                                </div>\n");
      out.write("                        </form>\n");
      out.write("                        ");
}
      out.write("   \n");
      out.write("                    </div>\n");
      out.write("                    <!-- Bang du lieu-->\n");
      out.write("                    <!--DeTrong-->\n");
      out.write("                    <div class=\"row\">\n");
      out.write("\n");
      out.write("                    </div>\n");
      out.write("                    <!-- /.row -->\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("        </div>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
