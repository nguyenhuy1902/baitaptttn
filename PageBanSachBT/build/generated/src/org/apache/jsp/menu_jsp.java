package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import dao.BookDAO;
import model.DbtBook;
import model.DbtCategory;
import dao.CategoryDAO;

public final class menu_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>JSP Page</title>\n");
      out.write("<!--        <link id=\"callCss\" rel=\"stylesheet\" href=\"themes/bootshop/bootstrap.min.css\" media=\"screen\"/>\n");
      out.write("        <link href=\"themes/css/base.css\" rel=\"stylesheet\" media=\"screen\"/>\n");
      out.write("         Bootstrap style responsive \t\n");
      out.write("        <link href=\"themes/css/bootstrap-responsive.min.css\" rel=\"stylesheet\"/>\n");
      out.write("        <link href=\"themes/css/font-awesome.css\" rel=\"stylesheet\" type=\"text/css\">\n");
      out.write("         Google-code-prettify \t\n");
      out.write("        <link href=\"themes/js/google-code-prettify/prettify.css\" rel=\"stylesheet\"/>\n");
      out.write("        <link href=\"themes/js/jquery.min.js\" rel=\"stylesheet\"/>\n");
      out.write("        <link href=\"themes/js/jquery.js\" rel=\"stylesheet\"/>\n");
      out.write("         fav and touch icons -->\n");
      out.write("\n");
      out.write("        \n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        ");

            CategoryDAO categoryDAO = new CategoryDAO();
        
      out.write("\n");
      out.write("        ");

            BookDAO BookDAO = new BookDAO();
            
      out.write("\n");
      out.write("\n");
      out.write("        <div id=\"sidebar\" class=\"span3\">\n");
      out.write("            <ul id=\"sideManu\" class=\"nav nav-tabs nav-stacked\">\n");
      out.write("                ");

                    for(DbtCategory c : categoryDAO.getCategoryByParent()){
                    
      out.write("\n");
      out.write("                    <li><a href=\"product.jsp?DbtCategory=");
      out.print(c.getCategoryId());
      out.write('"');
      out.write('>');
      out.print(c.getCategoryName());
      out.write("</a>\n");
      out.write("                    ");
if(categoryDAO.getCategoryByChildrent(c.getCategoryId()).size()>0){
      out.write("\n");
      out.write("                    <ul>\n");
      out.write("                        ");

                            for(DbtCategory c1 : categoryDAO.getCategoryByChildrent(c.getCategoryId())){
                            
      out.write("\n");
      out.write("                        <li><a class=\"active\" href=\"product.jsp?DbtCategory=");
      out.print(c1.getCategoryId());
      out.write("\"><i class=\"icon-chevron-right\"></i>");
      out.print(c1.getCategoryName());
      out.write("</a></li>\n");
      out.write("                        ");
}
      out.write("\n");
      out.write("                    </ul>\n");
      out.write("                    ");
}
      out.write("\n");
      out.write("                </li>\n");
      out.write("                ");

                    }
                    
      out.write("\n");
      out.write("            </ul>\n");
      out.write("            <br/>\n");
      out.write("            ");

                for(DbtBook b : BookDAO.getDbtBookRandom2()){
                
      out.write("\n");
      out.write("<!--            <div class=\"thumbnail\">\n");
      out.write("                <a href=\"product-details.jsp?bookId=");
      out.print(b.getBookId());
      out.write("\"><img src=\"");
      out.print(b.getBookImage());
      out.write("\" alt=\"\"/></a>\n");
      out.write("                <div class=\"caption\">\n");
      out.write("                    <h5>");
      out.print(b.getBookName());
      out.write("</h5>\n");
      out.write("                    <h4 style=\"text-align:center\"><a class=\"btn\" href=\"product-details.jsp?bookId=");
      out.print(b.getBookId());
      out.write("\"> <i class=\"icon-zoom-in\"></i></a> <a class=\"btn\" href=\"#\">Thêm vào <i class=\"icon-shopping-cart\"></i></a> <a class=\"btn btn-primary\" href=\"#\">");
      out.print(b.getBookPrice());
      out.write(" VNĐ</a></h4>\n");
      out.write("                </div>\n");
      out.write("            </div><br/>-->\n");
      out.write("            ");

                }
                
      out.write("\n");
      out.write("            <div class=\"thumbnail\">\n");
      out.write("                <img src=\"themes/images/payment_methods.png\" title=\"Bootshop Payment Methods\" alt=\"Payments Methods\">\n");
      out.write("                <div class=\"caption\">\n");
      out.write("                    <h5>Phương thức thanh toán</h5>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("            \n");
      out.write("            <script src=\"themes/js/jquery.js\" type=\"text/javascript\"></script>\n");
      out.write("        <!--<script src=\"themes/js/jquery.js\" type=\"text/javascript\"></script>-->\n");
      out.write("        <script src=\"themes/js/bootstrap.min.js\" type=\"text/javascript\"></script>\n");
      out.write("        <script src=\"themes/js/google-code-prettify/prettify.js\"></script>\n");
      out.write("\n");
      out.write("        <script src=\"themes/js/bootshop.js\"></script>\n");
      out.write("        <script src=\"themes/js/jquery.lightbox-0.5.js\"></script>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
