package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class buy_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>buy</title>\n");
      out.write("\n");
      out.write("        <!-- Bootstrap style --> \n");
      out.write("<!--        <link id=\"callCss\" rel=\"stylesheet\" href=\"themes/bootshop/bootstrap.min.css\" media=\"screen\"/>\n");
      out.write("        <link href=\"themes/css/base.css\" rel=\"stylesheet\" media=\"screen\"/>\n");
      out.write("         Bootstrap style responsive \t\n");
      out.write("        <link href=\"themes/css/bootstrap-responsive.min.css\" rel=\"stylesheet\"/>\n");
      out.write("        <link href=\"themes/css/font-awesome.css\" rel=\"stylesheet\" type=\"text/css\">\n");
      out.write("         Google-code-prettify \t\n");
      out.write("        <link href=\"themes/js/google-code-prettify/prettify.css\" rel=\"stylesheet\"/>\n");
      out.write("         fav and touch icons \n");
      out.write("\n");
      out.write("        <script src=\"themes/js/jquery.js\" type=\"text/javascript\"></script>\n");
      out.write("        <script src=\"themes/js/bootstrap.min.js\" type=\"text/javascript\"></script>\n");
      out.write("        <script src=\"themes/js/google-code-prettify/prettify.js\"></script>\n");
      out.write("\n");
      out.write("        <script src=\"themes/js/bootshop.js\"></script>\n");
      out.write("        <script src=\"themes/js/jquery.lightbox-0.5.js\"></script>-->\n");
      out.write("\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        ");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "header.jsp", out, false);
      out.write("\n");
      out.write("\n");
      out.write("            <div id=\"mainBody\">\n");
      out.write("                <div class=\"container\">\n");
      out.write("                    <div class=\"row\">\n");
      out.write("                    ");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "menu.jsp", out, false);
      out.write("\n");
      out.write("\n");
      out.write("                        <div class=\"span9\">\n");
      out.write("                            <ul class=\"breadcrumb\">\n");
      out.write("                                <li><a href=\"index.jsp\">Trang chủ</a> <span class=\"divider\">/</span></li>\n");
      out.write("                                <li class=\"active\">Mua hàng</li>\n");
      out.write("                            </ul>\n");
      out.write("                            <h3> Mua hàng</h3>\t\n");
      out.write("                            <div class=\"well\">\n");
      out.write("                                <!--\n");
      out.write("                                <div class=\"alert alert-info fade in\">\n");
      out.write("                                        <button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>\n");
      out.write("                                        <strong>Lorem Ipsum is simply dummy</strong> text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s\n");
      out.write("                                 </div>\n");
      out.write("                                <div class=\"alert fade in\">\n");
      out.write("                                        <button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>\n");
      out.write("                                        <strong>Lorem Ipsum is simply dummy</strong> text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s\n");
      out.write("                                 </div>\n");
      out.write("                                 <div class=\"alert alert-block alert-error fade in\">\n");
      out.write("                                        <button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>\n");
      out.write("                                        <strong>Lorem Ipsum is simply</strong> dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s\n");
      out.write("                                 </div> -->\n");
      out.write("                                <form class=\"form-horizontal\" method=\"post\" action=\"");
      out.print(request.getContextPath());
      out.write("/OrderServlet?command=checkout\">\n");
      out.write(" \n");
      out.write("                                    <h4>Địa chỉ của bạn</h4>\n");
      out.write("                                    <div class=\"control-group\">\n");
      out.write("                                        <label class=\"control-label\" for=\"inputFname\">Họ tên</label>\n");
      out.write("                                        <div class=\"controls\">\n");
      out.write("                                            <input type=\"text\" id=\"inputFname\" name=\"hoten\" placeholder=\"\" required=\"Vui lòng nhập tên\">\n");
      out.write("                                        </div>\n");
      out.write("                                    </div>\n");
      out.write("                                    <div class=\"control-group\">\n");
      out.write("                                        <label class=\"control-label\" for=\"address\">Địa chỉ</label>\n");
      out.write("                                        <div class=\"controls\">\n");
      out.write("                                            <input type=\"text\" id=\"address\" name=\"dchi\" placeholder=\"\" required=\"Vui lòng nhập địa chỉ\"/> <span>Địa chỉ đường phố, hộp thư, tên công ty ...</span>\n");
      out.write("                                        </div>\n");
      out.write("                                    </div>\n");
      out.write("\n");
      out.write("                                    <div class=\"control-group\">\n");
      out.write("                                        <label class=\"control-label\" for=\"phone\">Điện thoại di động</label>\n");
      out.write("                                        <div class=\"controls\">\n");
      out.write("                                            <input type=\"text\" id=\"phone\" name=\"dthoai\" placeholder=\"\" required=\"Vui lòng nhập số điện thoại\"/>\n");
      out.write("                                        </div>\n");
      out.write("                                    </div>\n");
      out.write("\n");
      out.write("                                    <div class=\"control-group\">\n");
      out.write("                                        <label class=\"control-label\" for=\"email\">Email</label>\n");
      out.write("                                        <div class=\"controls\">\n");
      out.write("                                            <input type=\"email\" id=\"email\" name=\"email\" placeholder=\"\" required=\"Vui lòng nhập Email\"/>\n");
      out.write("                                        </div>\n");
      out.write("                                    </div>\n");
      out.write("\n");
      out.write("                                    <div class=\"control-group\">\n");
      out.write("                                        <label class=\"control-label\" for=\"aditionalInfo\">Thêm thông tin</label>\n");
      out.write("                                        <div class=\"controls\">\n");
      out.write("                                            <textarea name=\"aditionalInfo\" name=\"thongtin\" id=\"aditionalInfo\" cols=\"26\" rows=\"3\"></textarea>\n");
      out.write("                                        </div>\n");
      out.write("                                    </div>\n");
      out.write("\n");
      out.write("                                    <div class=\"control-group\">\n");
      out.write("                                        <div class=\"controls\">\n");
      out.write("                                            <input type=\"hidden\" name=\"email_create\" value=\"1\">\n");
      out.write("                                            <input type=\"hidden\" name=\"is_new_customer\" value=\"1\">\n");
      out.write("                                            <input id=\"thongbao\" class=\"btn btn-large btn-success\" type=\"submit\" value=\"Mua hàng\" onclick=\"return thongbao();\"/>\n");
      out.write("                                        </div>\n");
      out.write("                                    </div>\t\t\n");
      out.write("                                </form>\n");
      out.write("                            </div>\n");
      out.write("\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        ");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "footer.jsp", out, false);
      out.write("\n");
      out.write("        <script>\n");
      out.write("            $('#thongbao').on('click',function(){\n");
      out.write("               alert(\"Bạn đã mua hàng thành công. Sách sẽ được chuyển đến cho bạn sớm nhất\"); \n");
      out.write("            });\n");
      out.write("        </script>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
