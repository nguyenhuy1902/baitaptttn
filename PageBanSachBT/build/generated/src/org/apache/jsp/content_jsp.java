package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.TreeMap;
import model.DbtCart;
import model.DbtBook;
import dao.BookDAO;

public final class content_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>content</title>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        ");

            BookDAO BookDAO = new BookDAO();
            DbtCart dbtCart = (DbtCart) session.getAttribute("dbtCart");
            if (dbtCart == null) {
                dbtCart = new DbtCart();
                session.setAttribute("dbtCart", dbtCart);
            }
//            TreeMap<DbtBook, Integer> list = dbtCart.getCartListBook();
        
      out.write("\n");
      out.write("        <div class=\"span9\">\t\t\n");
      out.write("            <div class=\"well well-small\">\n");
      out.write("                <h4>Tác phẩm mới <small class=\"pull-right\"></small></h4>\n");
      out.write("                <div class=\"row-fluid\">\n");
      out.write("                    <div id=\"featured\" class=\"carousel slide\">\n");
      out.write("                        <div class=\"carousel-inner\">\n");
      out.write("                            <div class=\"item active\">\n");
      out.write("                                <ul class=\"thumbnails\">\n");
      out.write("                                    ");

                                        int i = 0;
                                        for (DbtBook b : BookDAO.getDbtBookRandom()) {
                                    
      out.write("\n");
      out.write("                                    <li class=\"span3\">\n");
      out.write("                                        <div class=\"thumbnail\">\n");
      out.write("                                            <i class=\"tag\"></i>\n");
      out.write("                                            <a href=\"product-details.jsp?bookId=");
      out.print(b.getBookId());
      out.write("\"><img src=\"");
      out.print(b.getBookImage());
      out.write("\" alt=\"\"></a>\n");
      out.write("                                            <div class=\"caption\">\n");
      out.write("                                                <h5 class=\"name_pro\">");
      out.print(b.getBookName());
      out.write("</h5>\n");
      out.write("                                                <h4><a class=\"btn\" href=\"product-details.jsp?bookId=");
      out.print(b.getBookId());
      out.write("\">Xem</a> <span class=\"pull-right\">");
      out.print(b.getBookPrice());
      out.write(" VNĐ</span></h4>\n");
      out.write("                                            </div>\n");
      out.write("                                        </div>\n");
      out.write("                                    </li>\n");
      out.write("                                    ");

                                            i++;
                                            if (i%4 == 0) {
                                                out.print("</ul></div><div class=\"item\"><ul class=\"thummbnails\">");
                                            }
                                        }
                                    
      out.write("\n");
      out.write("                                </ul>\n");
      out.write("                            </div>\n");
      out.write("\n");
      out.write("                        </div>\n");
      out.write("                        <a class=\"left carousel-control\" href=\"#featured\" data-slide=\"prev\">‹</a>\n");
      out.write("                        <a class=\"right carousel-control\" href=\"#featured\" data-slide=\"next\">›</a>\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("            <h4>Tác phẩm đặc trưng </h4>\n");
      out.write("            <ul class=\"thumbnails\">\n");
      out.write("                ");

                    for (DbtBook b : BookDAO.getDbtBookRandom1()) {
                
      out.write("\n");
      out.write("                <li class=\"span3\">\n");
      out.write("                    <div class=\"thumbnail\">\n");
      out.write("                        <a  href=\"product-details.jsp?bookId=");
      out.print(b.getBookId());
      out.write("\"><img src=\"");
      out.print(b.getBookImage());
      out.write("\" alt=\"\" width=\"150px\"/></a>\n");
      out.write("                        <div class=\"caption\">\n");
      out.write("                            <h5>");
      out.print(b.getBookName());
      out.write("</h5>\n");
      out.write("                            <p> \n");
      out.write("                                Tác giả: ");
      out.print(b.getBookWriterName());
      out.write("\n");
      out.write("                            </p>\n");
      out.write("\n");
      out.write("                            <h4 style=\"text-align:center\"><a class=\"btn\" href=\"product-details.jsp?bookId=");
      out.print(b.getBookId());
      out.write("\"> <i class=\"icon-zoom-in\"></i></a> \n");
      out.write("                                <!--<a class=\"btn\" href=\"");
      out.print(request.getContextPath());
      out.write("/CartServlet?command=insertItem&bookId=");
      out.print(b.getBookId());
      out.write("&bookName=");
      out.print(b.getBookName());
      out.write("&bookWriter=");
      out.print(b.getBookWriterName());
      out.write("&bookAnh=");
      out.print(b.getBookImage());
      out.write("&bookGia=");
      out.print(b.getBookPrice());
      out.write("\">Thêm vào <i class=\"icon-shopping-cart\"></i></a>--> \n");
      out.write("                                <a class=\"btn btn-primary\" href=\"#\">");
      out.print(b.getBookPrice());
      out.write(" VNĐ</a>\n");
      out.write("                            </h4>\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                </li>\n");
      out.write("                ");

                    }
                
      out.write("\n");
      out.write("            </ul>\t\n");
      out.write("\n");
      out.write("        </div>\n");
      out.write("\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
