-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: pagebansach
-- ------------------------------------------------------
-- Server version	5.7.21-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `dbt_book`
--

DROP TABLE IF EXISTS `dbt_book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dbt_book` (
  `book_ID` int(11) NOT NULL AUTO_INCREMENT,
  `book_Name` text NOT NULL,
  `book_Image` varchar(100) DEFAULT NULL,
  `book_WriterName` text NOT NULL,
  `book_Price` float NOT NULL,
  `book_Quantity` int(11) NOT NULL,
  `book_Description` text NOT NULL,
  `book_Status` text,
  `book_PublishingYear` text NOT NULL,
  `book_PublishingCompany` text NOT NULL,
  `book_Provider` text NOT NULL,
  `book_CreateAt` text,
  `book_UpdateAt` text,
  `category_ID` int(11) NOT NULL,
  `writer_ID` int(11) DEFAULT NULL,
  `category_parent` int(11) DEFAULT NULL,
  PRIMARY KEY (`book_ID`),
  KEY `category_ID_idx` (`category_ID`),
  KEY `writer_ID_idx` (`writer_ID`),
  KEY `category_parent_idx` (`category_parent`),
  CONSTRAINT `category_ID` FOREIGN KEY (`category_ID`) REFERENCES `dbt_category` (`category_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `category_parent` FOREIGN KEY (`category_parent`) REFERENCES `dbt_category` (`category_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `writer_ID` FOREIGN KEY (`writer_ID`) REFERENCES `dbt_writer` (`writer_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbt_book`
--

LOCK TABLES `dbt_book` WRITE;
/*!40000 ALTER TABLE `dbt_book` DISABLE KEYS */;
INSERT INTO `dbt_book` VALUES (1,'Đi tìm lẽ sống','themes/images/ditimlesong.jpg','Viktor Frankl',20000,20,'Đi tìm lẽ sống của Viktor Frankl là một trong những quyển sách kinh điển của thời đại. Thông thường, nếu một quyển sách chỉ có một đoạn văn, một ý tưởng có sức mạnh thay đổi cuộc sống của một người, thì chỉ riêng điều đó cũng đã đủ để chúng ta đọc đi đọc lại và dành cho nó một chỗ trên kệ sách của mình. Quyển sách này có nhiều đoạn văn như thế.',NULL,'2016','Nhà Xuất Bản Tổng hợp TP.HCM','Sách điện tử Firstnews',NULL,NULL,7,NULL,1),(2,'Nhà giả kim','themes/images/nhagiakim.jpg','Paulo Coelho',35000,15,'Tiểu thuyết Nhà giả kim của Paulo Coelho như một câu chuyện cổ tích giản dị, nhân ái, giàu chất thơ, thấm đẫm những minh triết huyền bí của phương Đông. Trong lần xuất bản đầu tiên tại Brazil vào năm 1988, sách chỉ bán được 900 bản. Nhưng, với số phận đặc biệt của cuốn sách dành cho toàn nhân loại, vượt ra ngoài biên giới quốc gia, Nhà giả kim đã làm rung động hàng triệu tâm hồn, trở thành một trong những cuốn sách bán chạy nhất mọi thời đại, và có thể làm thay đổi cuộc đời người đọc.',NULL,'2013','Nhà Xuất Bản Văn Học','Nhã Nam',NULL,NULL,1,1,8),(3,'Thế giới phẳng','themes/images/thegioiphang.jpg','	Thomas L. Friedman',12000,36,'Trong xu thế toàn cầu hóa, việc tiếp cận và tham khảo những tri thức đương đại từ những nước đã phát triển về sự chuyển động của thế giới có lẽ sẽ giúp chúng ta có thêm những thông tin bổ ích để có sự chủ động trong quá trình hội nhập. Tác phẩm được xếp vào danh mục sách bán chạy nhất ở Mỹ',NULL,'2014','	Nhà Xuất Bản Trẻ','	Nhà Xuất Bản Trẻ',NULL,NULL,2,1,2),(4,'Tắt đèn','themes/images/tatden.jpg','Ngô Tất Tố',50000,25,'Chị Dậu là nhân vật điển hình được người đọc yêu mến. Và người yêu mến chị hơn cả là Ngô Tất Tố. Giữa biết bao tệ nạn và cảnh đời bất công ngang trái ở nông thôn Việt Nam cũ, Ngô Tất Tố đã hết lòng bảo vệ một người phụ nữ là chị Dậu. Nhiều lần chị Dậu bị đẩy vào tình thế hiểm nghèo, rất có thể bị làm nhục nhưng Ngô Tất Tố đã bảo đảm cho chị Dậu được bảo đảm an toàn phẩm toàn vẹn, giữ trọn phẩm giá, không phải đau đớn. dằn vặt.',NULL,'2018','Nhà Xuất Bản Văn Học','Nhà Xuất Bản Văn Học',NULL,NULL,1,1,7),(5,'13 Nguyên Tắc Nghĩ Giàu Làm Giàu','themes/images/13nguyentac.jpg','	Napoleon Hill',120000,45,'13 Nguyên Tắc Nghĩ Giàu Làm Giàu là cuốn sách “chỉ dẫn” duy nhất chỉ ra những nguồn lực bạn phải có để thành công. Cuốn sách sẽ giúp bạn trở nên giàu có, làm giàu thêm cho cuộc sống của bạn trên tất cả các phương diện của cuộc sống chứ không chỉ về tài chính và vật chất. Những ý tưởng trong cuốn sách Think and Grow rich – 13 nguyên tắc nghĩ giàu, làm giàu bắt nguồn từ những động lực tốt đẹp: “Thành công cá nhân” và “Quan điểm suy nghĩ tích cực”.',NULL,'2010','Nhà Xuất Bản Lao Động Xã Hội','Thái Hà',NULL,NULL,2,1,10),(6,'Khác biệt hay là chết','themes/images/khacbiethaylachet.jpg','	Jack Trout',52000,87,'Khác biệt hay là chết được đánh giá là một trong những cuốn sách marketing hay nhất của mọi thời đại. Chính qua quyển sách này, khác biệt hóa (differentiate) đã trở thành một “từ ngữ lớn” trong thế giới kinh doanh ngày nay.',NULL,'2015','Nhà Xuất Bản Tổng hợp TP.HCM','FirstNews',NULL,NULL,2,1,10),(7,'Điện tử thực hành','themes/images/dientuthuchanh.jpg','Đỗ Đức Trí',36000,65,'Điện tử thực hành là môn học cơ bản trong khoa Điện - Điện tử. Vì vậy, nắm vững môn Điện tử thực hành là vấn đề cơ bản của sinh viên ngành kỹ thuật. Cuốn sách Điện tử thực hành được biên soạn dựa trên cơ sở chương trình môn học Điện tử thực hành có thể dùng trong các trường cao đẳng, đại học khối công nghệ. Nó cung cấp cho sinh viên những kiến thức và kỹ năng thực hành và hướng dẫn sử dụng cũng như vận hành các dụng cụ, thiết bị điện tử cơ bản nhất của môn học',NULL,'2016','	Nhà Xuất Bản Bách Khoa Hà Nội','Bão',NULL,NULL,3,1,14),(8,'Tự Học Microsoft Excel','themes/images/tuhocexcel.jpg','VL - Comp',80000,14,'Tạo bảng, quản lý và xem bảng tính, sử dụng các công thức và hàm, định dạng các ô, định dạng bảng tính',NULL,'2017','Nhà Xuất Bản Hồng Đức','Văn Lang',NULL,NULL,3,1,11),(9,'Con Đường Hồi Giáo','themes/images/conduonghoigiao.jpg','	Nguyễn Phương Mai',98000,25,'Bởi tôi biết còn có rất nhiều điều thiêng liêng hơn niềm tin tôn giáo, ấy là niềm tin vào sự ràng buộc cội rễ của giống loài; vào sự giống nhau giữa người với người hơn là sự khác biệt về đức tin; vào lòng tốt; vào sự đồng cảm và hướng thiện.',NULL,'2018','	Nhà Xuất Bản Hội Nhà Văn','Nhã Nam',NULL,NULL,4,1,13),(10,'Sống Như Người Paris','themes/images/songnhunguoiparis.jpg','	Sophie Mas',48000,64,'Cuốn cẩm nang thú vị về phong cách Pháp này sở hữu một uy quyền duyên dáng. Nó cũng đồng thời giàu sức thuyết phục một cách kỳ lạ.',NULL,'2010','	Nhà Xuất Bản Thế Giới','Kim Đồng',NULL,NULL,4,1,12),(11,'Nước Đức Trong Lòng Bàn Tay','themes/images/nuocductronglongbantay.jpg','Trần Mai',65000,25,'Trong cuốn sách này, tác giả sẽ kể cho bạn nghe về con đường du học của mình, về những kinh nghiệm với đủ cả nắng ấm và mưa rào, để bạn cân nhắc trước khi đưa ra lựa chọn cho bản thân. Lẽ dĩ nhiên là con đường của bạn rồi sẽ khác. Năm tháng qua đi, chỉ có những trải nghiệm và ký ức sẽ được lưu giữ. Dù cuối cùng, bạn quyết định như thế nào thì cũng hy vọng rằng, bạn đã sống trọn vẹn không có điều gì phải nuối tiếc.',NULL,'2011','	Nhà Xuất Bản Thế Giới','	Nhà Xuất Bản Thế Giới',NULL,NULL,4,1,13),(12,'Cẩm Nang Du Lịch Anh','themes/images/dulichanh.jpg','Dorling Kindersley Limited',32000,36,'Cuốn cẩm nang này sẽ giúp bạn có được trải nghiệm tuyệt vời nhất trong chuyến du lịch đảo Anh, với hàng loạt thông tin thiết thực và gợi ý hữu ích từ các chuyên gia. Phần Giới thiệu đảo Anh cho bạn cái nhìn tổng thể về bối cảnh văn hóa và lịch sử của đảo quốc này',NULL,'2012','	Nhà Xuất Bản Dân Trí','	Nhà Xuất Bản Dân Trí',NULL,NULL,4,1,13),(13,'Văn Minh Việt Nam','themes/images/vanminhvietnam.jpg','Nguyễn Văn Huyên',46000,14,'Bản thảo cuốn sách này được hoàn thành năm 1939 với tựa đề tiếng Pháp \"La civilisation annamite\", được xuất bản tại Hà Nội năm 1944, có thể được coi là phát ngôn của người Việt về văn hóa Việt Nam với cộng đồng thế giới',NULL,'2012','	Nhà Xuất Bản Hồng Đức','Phương Nam',NULL,NULL,4,1,12),(14,'Photoshop CS6','themes/images/tuhocthietkedohoa.jpg','Nguyễn Đức Hiếu',130000,52,'Cuốn sách Tự Học Thiết Kế Đồ Họa Trên Photoshop CS6 được biên soạn nhằm hướng dẫn bạn đọc các cách chỉnh sửa ảnh, tạo ảnh nghệ thuật, thiết kế những tấm quảng cáo bắt mắt, xây dựng giao diện Web tiện dụng... bằng ngôn từ và hình ảnh minh họa hướng dẫn dễ hiểu nhất.',NULL,'2013','	Nhà Xuất Bản Hồng Đức','Trí Việt',NULL,NULL,3,1,15),(15,'Giáo Trình HDH Windows Server 2003','themes/images/giaotrinhwindows.jpg','	Ths. Lê Tự Thanh',150000,465,'Giáo trình cung cấp những kiến thức cơ bản và nền tảng nhất, nhằm giúp cho bạn đọc nắm bắt và có thể triển khai ngay một mạng máy tính sử dụng hệ điều hành Windows Server 2003; cách thức triển khai một số dịch vụ mạng cơ bản như: DHCP Server, DNS Server, FTP Server, Web Server…',NULL,'2014','Nhà Xuất Bản Thông Tin Và Truyền Thông','Nhà Xuất Bản Thông Tin Và Truyền Thông',NULL,NULL,3,1,14),(16,'Tư Duy Pháp Lý Của Luật Sư','themes/images/tuduyphaply.jpg','Luật sư Nguyễn Ngọc Bích',125000,78,'Quyển sách này là phiên bản mới của quyển Tài ba của luật sư xuất bản năm 2010 và tái bản hai lần sau đó. Đó là kết quả của những góp ý từ độc giả, của kinh nghiệm tác giả thu thập được qua các lớp học được tổ chức tại Đoàn Luật sư TP. Hồ Chí Minh cũng như sự tìm tòi và học hỏi của tác giả trong suốt 5 năm qua. Cũng như sách trước, quyển sách này được viết cho các luật sư mới bước chân vào nghề.',NULL,'2015','	Nhà Xuất Bản Trẻ','	Nhà Xuất Bản Trẻ',NULL,NULL,5,1,NULL),(17,'Luật Đất Đai Năm 2013','themes/images/luatdatdai.jpg','Minh Ngọc',56000,65,'Nhằm giúp cho mọi người hiểu thêm về chế độ sở hữu đất đai, quyền hạn và trách nhiệm của Nhà nước đại diện chủ sở hữu toàn dân về đất đai và thống nhất quản lý đất đai, chế độ quản lý và sử dụng đất đai, quyền và nghĩa vụ của người sử dụng đất đối với đất đai thuộc lãnh thổ của nước Cộng hòa xã hội chủ nghĩa Việt Nam, tác giả Minh Ngọc đã sưu tầm, tuyển chọn những nội dung liên quan đến Luật Đất Đai năm 2013 và văn bản hướng dẫn thi hành để giới thiệu đến bạn đọc.',NULL,'2016','	Nhà Xuất Bản Lao Động','Trí Việt',NULL,NULL,5,1,NULL),(18,'Bộ Luật Tố Tụng Dân Sự','themes/images/totungdansu.jpg','Minh Ngọc',65000,15,'Nội dung sách bao gồm các văn bản quy phạm pháp luật quy định những nguyên tắc cơ bản trong tố tụng dân sự; trình tự, thủ tục khởi kiện để Toà án giải quyết các vụ án về tranh chấp dân sự, hôn nhân và gia đình, kinh doanh, thương mại, lao động (sau đây gọi chung là vụ án dân sự) và trình tự, thủ tục yêu cầu để Toà án giải quyết các việc về yêu cầu dân sự, hôn nhân và gia đình, kinh doanh, thương mại, lao động (sau đây gọi chung là việc dân sự)...',NULL,'2017','	Nhà Xuất Bản Lao Động','Trí Việt',NULL,NULL,5,1,NULL),(19,'Bộ Luật Lao Động','themes/images/boluatlaodong.jpg','Nguyễn Quang Huy',24000,35,'Bộ luật lao động quy định tiêu chuẩn lao động; quyền, nghĩa vụ, trách nhiệm của người lao động, người sử dụng lao động, tổ chức đại diện tập thể lao động, tổ chức đại diện người sử dụng lao động trong quan hệ lao động và các quan hệ khác liên quan trực tiếp đến quan hệ lao động; quản lý nhà nước về lao động.',NULL,'2018','	Nhà Xuất Bản Lao Động','Trí Việt',NULL,NULL,5,1,NULL),(20,'Bộ Luật Hình Sự','themes/images/boluathinhsu.jpg','	Đặng Thị Minh',56000,78,'Nội dung cuốn sách bao gồm những phần chính sau: Nghị quyết 41/2017/QH14 vừa được Quốc hội khóa XIV, kỳ họp thứ 3 chính thức thông qua ngày 20/6/2017. Theo đó, từ ngày 01/01/2018 các Bộ luật và Luật dưới đây chính thức có hiệu lực thi hành: Bộ luật Hình sự số 100/2015/QH13 đã được sửa đổi, bổ sung theo Luật số 12/2017/QH14',NULL,'2017','	Nhà Xuất Bản Thế Giới','Nhà sách Lao Động',NULL,NULL,5,1,NULL),(21,'Nghệ Thuật Bài Trí Của Người Nhật','themes/images/nghethuatbaitri.jpg','	Marie Kondo',89000,96,'Hãy biến ngôi nhà của bạn thành một không gian luôn ngăn nắp và sạch sẽ với phương pháp KonMari kỳ diệu. Chuyên gia sắp xếp và dọn dẹp người Nhật, Marie Kondo, sẽ giúp bạn dọn sạch các căn phòng một lần cho mãi mãi với các phương pháp truyền cảm hứng được thực hiện theo từng bước.',NULL,'2016','	Nhà Xuất Bản Lao Động','Thái Hà',NULL,NULL,6,1,NULL),(22,'Nhật Ký Học Làm Bánh','themes/images/nhatkyhoclambanh.jpg','	Linh Trang',80000,54,'Sau hơn một năm chuẩn bị kỹ lưỡng, Nhật ký học làm bánh 2 tập trung vào nhiều loại bánh quen thuộc nhưng không dễ chinh phục - chẳng hạn như các loại bánh mì - và được đầu tư công phu về mặt trình bày hứa hẹn sẽ còn mang đến nhiều điều thú vị hơn nữa cho người đọc. Hi vọng cuốn sách có thể giúp bạn tiến bước thuận lợi, suôn sẻ trên hành trình \"bơ-bột- trứng\", làm sao tránh được nhiều nhất các “ổ voi” thất bại khiến cho bánh từ lò nướng bay thẳng tới thùng rác. Và cũng mong rằng, những ghi chép từ quá trình tự học làm bánh của tác giả sẽ tăng thêm quyết tâm, gạt bỏ đôi chút chần chừ, lưỡng lự của những bạn trẻ muốn “dấn thân” vào con đường này.',NULL,'2016','	Nhà Xuất Bản Thế Giới','Nhã Nam',NULL,NULL,6,1,NULL),(23,'Dưỡng Da Trọn Gói','themes/images/duongdatrongoi.jpg','Đỗ Anh Thư - Phạm Hương Thuỷ',46000,55,' Với phần khảo sát thị trường rất kỳ công của các tác giả, bạn không những biết da mình hợp với nhãn Mỹ phẩm nào mà còn biết chính xác túi tiền mình liệu có phù hợp với loại Mỹ phẩm đó hay không? Và quan trọng hơn, thật sung sướng khi được yên tâm rằng món giá cả rất phải chăng có khi lại là món có chất lượng rất tốt.',NULL,'2016','	Nhà Xuất Bản Thế Giới','Nhã Nam',NULL,NULL,6,1,NULL),(24,'202 Món Canh Ngon','themes/images/202moncanhngon.jpg','	Nguyễn Viên Chi',35000,25,'Cuốn sách cung cấp công thức chế biến những món canh ngon miệng, giải nhiệt như: Canh ngót Thái, canh gấc hải sản, canh đậu rồng trứng cá thu... Các món canh được phân loại và sắp xếp theo nguyên liệu chế biến (hải sản, rau thịt) rất thuận tiện cho việc thiết kê thực đơn của bạn. Đây là cuốn cẩm nang hữu ích góp phần làm phong phú thêm bữa cơm gia đình.',NULL,'2014','	Nhà Xuất Bản Hà Nội','Tân Việt',NULL,NULL,7,NULL,1),(25,'abc','themes/images/caychuoinondigiayxanh.png','huy',35000,30,'abcaaa',NULL,'2018','NX Kim Đồng','Quang Huy',NULL,NULL,7,NULL,1),(26,'vũ trụ bất tận','themes/images/luocsuvanvat.jpg','huy',35000,30,'Vũ trụ là bất tận',NULL,'2018','Quang Huy','Nhà Xuất Bản Tổng hợp TP.HCM HCM',NULL,NULL,8,NULL,9);
/*!40000 ALTER TABLE `dbt_book` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbt_cart`
--

DROP TABLE IF EXISTS `dbt_cart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dbt_cart` (
  `cart_ID` int(11) NOT NULL,
  `cart_listBook` text,
  PRIMARY KEY (`cart_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbt_cart`
--

LOCK TABLES `dbt_cart` WRITE;
/*!40000 ALTER TABLE `dbt_cart` DISABLE KEYS */;
INSERT INTO `dbt_cart` VALUES (1,'1');
/*!40000 ALTER TABLE `dbt_cart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbt_category`
--

DROP TABLE IF EXISTS `dbt_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dbt_category` (
  `category_ID` int(11) NOT NULL,
  `category_Name` text,
  `category_CreateAt` varchar(45) DEFAULT NULL,
  `category_UpdateAt` varchar(45) DEFAULT NULL,
  `category_parent` int(11) DEFAULT NULL,
  PRIMARY KEY (`category_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbt_category`
--

LOCK TABLES `dbt_category` WRITE;
/*!40000 ALTER TABLE `dbt_category` DISABLE KEYS */;
INSERT INTO `dbt_category` VALUES (1,'Sách văn học',NULL,NULL,0),(2,'Sách kinh tế',NULL,NULL,0),(3,'Sách công nghệ thông tin',NULL,NULL,0),(4,'Sách văn hóa du lịch',NULL,NULL,0),(5,'Sách chính trị pháp luật',NULL,NULL,0),(6,'Sách thường thức đời sống',NULL,NULL,0),(7,'Sách văn học Việt Nam',NULL,NULL,1),(8,'Sách văn học Nước ngoài',NULL,NULL,1),(9,'Thiên văn học',NULL,NULL,0),(10,'Sách Marketing',NULL,NULL,2),(11,'Tin học văn phòng',NULL,NULL,3),(12,'Sách Phong Tục - Tập Quán',NULL,NULL,4),(13,'Văn Hóa - Du Lịch',NULL,NULL,4),(14,'Lập Trình',NULL,NULL,3),(15,'Thiết kế đồ họa',NULL,NULL,3);
/*!40000 ALTER TABLE `dbt_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbt_contact`
--

DROP TABLE IF EXISTS `dbt_contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dbt_contact` (
  `contact_ID` int(11) NOT NULL AUTO_INCREMENT,
  `contact_FullName` varchar(45) NOT NULL,
  `contact_Email` varchar(45) NOT NULL,
  `contacct_Title` varchar(45) DEFAULT NULL,
  `contact_Content` varchar(45) DEFAULT NULL,
  `contact_CreateAt` varchar(45) DEFAULT NULL,
  `contact_UpdateAt` varchar(45) DEFAULT NULL,
  `user_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`contact_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbt_contact`
--

LOCK TABLES `dbt_contact` WRITE;
/*!40000 ALTER TABLE `dbt_contact` DISABLE KEYS */;
/*!40000 ALTER TABLE `dbt_contact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbt_customer`
--

DROP TABLE IF EXISTS `dbt_customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dbt_customer` (
  `customer_ID` int(11) NOT NULL,
  `customer_Name` text NOT NULL,
  `customer_Address` text NOT NULL,
  `customer_Email` text NOT NULL,
  `customer_Phone` int(11) DEFAULT NULL,
  `customer_CreateAt` varchar(45) DEFAULT NULL,
  `customer_UpdateAt` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`customer_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbt_customer`
--

LOCK TABLES `dbt_customer` WRITE;
/*!40000 ALTER TABLE `dbt_customer` DISABLE KEYS */;
/*!40000 ALTER TABLE `dbt_customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbt_order`
--

DROP TABLE IF EXISTS `dbt_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dbt_order` (
  `order_ID` int(11) NOT NULL AUTO_INCREMENT,
  `order_CustomerName` text,
  `order_CityName` text,
  `order_CustomerAddress` text,
  `order_CustomerPhone` int(11) DEFAULT NULL,
  `order_CustomerEmail` text,
  `order_ZipCodeID` varchar(45) DEFAULT NULL,
  `order_MoreInformation` text,
  `order_TotalMoney` float DEFAULT NULL,
  `order_CreateAt` varchar(45) DEFAULT NULL,
  `order_UpdateAt` varchar(45) DEFAULT NULL,
  `user_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`order_ID`),
  KEY `user_idx` (`user_ID`),
  CONSTRAINT `user` FOREIGN KEY (`user_ID`) REFERENCES `dbt_user` (`user_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbt_order`
--

LOCK TABLES `dbt_order` WRITE;
/*!40000 ALTER TABLE `dbt_order` DISABLE KEYS */;
INSERT INTO `dbt_order` VALUES (4,'thy ngọc',NULL,'sài gòn',123456789,'ltn@gmail.com',NULL,NULL,NULL,NULL,NULL,1),(5,'Duy Bách',NULL,'hà nội',1230456,'duybach@gmail.com',NULL,NULL,NULL,NULL,NULL,3),(6,'thành đạt',NULL,'hải dương',1234567,'thanhdat@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL),(9,'Ngô Thành ĐẠt',NULL,'Hải Dương',1136524985,'thanhdat@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL),(10,'hoang diep anh',NULL,'hà nội',1635242516,'hda@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL),(47,'quang huy',NULL,'Hà Đông',1635224426,'nguyenquanghuy.190297@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL),(48,'nguyen huy',NULL,'ha dong',123456,'nguyenquanghuy.190297@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL),(49,'nguyen huy',NULL,'ha dong',123456,'nguyenquanghuy.190297@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL),(50,'nguyen huy',NULL,'ha dong',123456,'nguyenquanghuy.190297@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL),(51,'nguyen huy',NULL,'ha dong',123456,'nguyenquanghuy.190297@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL),(52,'nguyen huy',NULL,'ha dong',123456,'nguyenquanghuy.190297@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `dbt_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbt_orderdetail`
--

DROP TABLE IF EXISTS `dbt_orderdetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dbt_orderdetail` (
  `orderdetail_ID` int(11) NOT NULL AUTO_INCREMENT,
  `book_ID` int(11) DEFAULT NULL,
  `order_ID` int(11) DEFAULT NULL,
  `orderdetail_Price` varchar(45) DEFAULT NULL,
  `orderdetail_Quantity` varchar(45) DEFAULT NULL,
  `orderdetail_TotalMoney` varchar(45) DEFAULT NULL,
  `orderdetail_CreateAt` varchar(45) DEFAULT NULL,
  `orderdetail_UpdateAt` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`orderdetail_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbt_orderdetail`
--

LOCK TABLES `dbt_orderdetail` WRITE;
/*!40000 ALTER TABLE `dbt_orderdetail` DISABLE KEYS */;
INSERT INTO `dbt_orderdetail` VALUES (7,4,4,'50000.0','4','200000.0',NULL,NULL),(8,15,5,'150000.0','2','300000.0',NULL,NULL),(9,3,5,'12000.0','3','36000.0',NULL,NULL),(10,17,6,'56000.0','2','112000.0',NULL,NULL),(11,24,7,'35000.0','4','140000.0',NULL,NULL),(12,12,8,'32000.0','2','64000.0',NULL,NULL),(13,20,9,'56000.0','1','56000.0',NULL,NULL),(14,11,9,'65000.0','2','130000.0',NULL,NULL),(15,5,10,'120000.0','2','240000.0',NULL,NULL),(16,11,10,'65000.0','1','65000.0',NULL,NULL),(57,18,47,'65000.0','1','65000.0',NULL,NULL),(58,6,47,'52000.0','2','104000.0',NULL,NULL),(59,1,48,'20000.0','1','20000.0',NULL,NULL),(60,22,49,'80000.0','1','80000.0',NULL,NULL),(61,4,49,'50000.0','3','150000.0',NULL,NULL),(62,10,50,'48000.0','1','48000.0',NULL,NULL),(63,13,51,'46000.0','1','46000.0',NULL,NULL),(64,25,52,'35000.0','1','35000.0',NULL,NULL);
/*!40000 ALTER TABLE `dbt_orderdetail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbt_user`
--

DROP TABLE IF EXISTS `dbt_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dbt_user` (
  `user_ID` int(11) NOT NULL AUTO_INCREMENT,
  `user_Name` text,
  `user_Email` text,
  `user_PassWord` text,
  `user_Role` int(11) DEFAULT NULL,
  `user_Address` text,
  `user_CityName` text,
  `user_Avatar` text,
  `user_CreateAt` varchar(45) DEFAULT NULL,
  `user_UpdateAt` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`user_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbt_user`
--

LOCK TABLES `dbt_user` WRITE;
/*!40000 ALTER TABLE `dbt_user` DISABLE KEYS */;
INSERT INTO `dbt_user` VALUES (1,'thy ngọc','ltn@gmail.com','123456',123648789,'TP Hồ Chí Minh',NULL,'null',NULL,NULL),(2,'quang huy','nqhuy@gmail.com','123456',1635224426,'Hà Đông Hà Nội',NULL,'null',NULL,NULL),(3,'bach','duybach@gmail.com','123456',985214142,'HÀ Nội',NULL,'null',NULL,NULL),(4,'thanh dat','thanhdat@gmail.com','123456',96352412,'Hải Dương',NULL,'null',NULL,NULL),(5,'diep anh','hda@gmail.com','diepanh',98969652,'Hà Nội',NULL,'null',NULL,NULL),(8,'linh nhi','linhnhi97@gmail.com','linhnhi',12362514,'Mỹ Đình Hà Nội',NULL,'null',NULL,NULL),(9,'bach bach','bachbach@gmail.com','bachbach',123632514,'Hà Nội',NULL,'null',NULL,NULL),(12,'Quang Huy','nguyenquanghuy.190297@gmail.com','123456',1635224426,'Hà Nội',NULL,'null',NULL,NULL);
/*!40000 ALTER TABLE `dbt_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbt_writer`
--

DROP TABLE IF EXISTS `dbt_writer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dbt_writer` (
  `writer_ID` int(11) NOT NULL,
  `writer_Name` text,
  `writer_Dob` varchar(45) DEFAULT NULL,
  `writer_Address` text,
  `writer_Phone` int(11) DEFAULT NULL,
  `writer_Email` text,
  `book_ID` int(11) DEFAULT NULL,
  `writer_CreateAt` varchar(45) DEFAULT NULL,
  `writer_UpdateAt` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`writer_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbt_writer`
--

LOCK TABLES `dbt_writer` WRITE;
/*!40000 ALTER TABLE `dbt_writer` DISABLE KEYS */;
INSERT INTO `dbt_writer` VALUES (1,'Nguyễn Quang Huy','1997/02/19','HÀ Nội',123456,'nguyenquanghuy@gmail.com',1,NULL,NULL);
/*!40000 ALTER TABLE `dbt_writer` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-25  9:10:19
